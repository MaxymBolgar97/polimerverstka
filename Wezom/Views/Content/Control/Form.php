<?php echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(['name' => 'name', 'value' => __('Отправить'), 'class' => 'submit btn btn-primary pull-right']); ?>
    </div>
    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo __('Основные данные'); ?>
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Название'); ?></label>
                        <b class="red"><?php echo $obj->name; ?></b>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::tiny([
                            'name' => 'FORM[text]',
                            'value' => $obj->text,
                        ], __('Контент')); ?>
                    </div>
                    <?php if($obj->alias == 'contact'): ?>
                        <?php $data = json_decode($obj->other, TRUE); ?>
                        <div class="widget box">
                            <div class="widgetHeader">
                                <div class="widgetTitle">
                                    <i class="fa-reorder"></i>
                                    <?php echo __('Настройка карты'); ?>
                                </div>
                            </div>
                            <div class="widgetContent">
                                <div class="mapWrap" style="height: 450px;">
                                    <input type="text" id="address" class="form-control ui-autocomplete-input"
                                           placeholder="<?php echo __('Начните писать адрес...'); ?>" autocomplete="off">
                                    <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                                    <div style="width: 100%; height: calc(100% - 30px); position: relative; background-color: rgb(229, 227, 223); overflow: hidden;" id="gmap"></div>
                                </div>
                                <input type="hidden" value="<?php echo \Core\Arr::get($data, 'latitude'); ?>" name="latitude">
                                <input type="hidden" value="<?php echo \Core\Arr::get($data, 'longitude'); ?>" name="longitude">
                                <input type="hidden" value="<?php echo \Core\Arr::get($data, 'zoom', 12); ?>" name="zoom">
                            </div>
                        </div>
                        <script src="http://maps.googleapis.com/maps/api/js?key=<?php echo \Core\Config::get('basic.googlemapskey'); ?>&sensor=false" type="text/javascript"></script>
                        <script type="text/javascript">
                            $('#address').val('');
                            var map = false;

                            var geocoder = false;

                            <?php if(!$data['longitude'] || !$data['latitude']): ?>
                            var myLatlng = new google.maps.LatLng(48.93982088660896, 31.46484375);
                            var zoom = 6;
                            var marker = false;
                            <?php else: ?>
                            var myLatlng = new google.maps.LatLng(parseFloat('<?php echo $data['latitude']; ?>'), parseFloat('<?php echo $data['longitude']; ?>'));
                            var zoom = parseInt($('input[name="zoom"]').val());
                            var marker = true;
                            <?php endif; ?>

                            function initialize() {
                                var mapOptions = {
                                    center: myLatlng,
                                    zoom: zoom,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                };
                                map = new google.maps.Map(document.getElementById("gmap"), mapOptions);
                                geocoder = new google.maps.Geocoder();
                                google.maps.event.addListener(map, 'zoom_changed', function(){
                                    $('input[name="zoom"]').val( map.zoom );
                                });
                            }

                            function bind_location(location) {
                                $('input[name="latitude"]').val( location.lat() );
                                $('input[name="longitude"]').val( location.lng() );
                                $('input[name="zoom"]').val( map.zoom );
                            }

                            function placeMarker(location) {
                                if (marker) {
                                    marker.setMap(null);
                                }
                                bind_location(location);
                                pointt = new google.maps.Point(18, 54);
                                marker = new google.maps.Marker({
                                    position: location,
                                    map: map,
                                    draggable: true
                                });
                                google.maps.event.addListener(marker, 'dragend', function(cc){
                                    bind_location(cc.latLng);
                                });
                            }

                            $(document).ready(function(){
                                initialize();
                                google.maps.event.addListener(map, 'click', function(cc){
                                    placeMarker(cc.latLng);
                                });

                                if(marker) {
                                    pointt = new google.maps.Point(18, 54);
                                    marker = new google.maps.Marker({
                                        position: myLatlng,
                                        map: map,
                                        draggable: true
                                    });
                                    google.maps.event.addListener(marker, 'dragend', function(cc){
                                        bind_location(cc.latLng);
                                    });
                                }

                                $("#address").autocomplete({
                                    //Определяем значение для адреса при геокодировании
                                    source: function(request, response) {
                                        geocoder.geocode( {'address': request.term}, function(results, status) {
                                            response($.map(results, function(item) {
                                                return {
                                                    label:  item.formatted_address,
                                                    value: item.formatted_address,
                                                    latitude: item.geometry.location.lat(),
                                                    longitude: item.geometry.location.lng()
                                                }
                                            }));
                                        })
                                    },
                                    select: function(event, ui) {
                                        $('input[name="latitude"]').val( ui.item.latitude );
                                        $('input[name="longitude"]').val( ui.item.longitude );
                                        $('input[name="zoom"]').val( map.zoom );
                                        var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
                                        map.setCenter(location);
                                        map.setZoom(14);
                                        placeMarker(location);
                                    }
                                });
                            });
                        </script>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="widget">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo __('Мета-данные'); ?>
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <?php echo \Forms\Builder::input([
                            'name' => 'FORM[h1]',
                            'value' => $obj->h1,
                        ], [
                            'text' => 'H1',
                            'tooltip' => __('Рекомендуется, чтобы тег h1 содержал ключевую фразу, которая частично или полностью совпадает с title'),
                        ]); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::input([
                            'name' => 'FORM[title]',
                            'value' => $obj->title,
                        ], [
                            'text' => 'Title',
                            'tooltip' => __('<p>Значимая для продвижения часть заголовка должна быть не более 12 слов</p><p>Самые популярные ключевые слова должны идти в самом начале заголовка и уместиться в первых 50 символов, чтобы сохранить привлекательный вид в поисковой выдаче.</p><p>Старайтесь не использовать в заголовке следующие знаки препинания – . ! ? – </p>'),
                        ]); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::textarea([
                            'name' => 'FORM[keywords]',
                            'rows' => 5,
                            'value' => $obj->keywords,
                        ], [
                            'text' => 'Keywords',
                        ]); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::textarea([
                            'name' => 'FORM[description]',
                            'value' => $obj->description,
                            'rows' => 5,
                        ], [
                            'text' => 'Description',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>