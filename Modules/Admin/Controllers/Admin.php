<?php

namespace Modules\Admin\Controllers;

use Core\Route;
use Core\HTTP;
use Core\View;
use Core\QB\DB;
use Core\Config;
use Modules\Base;
use Modules\Content\Models\Control;
use Modules\Admin\Models\Admin AS Model;

class Admin extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
    }

    public function indexAction() {
        if (Config::get('error')) {
            return false;
        }

        $user = DB::select()
                ->from('users1')
                ->where('id', '=', $_SESSION['user'])
                ->find();

        $userss = DB::select()
                ->from('users1')
                ->find_all();

        if ($user->login == 'adminP') {
            $this->_content = View::tpl([
                        'users' => $userss
                            ], 'Admin/Page');
        } else {
            HTTP::redirect('/');
        }
    }

    public function userAction() {
        if (Config::get('error')) {
            return false;
        }

        $id = Route::param('id');

        $user = DB::select()
                ->from('users1')
                ->where('id', '=', $_SESSION['user'])
                ->find();

        $userr = DB::select()
                ->from('users1')
                ->where('id', '=', $id)
                ->find();

        // Render template
        if ($user->login == 'adminP') {
            $this->_content = View::tpl([
                        'user' => $userr
                            ], 'Admin/User');
        } else {
            HTTP::redirect('/');
        }
    }

    public function addUserAction() {

        if (Config::get('error')) {
            return false;
        }

        $name = $_POST['name'];
        $login = $_POST['login'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $phone = $_POST['phone'];

        $result = Model::insertUser($name, $login, $email, $password, $phone);

        HTTP::redirect('/admin');
    }

}
