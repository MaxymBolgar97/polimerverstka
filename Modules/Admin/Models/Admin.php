<?php

namespace Modules\Admin\Models;

use Core\QB\DB;
use Core\Common;
use Core\User AS U;

class Admin extends Common {

    public static $table = 'users1';

    public static function insertUser($name, $login, $email, $password, $phone) {
        $result = Common::factory(static::$table)->insert([
            'name' => $name,
            'login' => $login,
            'email' => $email,
            'password' => U::factory()->hash_password($password),
            'phone' => $phone,
        ]);
        return $result;
    }

}
