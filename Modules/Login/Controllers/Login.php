<?php

namespace Modules\Login\Controllers;

use Core\Route;
use Core\View;
use Core\Config;
use Modules\Base;
use Modules\Content\Models\Control;
use Core\HTTP;
use Core\User;
use Modules\Login\Models\Login AS Model;
use Core\User AS U;

class Login extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
    }

    public function indexAction() {
        
        $email = $_POST['emailF'];
        $password = $_POST['passwordF'];
        $finded = false;

            if ($user = User::factory()->get_user_by_email($email, $password)) {
                $finded = true;
                User::factory()->auth($user);
                HTTP::redirect('/');
            }
        if (!$finded) {
            echo 'Вход не выполнен';
        }
        echo '<a href="/">На главную</a>';
    }
    
     public function logoutAction() {
         
         User::factory()->logout();
         HTTP::redirect('/');
     }

}