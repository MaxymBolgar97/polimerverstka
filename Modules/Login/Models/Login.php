<?php

namespace Modules\Login\Models;

use Core\QB\DB;
use Core\Common;

class Login extends Common {

    public static $table = 'users1';

    public static function getUsers() {
        $result = DB::select()
                ->from(static::$table);
        return $result->find_all();
    }

}
