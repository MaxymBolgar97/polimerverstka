<?php

namespace Modules\Sitemap\Controllers;

use Core\Route;
use Core\View;
use Core\Config;
use Modules\Base;
use Modules\Sitemap\Models\Sitemap AS Model;
use Modules\Content\Models\Control;

class Sitemap extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
    }

    public function indexAction() {
        if (Config::get('error')) {
            return false;
        }
        // Seo
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        Config::set('content_class', 'news_block');
        // Get Rows
        $result = Model::getSitemap();
        //canonicals settings
        // Render template
        $this->_content = View::tpl(['result' => $result], 'Sitemap');
    }

}
