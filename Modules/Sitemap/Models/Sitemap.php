<?php

namespace Modules\Sitemap\Models;

use Core\QB\DB;
use Core\Common;

class Sitemap extends Common {

    public static $table = 'sitemenuUSE';

    public static function getSitemap() {
        $result = DB::select()
                ->from(static::$table)
                ->order_by('sort');
        return $result->find_all();
    }

}
