<?php

namespace Modules\Reg\Models;

use Core\QB\DB;
use Core\Common;
use Core\User AS U;

class Reg extends Common {

    public static $table = 'users1';

    public static function insertUser($name, $login, $email, $password) {
        $result = Common::factory(static::$table)->insert([
            'name' => $name,
            'login' => $login,
            'email' => $email,
            'password' => U::factory()->hash_password($password),
        ]);
        return $result;
    }

}
