<?php

namespace Modules\Reg\Controllers;

use Core\Route;
use Core\View;
use Core\Config;
use Modules\Base;
use Modules\Content\Models\Control;
use Core\HTTP;
use Core\User;
use Modules\Reg\Models\Reg AS Model;
use Core\Message;

class Reg extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
    }

    public function indexAction() {
        $name = $_POST['nameF'];
        $login = $_POST['loginF'];
        $email = $_POST['emailF'];
        $password = $_POST['passwordF'];

        $result = Model::insertUser($name, $login, $email, $password);

        HTTP::redirect('/');
    }

}
