/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : osis_db

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2017-10-12 15:31:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `blog_rubrics1`
-- ----------------------------
DROP TABLE IF EXISTS `blog_rubrics1`;
CREATE TABLE `blog_rubrics1` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`sort`  int(10) NULL DEFAULT NULL ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`text`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`h1`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`keywords`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`status`  tinyint(1) NULL DEFAULT 0 ,
PRIMARY KEY (`id`),
UNIQUE INDEX `alias` (`alias`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=13

;

-- ----------------------------
-- Records of blog_rubrics1
-- ----------------------------
BEGIN;
INSERT INTO `blog_rubrics1` VALUES ('10', '1447090962', '1458160121', '1', 'Мода', 'moda', '<p>Мода - совокупность привычек и вкусов (в отношении одежды, туалета), господствующих в определённой общественной среде в определённое время.</p>', 'Мода', 'Мода', 'Мода', 'Мода', '1'), ('11', '1447090973', '1458160121', '2', 'Стиль', 'stil', '', 'Стиль', 'Стиль', 'Стиль', 'Стиль', '1'), ('12', '1447091013', '1483517879', '0', 'Домашние дела', 'domashnie-dela', '', 'Домашние дела', 'Домашние дела', 'Домашние дела', 'Домашние дела', '1');
COMMIT;

-- ----------------------------
-- Table structure for `brands1`
-- ----------------------------
DROP TABLE IF EXISTS `brands1`;
CREATE TABLE `brands1` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`status`  tinyint(1) NOT NULL DEFAULT 0 ,
`sort`  int(10) NULL DEFAULT 0 ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`text`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`h1`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`keywords`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`views`  int(10) NOT NULL DEFAULT 0 ,
`image`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
UNIQUE INDEX `alias` (`alias`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=47

;

-- ----------------------------
-- Records of brands1
-- ----------------------------
BEGIN;
INSERT INTO `brands1` VALUES ('29', '1447092819', null, '1', '0', 'Nike', '', 'nike', 'Nike', 'Nike', 'Nike', 'Nike', '0', null), ('30', '1447092830', null, '1', '0', 'New Balance', '', 'newbalance', 'New Balance', 'New Balance', 'New Balance', 'New Balance', '0', null), ('31', '1447092841', null, '1', '0', 'Benetton', '', 'benetton', 'Benetton', 'Benetton', 'Benetton', 'Benetton', '0', null), ('32', '1447092852', null, '1', '0', 'H&M', '', 'hm', 'H&M', 'H&M', 'H&M', 'H&M', '0', null), ('33', '1447092863', null, '1', '0', 'Next', '', 'next', 'Next', 'Next', 'Next', 'Next', '0', null), ('34', '1453976736', '1492434270', '1', '0', 'Aldo Brue', '', 'aldobrue', '', '', '', '', '0', null), ('35', '1453976759', null, '1', '0', 'Fabric Frontline', '', 'fabricfrontline', '', '', '', '', '0', null), ('36', '1453976774', null, '1', '0', 'Caf', '', 'caf', '', '', '', '', '0', null), ('37', '1453978318', null, '1', '0', 'Dolce & Gabbana', '', 'dolcegabbana', '', '', '', '', '0', null), ('38', '1453978328', null, '1', '0', 'Erdem', '', 'erdem', '', '', '', '', '0', null), ('39', '1453978341', null, '1', '0', 'Lancome', '', 'lancome', '', '', '', '', '0', null), ('40', '1453978382', null, '1', '0', 'Moorer', '', 'moorer', '', '', '', '', '0', null), ('41', '1453978395', null, '1', '0', 'Passarella Death Squad', '', 'passarelladeathsquad', '', '', '', '', '0', null), ('42', '1453978406', null, '1', '0', 'Valentino', '', 'valentino', '', '', '', '', '0', null), ('43', '1453978419', null, '1', '0', 'Victoria Beckham', '', 'victoriabeckham', '', '', '', '', '0', null), ('44', '1453978442', null, '1', '0', 'Stizzoli', '', 'stizzoli', '', '', '', '', '0', null), ('45', '1453978456', '1453982939', '1', '0', 'Two Women In The World', '', 'twowomenintheworld', 'Two Women In The World', 'Two Women In The World', 'Two Women In The World', 'Two Women In The World', '0', null), ('46', '1462882036', '1462882121', '1', '0', 'Puma ', '', 'puma', '', '', '', '', '0', null);
COMMIT;

-- ----------------------------
-- Table structure for `carts`
-- ----------------------------
DROP TABLE IF EXISTS `carts`;
CREATE TABLE `carts` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`hash`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`user_id`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`user_id`) REFERENCES `users1` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `hash` (`hash`) USING BTREE ,
INDEX `user_id` (`user_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=22

;

-- ----------------------------
-- Records of carts
-- ----------------------------
BEGIN;
INSERT INTO `carts` VALUES ('12', '1507801783', null, '56f0ec898f6e8b04d52669e92deda6e8bdc50ce2', null), ('13', '1507807170', null, 'a076b072c87c9daaa90034a4a218323e670ce138', '43'), ('14', '1507807285', null, '650dc31006140242891822396253f92e9771f36f', '42'), ('15', '1507807297', null, '85b441be1a31a96794a4a6d265f36dafe2cc460d', '44');
COMMIT;

-- ----------------------------
-- Table structure for `carts_items`
-- ----------------------------
DROP TABLE IF EXISTS `carts_items`;
CREATE TABLE `carts_items` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`cart_id`  int(10) NOT NULL ,
`catalog_id`  int(10) NOT NULL ,
`count`  int(6) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `id_cart` (`cart_id`) USING BTREE ,
INDEX `id_goods` (`catalog_id`) USING BTREE ,
INDEX `cart_id` (`cart_id`) USING BTREE ,
INDEX `catalog_id` (`catalog_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of carts_items
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `catalog`
-- ----------------------------
DROP TABLE IF EXISTS `catalog`;
CREATE TABLE `catalog` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`status`  tinyint(1) NOT NULL DEFAULT 0 ,
`sort`  int(10) NOT NULL DEFAULT 0 ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`parent_id`  int(10) NULL DEFAULT 0 ,
`new`  tinyint(1) NOT NULL DEFAULT 0 ,
`sale`  tinyint(1) NOT NULL DEFAULT 0 ,
`top`  tinyint(1) NOT NULL DEFAULT 0 ,
`available`  tinyint(1) NOT NULL DEFAULT 1 ,
`h1`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`keywords`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`cost`  int(6) NOT NULL DEFAULT 0 ,
`cost_old`  int(6) NOT NULL DEFAULT 0 ,
`artikul`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`views`  int(10) NOT NULL DEFAULT 0 ,
`brand_alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`model_alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`image`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`specifications`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`brand_alias`) REFERENCES `brands1` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE,
FOREIGN KEY (`model_alias`) REFERENCES `models1` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE,
FOREIGN KEY (`image`) REFERENCES `catalog_images1` (`image`) ON DELETE SET NULL ON UPDATE CASCADE,
FOREIGN KEY (`parent_id`) REFERENCES `catalog_tree1` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
UNIQUE INDEX `alias` (`alias`) USING BTREE ,
INDEX `parent_id` (`parent_id`) USING BTREE ,
INDEX `cat_brand_alias` (`brand_alias`) USING BTREE ,
INDEX `cat_model_alias` (`model_alias`) USING BTREE ,
INDEX `image_link` (`image`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=164

;

-- ----------------------------
-- Records of catalog
-- ----------------------------
BEGIN;
INSERT INTO `catalog` VALUES ('40', '1447094647', '1454059240', '1', '1', 'Куртка Blend', 'kurtka-blend', '29', '1', '0', '0', '1', '', '', '', '', '2810', '0', '1', '8', 'benetton', null, '31c79213d5aceed90eed954307c44416.jpg', '{\"material\":[\"nejlon\",\"poliestr\",\"silikon\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"italija\",\"sezon\":[\"zima\"],\"tsvet\":\"chernyj\"}'), ('41', '1447094746', '1454059253', '1', '2', 'Brave Soul', 'brave-soul', '29', '1', '0', '0', '1', '', '', '', '', '2210', '0', '2', '2', 'next', null, '3faf38fccbe35f31b603141be04317f3.jpg', '{\"material\":[\"kozha\",\"silikon\",\"hlopok\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"zima\"],\"tsvet\":\"sinij\"}'), ('42', '1447094856', '1454059257', '1', '3', 's.Oliver', 'soliver', '29', '0', '0', '1', '1', '', '', '', '', '2310', '0', '3', '2', 'benetton', null, 'fe75cb26c4ae4c06af7d7fdef455fe1b.jpg', '{\"material\":[\"nejlon\",\"sintetika\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"zima\"],\"tsvet\":\"green\"}'), ('43', '1447095158', '1461879646', '1', '1', 'Mudo', 'mudo', '30', '1', '0', '0', '0', '', '', '', '', '312', '0', '4', '4', 'hm', null, '21f15ca55231a0db69dad9a2f1c6d2cd.jpg', '{\"material\":[\"poliestr\",\"polimer\",\"sintetika\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('44', '1447095265', '1453987283', '1', '2', 'Only & Sons', 'only--sons', '30', '0', '0', '1', '2', '', '', '', '', '890', '820', '5', '53', 'next', null, '43c658475e0a09cb4fc1c63b46bdc5a9.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"leto\"],\"tsvet\":\"white\"}'), ('45', '1447095348', '1453986770', '1', '1', 'Top Secret', 'top-secret', '31', '0', '1', '0', '1', '', '', '', '', '850', '950', '6', '8', 'next', null, '85b47014b4b719452a06dc924426491f.jpg', '{\"material\":[\"nubuk\",\"poliestr\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"osen\"],\"tsvet\":\"korichnevyj\"}'), ('46', '1447095545', '1454061160', '1', '1', 'Patrol', 'patrol', '32', '0', '1', '0', '1', '', '', '', '', '875', '1030', '7', '155', 'newbalance', '1300', '6891ab8f29e5eeded563f514d42279f4.jpg', '{\"material\":[\"zamsha\",\"iskusstvennaja-kozha\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"zima\",\"leto\"],\"tsvet\":\"chernyj\"}'), ('47', '1447095635', '1491228017', '1', '2', 'Dirk Bikkembergs', 'dirk-bikkembergs', '32', '0', '0', '0', '0', '', '', '', '', '1270', '0', '8', '6', 'newbalance', '1400', '4454777def4b6cf80459a15566e23770.jpg', '{\"material\":[\"kozha\",\"sintetika\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"white\"}'), ('48', '1447095826', '1485439373', '1', '3', 'Reebok Classics', 'reebok-classics', '32', '0', '0', '0', '2', '', '', '', '', '1450', '0', '9', '335', 'nike', 'airfoamposite', 'c2c9d0b441f241040ff329880f7840e5.jpg', '{\"material\":[\"kozha\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"green\"}'), ('49', '1447095908', '1454006747', '1', '1', 'Burton Menswear London', 'burton-menswear-london', '33', '0', '0', '0', '0', '', '', '', '', '2860', '0', '10', '4', 'benetton', null, 'd56dbeb19c27a828e5762b1aabd956a2.jpg', '{\"material\":[\"kozha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('50', '1447095980', '1458206606', '1', '1', 'Vitacci', 'vitacci', '34', '1', '0', '0', '1', '', '', '', '', '1300', '0', '11', '8', 'nike', 'airjordan', '147378e984f87fbc42a0b80bd52899a2.jpg', '{\"material\":[\"zamsha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"leto\"],\"tsvet\":\"sinij\"}'), ('51', '1447096085', '1461223417', '1', '1', 'Quiksilver', 'quiksilver', '35', '1', '0', '0', '1', '', '', '', '', '1740', '0', '12', '4', 'hm', null, '528d7c85efd8247ba056e4a1c29b4bca.jpg', '{\"material\":[\"nejlon\",\"nubuk\",\"poliestr\",\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"korichnevyj\"}'), ('52', '1447096156', '1507542089', '1', '2', 'David Jones', 'david-jones', '35', '1', '0', '0', '1', '', '', '', '', '1500', '0', '13', '6', 'hm', null, '3e402891cdc71ca41bc7f7f09e042d4b.jpg', '{\"material\":[\"kozha\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"fioletovyj\"}'), ('53', '1447096237', '1454061351', '1', '3', 'A-Personality', 'a-personality', '35', '0', '0', '1', '1', '', '', '', '', '400', '0', '14', '1', 'benetton', null, 'ffeee2baa0eb681477b1b4ae95c92e79.jpg', '{\"material\":[\"iskusstvennaja-kozha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('54', '1447096327', '1492428101', '1', '1', 'Oakley', 'oakley', '36', '1', '0', '0', '1', '', '', '', '', '1701', '0', '15', '9', 'hm', null, '3b3c8e2b8b4c89d1fbb12c3e12de9d5a.jpg', '{\"material\":[\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"leto\"],\"tsvet\":\"korichnevyj\"}'), ('55', '1447096428', '1456335230', '1', '2', 'Polaroid', 'polaroid', '36', '1', '0', '0', '1', '', '', '', '', '756', '0', '16', '5', 'hm', null, 'ff92a4371fdb1b55022573290fc5ad2f.jpg', '{\"material\":[\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"leto\"],\"tsvet\":\"sinij\"}'), ('56', '1453972719', '1454076898', '1', '2', 'Regular Fit Jeans', 'regular-fit-jeans', '31', '0', '0', '1', '1', '', '', '', '', '1100', '0', '5', '3', 'nike', 'airforce', '9bd9e2c0cb862c21ba8b33160fa544b7.jpg', '{\"material\":[\"lateks\",\"nejlon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"sinij\"}'), ('57', '1453972822', '1453987499', '1', '3', 'Regular Slim Jeans', 'regular-slim-jeans', '31', '0', '0', '1', '1', '', '', '', '', '1450', '1200', '7', '2', 'hm', null, '72db86cef6b5e1901bd0dabf72b1735e.jpg', '{\"material\":[\"sintetika\",\"tekstil\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"seryj\"}'), ('58', '1453972990', '1492435428', '1', '4', 'Regular Jeans', 'regular-jeans', '31', '1', '0', '0', '1', '', '', '', '', '1270', '0', '5', '5', 'next', null, '46d9d8f8e3be8cb4f5049aa0b11b796c.jpg', '{\"tsvet\":\"sinij\",\"sezon\":[\"leto\",\"vesna\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"indonezija\",\"material\":[\"polimer\",\"silikon\"]}'), ('59', '1453973163', '1457985925', '1', '5', 'Jeans', 'jeans', '31', '1', '0', '0', '1', '', '', '', '', '800', '0', '4', '2', 'hm', null, 'feee3908006fdc4ab715cd327d00a745.jpg', '{\"material\":[\"nubuk\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"light_blue\"}'), ('60', '1453973235', '1464802566', '1', '6', 'FitJeans', 'fitjeans', '31', '1', '0', '0', '0', '', '', '', '', '980', '0', '5', '5', 'nike', 'airforce', '371f58a9be9b8faba677741ac6b12050.jpg', '{\"material\":[\"lateks\",\"nejlon\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"0\"}'), ('61', '1453973760', '1463061442', '1', '1', 'VAN HEUSEN', 'van-heusen', '30', '1', '0', '0', '1', '', '', '', '', '500', '0', '1', '4', 'benetton', null, '744f8a702159b78c379ed35dc8200eb9.jpg', '{\"material\":[\"lateks\",\"polimer\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"white\"}'), ('62', '1453973836', '1461250728', '1', '2', 'TOOTAL', 'tootal', '30', '1', '0', '0', '1', '', '', '', '', '670', '0', '2', '2', 'hm', null, '8aafb240eb2626fb201353890a8d9035.jpg', '{\"material\":[\"nubuk\",\"silikon\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"sinij\"}'), ('63', '1453973886', '1453987345', '1', '3', 'OXFORD', 'oxford', '30', '0', '0', '1', '1', '', '', '', '', '490', '0', '3', '0', 'next', null, '69fb49c55e1dbae657920116c3121aca.jpg', '{\"material\":[\"nejlon\",\"poliestr\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'), ('64', '1453973952', '1453987430', '1', '4', 'MORLEY', 'morley', '30', '0', '1', '0', '1', '', '', '', '', '600', '680', '4', '0', 'nike', null, 'e88ca78f3d83a09ec4d9a087f7488465.jpg', '{\"material\":[\"nejlon\",\"poliestr\",\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"light_blue\"}'), ('65', '1453974019', '1461250735', '1', '5', 'SHIRTING', 'shirting', '30', '0', '1', '0', '1', '', '', '', '', '500', '530', '5', '5', 'benetton', null, '343f2ed7a53c89842f76680def0ff912.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"nejlon\",\"polimer\",\"tekstil\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"green\"}'), ('66', '1453974080', '1453987621', '1', '7', 'AERTEX', 'aertex', '30', '0', '0', '0', '2', '', '', '', '', '549', '0', '7', '0', 'nike', 'airhuarache', '4000f9750e94224c62a0c5a4e155a753.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"kozha\",\"polimer\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"light_blue\"}'), ('67', '1453974669', '1454059251', '1', '1', 'Marnelly', 'marnelly', '29', '0', '1', '0', '1', '', '', '', '', '2500', '3050', '1', '1', 'hm', null, 'cf92f6be7ef042db8506fd217337ec04.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"polimer\",\"silikon\",\"tekstil\",\"hlopok\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'), ('68', '1453974739', '1454059255', '1', '2', 'MIRAGE', 'mirage', '29', '0', '1', '0', '1', '', '', '', '', '2300', '2570', '2', '1', 'hm', null, '9b2bd4df4c45164ff451721eec1393a4.jpg', '{\"material\":[\"nejlon\",\"poliestr\",\"polimer\",\"sintetika\",\"tekstil\",\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('69', '1453974795', '1454059259', '1', '3', 'Sinta', 'sinta', '29', '0', '0', '0', '2', '', '', '', '', '2499', '0', '3', '1', 'benetton', null, 'af5e3e1d7e0cefb9d417502078673523.jpg', '{\"material\":[\"zamsha\",\"kozha\",\"nejlon\",\"nubuk\",\"sintetika\",\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'), ('70', '1453975300', '1454009693', '1', '1', 'GANT', 'gant', '34', '1', '0', '0', '1', '', '', '', '', '1500', '0', '1', '5', 'benetton', null, '0d8ad8265bc795e5f0b1e46f1872f01d.jpg', '{\"material\":[\"iskusstvennaja-zamsha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"turquoise\"}'), ('71', '1453975356', '1454004432', '1', '2', 'Alberto Guardiani', 'alberto-guardiani', '34', '0', '0', '1', '1', '', '', '', '', '1200', '0', '2', '1', 'hm', null, '99d4ab2baa6efb497393b8caa546b7d2.jpg', '{\"material\":[\"zamsha\",\"nubuk\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"0\"}'), ('72', '1453975422', '1454003835', '1', '3', 'Animas Code', 'animas-code', '34', '0', '0', '1', '1', '', '', '', '', '1400', '0', '3', '5', 'newbalance', '1400', '0e6af6118a01535c6d6b8cb9b9b1cc95.jpg', '{\"material\":[\"kozha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'), ('73', '1453975543', '1454006743', '1', '1', 'Baldinini', 'baldinini', '33', '0', '1', '0', '1', '', '', '', '', '800', '900', '1', '0', 'benetton', null, 'f61df09878d457ee66fc7e1fd14addbd.jpg', '{\"material\":[\"nejlon\",\"nubuk\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"krasnyj\"}'), ('74', '1453975599', '1454006751', '1', '2', 'Bally', 'bally', '33', '0', '0', '0', '2', '', '', '', '', '1300', '0', '2', '0', 'newbalance', '1300', '2b58dd8bf1316ed14ede88d7fe1a9a92.jpg', '{\"material\":[\"zamsha\",\"nubuk\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'), ('75', '1453975667', '1454006756', '1', '3', 'Barracuda', 'barracuda', '33', '0', '0', '0', '0', '', '', '', '', '2100', '0', '3', '0', null, null, '6acb9c32679cf8b76b7d4a1138f4ea24.jpg', '{\"material\":[\"kozha\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"beige\"}'), ('76', '1453975845', '1458248564', '1', '1', 'Sneakers Versace medusa', 'sneakers-versace-medusa', '32', '0', '1', '0', '1', '', '', '', '', '1100', '1300', '1', '4', 'nike', 'airforce', 'aa1d555bcf57d51b72b13f70e12d5036.png', '{\"material\":[\"zamsha\",\"iskusstvennaja-zamsha\",\"iskusstvennaja-kozha\",\"kozha\",\"lateks\",\"nejlon\",\"nubuk\",\"poliestr\",\"polimer\",\"silikon\",\"sintetika\",\"tekstil\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"light_blue\"}'), ('77', '1453975896', '1490015486', '1', '2', 'Rick Owens', 'rick-owens', '32', '0', '0', '0', '0', '', '', '', '', '1200', '970', '2', '3', 'newbalance', null, 'ac95a9367b31d93cf5c8f6e5bf1a34de.jpg', '{\"material\":[\"nejlon\",\"silikon\",\"tekstil\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"white\"}'), ('78', '1453975946', '1454061169', '1', '3', 'Kanye West Yeezy 750 Boost Low', 'kanye-west-yeezy-750-boost-low', '32', '0', '0', '0', '2', '', '', '', '', '1000', '880', '3', '2', 'newbalance', '1400', 'f30fe5c63fd4f0f9e382e275606aa7e5.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('79', '1453976111', '1471585831', '1', '1', 'Ray Ban Wayfarer', 'ray-ban-wayfarer', '36', '0', '1', '0', '1', '', '', '', '', '395', '790', '1', '3', 'benetton', null, '9286138925af6f01592d369df039cffc.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"poliestr\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('80', '1453976168', '1454062780', '1', '2', 'Ray Ban Aviator', 'olivia', '36', '0', '1', '1', '1', '', '', '', '', '395', '790', '2', '0', 'hm', null, '27a7915a23137d61f99bc2a20df0dc61.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"sintetika\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('81', '1453976216', '1454062891', '1', '3', 'GANT', 'gant1701', '36', '0', '0', '0', '1', '', '', '', '', '504', '0', '3', '0', 'benetton', null, '3e7b9e0e1683ca0fe9a851c4c3051520.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"nubuk\",\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('82', '1453976329', '1462888568', '1', '1', 'Женская сумка D&G', 'zhenskaja-sumka-dg', '35', '0', '0', '1', '1', '', '', '', '', '890', '0', '1', '4', 'benetton', null, '81734926419a4401f8322ece3a5fe691.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"kozha\",\"poliestr\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"yellow\"}'), ('83', '1453976392', '1454061671', '1', '2', 'Женская сумка D&G', 'zhenskaja-sumka-dg6134', '35', '0', '1', '0', '1', '', '', '', '', '1300', '1500', '2', '0', 'hm', null, '06020e5f548b14bbe0d3cddd1ddb9609.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"lateks\",\"polimer\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"white\"}'), ('84', '1453982705', '1453986781', '1', '1', 'Two Women In The World', 'two-women-in-the-world', '31', '0', '1', '0', '1', '', '', '', '', '680', '709', '1', '1', 'twowomenintheworld', null, '878a7e4e4ce127d11d0f01e49b165543.jpg', '{\"material\":[\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"sinij\"}'), ('85', '1453985288', '1464611176', '1', '1', 'Paige', 'paige', '31', '0', '0', '0', '0', '', '', '', '', '1645', '0', '1', '10', 'passarelladeathsquad', null, '1e45fd1d34c616984a94829c43c9d251.jpg', '{\"material\":[\"poliestr\",\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"]}'), ('86', '1453985577', '1453986790', '1', '1', 'Victoria Beckham', 'victoria-beckham', '31', '0', '0', '0', '0', '', '', '', '', '1500', '0', '1', '0', 'victoriabeckham', null, '8de18a7355287270b3df43c05a1048c0.jpg', '{\"material\":[\"poliestr\",\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"0\"}'), ('87', '1453985693', '1465034664', '1', '1', 'Victoria Beckham', 'victoria-beckham2339', '31', '0', '0', '0', '2', '', '', '', '', '1405', '0', '1', '1', 'victoriabeckham', null, '2130622858813fec5259b99f8b452e75.jpg', '{\"material\":[\"poliestr\",\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"0\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'), ('88', '1453988109', '1454059290', '1', '1', 'Burberry Brit', 'burberry-brit', '29', '0', '0', '0', '2', '', '', '', '', '1680', '0', '1', '3', 'hm', null, '650e1c2c39fcd2db0fe5552ce0a96813.jpg', '{\"material\":[\"zamsha\",\"hlopok\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"zima\",\"osen\"],\"tsvet\":\"sinij\"}'), ('89', '1453999405', '1454004747', '1', '1', 'Burberry', 'burberry', '34', '0', '1', '0', '1', '', '', '', '', '800', '970', '1', '2', 'newbalance', null, 'e16e898eaab10b2a8288cc73e4da458c.jpg', '{\"material\":[\"zamsha\",\"nubuk\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"chernyj\"}'), ('90', '1453999511', '1454004745', '1', '1', 'Dolce & Gabbana', 'dolce--gabbana', '34', '0', '1', '0', '1', '', '', '', '', '1000', '1350', '1', '6', 'benetton', null, '996c9737bb19cec0cdf8545920a35024.jpg', '{\"material\":[\"kozha\",\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"light_blue\"}'), ('91', '1454004856', '1454004928', '1', '1', 'Gucci', 'gucci', '34', '0', '0', '0', '2', '', '', '', '', '1590', '0', '1', '1', 'newbalance', null, '1a89bd77c02b30bb960a2758628ff129.jpg', '{\"material\":[\"kozha\",\"sintetika\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"leto\"],\"tsvet\":\"sinij\"}'), ('92', '1454005034', '1464611193', '1', '1', 'Bottega Veneta', 'bottega-veneta', '34', '0', '0', '0', '0', '', '', '', '', '1354', '0', '1', '4', 'hm', null, '27386468296175ab291d15df6925661d.jpg', '{\"material\":[\"zamsha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"coral\"}'), ('93', '1454005265', '1454005359', '1', '1', 'Hermes', 'hermes', '34', '0', '0', '0', '0', '', '', '', '', '1654', '0', '1', '0', 'nike', null, '222d27610fe00ae41888d76765c172b5.jpg', '{\"material\":[\"zamsha\",\"nubuk\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"korichnevyj\"}'), ('94', '1454005514', '1454005532', '1', '1', 'Lacoste', 'lacoste', '34', '0', '0', '0', '2', '', '', '', '', '800', '0', '1', '1', null, null, '0123777411cf5317bb713d9afdc3c9c1.jpg', '{\"material\":[\"iskusstvennaja-zamsha\",\"poliestr\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"leto\"],\"tsvet\":\"beige\"}'), ('95', '1454006167', '1459953214', '1', '1', 'Christian Louboutin', 'christian-louboutin', '33', '0', '1', '0', '1', '', '', '', '', '1987', '2133', '1', '1', 'newbalance', null, 'aedca7492b0710de73e4d3553a0b317c.jpg', '{\"material\":[\"zamsha\",\"poliestr\",\"sintetika\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"leto\"],\"tsvet\":\"sinij\"}'), ('96', '1454006239', '1455883752', '1', '1', 'Christian Louboutin', 'christian-louboutin5172', '33', '0', '0', '1', '1', '', '', '', '', '1679', '0', '1', '2', 'benetton', null, '296c2f7b532ab1905d9813cd4843d955.jpg', '{\"material\":[\"zamsha\",\"poliestr\",\"silikon\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"zima\"],\"tsvet\":\"chernyj\"}'), ('97', '1454006518', '1454006721', '1', '1', 'Hermes', 'hermes8759', '33', '0', '0', '1', '1', '', '', '', '', '1733', '0', '1', '0', 'nike', null, 'f733af1bc7c73d6def2e19d94fcebd15.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('98', '1454006577', '1460018408', '1', '1', 'Phillip Plein', 'phillip-plein', '33', '1', '0', '0', '1', '', '', '', '', '987', '0', '1', '2', 'benetton', null, 'ce1d58dc0da917077043c1edf6de746a.jpg', '{\"material\":[\"iskusstvennaja-zamsha\",\"kozha\",\"nubuk\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"0\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"0\"}'), ('99', '1454006650', '1454006713', '1', '0', 'Christian Louboutin', 'christian-louboutin6404', '33', '1', '0', '0', '1', '', '', '', '', '1267', '0', '', '0', 'nike', null, 'eb148d52f4cc6c272ee88bacf2776732.jpg', '{\"material\":[\"zamsha\",\"iskusstvennaja-zamsha\",\"polimer\",\"sintetika\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"0\"}'), ('100', '1454058542', '1454058563', '1', '0', 'Dior 2015', 'dior-2015', '30', '0', '0', '0', '2', '', '', '', '', '845', '0', '', '1', 'hm', null, '400f456673efc583825c497288dbdca7.jpg', '{\"material\":[\"tekstil\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"white\"}'), ('101', '1454058665', '1463997612', '1', '0', 'Alexander McQueen', 'alexander-mcqueen', '30', '0', '0', '0', '0', '', '', '', '', '750', '0', '', '4', 'benetton', null, 'bd1b148c4d131aaa82c04b236c01de3c.jpg', '{\"material\":[\"polimer\",\"sintetika\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"osen\"],\"tsvet\":\"0\"}'), ('102', '1454058981', '1454059244', '1', '0', 'Dsquared', 'dsquared', '29', '0', '0', '0', '0', '', '', '', '', '2400', '0', '', '2', 'next', null, 'c75370b832affc3be9d433178f864ead.jpg', '{\"material\":[\"kozha\",\"poliestr\",\"tekstil\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('103', '1454059079', '1454059238', '1', '0', 'Burberry', 'burberry5235', '29', '0', '0', '0', '1', '', '', '', '', '1220', '0', '', '1', 'benetton', null, '73487bc7d72b9ca7f581b1e2c87ed78d.jpg', '{\"material\":[\"zamsha\",\"kozha\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\"],\"tsvet\":\"chernyj\"}'), ('104', '1454059182', '1457985913', '1', '0', 'Burberry кожаная', 'burberry-kozhanaja', '29', '0', '0', '0', '0', '', '', '', '', '850', '0', '', '2', 'hm', null, '348b0a5a662583ff1543ba0703034249.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"lateks\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('105', '1454059444', '1454059512', '1', '0', 'Hermes', 'hermes5183', '33', '0', '0', '0', '1', '', '', '', '', '1580', '0', '', '1', 'benetton', null, '3ee3635b29378b2c1b92d0ee8a8262ae.jpg', '{\"material\":[\"kozha\",\"nubuk\",\"sintetika\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\"],\"tsvet\":\"korichnevyj\"}'), ('106', '1454060603', '1454061145', '1', '0', 'Dsquared2', 'dsquared2', '32', '0', '0', '1', '1', '', '', '', '', '1200', '0', '', '0', 'newbalance', null, 'bcedcb826fce171cb73c27dcf27e2b85.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"silikon\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\"],\"tsvet\":\"white\"}'), ('107', '1454060703', '1464168802', '1', '0', 'Nike чёрные', 'nike-chernye', '32', '0', '0', '1', '1', '', '', '', '', '984', '0', '', '2', 'nike', null, '1e2e2f992f6e9d1aad557278cd9d56b7.jpg', '{\"material\":[\"zamsha\",\"nejlon\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"zima\",\"osen\"],\"tsvet\":\"sinij\"}'), ('108', '1454060834', '1479218247', '1', '0', 'D&G серые', 'dg-serye', '32', '1', '0', '0', '1', '', '', '', '', '1640', '0', '', '2', 'newbalance', null, 'a4211640da4bb4ad88bf464938bdda63.jpg', '{\"material\":[\"kozha\",\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"italija\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'), ('109', '1454060948', '1485500892', '1', '0', 'Rick Owens', 'rick-owens8331', '32', '1', '0', '0', '1', '', '', '', '', '1400', '0', '', '3', 'newbalance', null, '40d2333010aa10e1f683eaee65ffeb1f.jpg', '{\"material\":[\"kozha\",\"nubuk\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('110', '1454061760', '1507552586', '1', '0', 'Женская сумка Dior', 'zhenskaja-sumka-dior', '35', '0', '1', '0', '1', '', '', '', '', '1800', '2000', '', '1', 'hm', null, '8243a32288f42683b19c2e6246d32434.jpg', '{\"material\":[\"kozha\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('111', '1454061862', '1454061926', '1', '0', 'Женская сумка Furla', 'zhenskaja-sumka-furla', '35', '0', '0', '0', '2', '', '', '', '', '1360', '0', '', '0', 'benetton', null, '2bc9719c017326e92508f46b404f342f.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"nubuk\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"rozovyj\"}'), ('112', '1454062022', '1454062063', '1', '0', 'Мужская сумка Fred Perry', 'muzhskaja-sumka-fred-perry', '35', '0', '0', '0', '2', '', '', '', '', '1120', '0', '', '0', 'hm', null, '82a09f8eae1e2e17f6f4d38a116fe1f9.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"nejlon\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"korichnevyj\"}'), ('113', '1454062111', '1454062156', '1', '0', 'Мужская сумка Fred Perry', 'muzhskaja-sumka-fred-perry5223', '35', '0', '0', '0', '0', '', '', '', '', '1227', '0', '', '0', 'hm', null, '027e8a2609c1af3e188c8638f016fe97.jpg', '{\"material\":[\"kozha\",\"poliestr\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'), ('114', '1454062198', '1454062273', '1', '0', 'Мужская сумка на пояс D&G', 'muzhskaja-sumka-na-pojas-dg', '35', '0', '0', '0', '0', '', '', '', '', '1413', '0', '', '0', 'hm', null, '992e36604939e00adee689ee475add0a.jpg', '{\"material\":[\"nubuk\",\"polimer\",\"sintetika\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('115', '1454063053', '1456335246', '1', '0', 'Porsche Design', 'porsche-design', '36', '0', '0', '1', '1', '', '', '', '', '618', '0', '', '1', 'hm', null, '5bb962a05bf5a36940a77ba77e496ac6.jpg', '{\"material\":[\"iskusstvennaja-zamsha\",\"kozha\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('116', '1454063205', '1460022189', '1', '0', 'Cartier', 'cartier', '36', '0', '0', '1', '1', '', '', '', '', '890', '0', '', '1', 'benetton', null, 'bf2ae8500081b45c16b214f51e50d39e.jpg', '{\"material\":[\"kozha\",\"lateks\",\"nubuk\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('117', '1454063317', '1461219408', '1', '0', 'Ferrari', 'ferrari', '36', '0', '0', '0', '0', '', '', '', '', '796', '0', '', '2', 'hm', null, '3d087c18ee28a88fc99dc78933d842a1.jpg', '{\"material\":[\"kozha\",\"nubuk\",\"poliestr\",\"sintetika\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('118', '1454063440', '1454063488', '1', '0', 'Porsche Design', 'porsche-design9374', '36', '0', '0', '0', '0', '', '', '', '', '619', '0', '', '0', 'benetton', null, '69efd778c57c7f5e8561eec011853107.jpg', '{\"material\":[\"kozha\",\"lateks\",\"polimer\",\"silikon\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('119', '1454063524', '1454323431', '1', '0', 'Мужские очки Модель 2368c2', 'muzhskie-ochki-model-2368c2', '36', '0', '0', '0', '2', '', '', '', '', '589', '0', '', '2', 'benetton', null, '92f3b935559751b2265a53c2fecf62ef.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"silikon\",\"tekstil\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"korichnevyj\"}'), ('120', '1454319163', '1461219368', '1', '0', 'Джинсы Philipp Plein', 'dzhinsy-philipp-plein', '31', '0', '0', '0', '1', '', '', '', '', '1368', '0', '', '2', 'passarelladeathsquad', null, 'c0fec459280ee93b12b4e4a711705309.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"poliestr\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"zima\",\"osen\"],\"tsvet\":\"sinij\"}'), ('121', '1454319233', '1454319250', '1', '0', 'Джинсы мужские Balmain', 'dzhinsy-muzhskie-balmain', '31', '0', '0', '0', '2', '', '', '', '', '2500', '0', '', '0', 'nike', null, '971c2e8906aa445de06e2a0bf33f0b87.jpg', '{\"material\":[\"zamsha\",\"poliestr\",\"tekstil\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"0\"}'), ('122', '1454319307', '1492434833', '1', '0', 'Джинсы мужские Dsquared', 'dzhinsy-muzhskie-dsquared', '31', '1', '0', '0', '1', '', '', '', '', '1368', '0', '', '3', 'next', null, '8c7b7cecca6ff092982454f7334ab879.jpg', '{\"tsvet\":\"0\",\"sezon\":[\"zima\",\"osen\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"vetnam\",\"material\":[\"iskusstvennajazamsha\",\"iskusstvennajakozha\",\"nejlon\",\"sintetika\"]}'), ('123', '1454319356', '1454319373', '1', '0', 'Джинсы мужские Versace', 'dzhinsy-muzhskie-versace', '31', '0', '0', '1', '1', '', '', '', '', '1392', '0', '', '0', 'hm', null, 'a0977a205a70991491100a55f09d1368.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"seryj\"}'), ('124', '1454319406', '1454319419', '1', '0', 'Женские джинсы Dsquared', 'zhenskie-dzhinsy-dsquared', '31', '0', '0', '0', '1', '', '', '', '', '1467', '0', '', '0', 'twowomenintheworld', null, '00538151c9f5b0c2548a2dd7aff6339e.jpg', '{\"material\":[\"lateks\",\"silikon\",\"tekstil\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"zima\",\"osen\"],\"tsvet\":\"0\"}'), ('125', '1454319828', '1454319865', '1', '0', 'Мужская рубашка McQueen', 'muzhskaja-rubashka-mcqueen', '30', '0', '0', '0', '1', '', '', '', '', '1008', '0', '', '0', 'benetton', null, '59d38fdc0ca405a5b33d3c7841fd842b.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"krasnyj\"}'), ('126', '1454319898', '1460016736', '1', '0', 'Рубашка Dior Homme', 'rubashka-dior-homme', '30', '1', '0', '0', '1', '', '', '', '', '1488', '0', '', '3', 'hm', null, '654581ae81effacc3a69f675c1fe3a94.jpg', '{\"material\":[\"kozha\",\"nubuk\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('127', '1454319945', '1454319955', '1', '0', 'Рубашка Givenchy', 'rubashka-givenchy', '30', '0', '0', '1', '1', '', '', '', '', '1739', '0', '', '0', 'next', null, '720d0d3efa105c644950af32522f4045.jpg', '{\"material\":[\"nejlon\",\"poliestr\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('128', '1454320002', '1461250714', '1', '0', 'Мужская рубашка Kenzo', 'muzhskaja-rubashka-kenzo', '30', '0', '0', '0', '0', '', '', '', '', '867', '0', '', '1', 'next', null, 'b22809828871f8e09526177f0baed2b1.jpg', '{\"material\":[\"nejlon\",\"nubuk\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"white\"}'), ('129', '1454320064', '1460975257', '1', '0', 'Рубашка Givenchy', 'rubashka-givenchy2219', '30', '0', '0', '0', '2', '', '', '', '', '1699', '0', '', '1', 'nike', null, 'b872e6cbc18e01ce6047b418aa0b2f88.jpg', '{\"material\":[\"poliestr\",\"sintetika\",\"hlopok\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"white\"}'), ('130', '1454320424', '1458764141', '1', '0', 'Мужской пуховик Armani', 'muzhskoj-puhovik-armani', '29', '0', '0', '1', '1', '', '', '', '', '2107', '0', '', '3', 'benetton', null, '4c81d2327a33a5aa58e18501eccf17a5.jpg', '{\"material\":[\"nejlon\",\"nubuk\",\"sintetika\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"zima\"],\"tsvet\":\"fioletovyj\"}'), ('131', '1454320481', '1454320498', '1', '0', 'Куртка Burberry кожаная', 'kurtka-burberry-kozhanaja', '29', '0', '0', '0', '2', '', '', '', '', '3510', '0', '', '0', 'hm', null, '2e49d83ce27e526f8e7cc578406fb5a8.jpg', '{\"material\":[\"kozha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('132', '1454320548', '1454320563', '1', '0', 'Куртка Burberry кожаная', 'kurtka-burberry-kozhanaja5847', '29', '0', '0', '0', '0', '', '', '', '', '3400', '0', '', '0', 'next', null, 'cb0ab2f6740708dff3d7607f41163153.jpg', '{\"material\":[\"kozha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('133', '1454320609', '1479125714', '1', '0', 'Женский плащ Karen Millen', 'zhenskij-plasch-karen-millen', '29', '0', '0', '1', '1', '', '', '', '', '2067', '0', '', '2', 'benetton', null, '7991833d4e097829b93dd9f278b40c14.jpg', '{\"material\":[\"nejlon\",\"nubuk\",\"polimer\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"seryj\"}'), ('134', '1454320662', '1454320674', '1', '0', 'Мужское пальто Burberry', 'muzhskoe-palto-burberry', '29', '0', '0', '0', '2', '', '', '', '', '2800', '0', '', '0', 'hm', null, '857b1bc39a911a1a24ae239ee59d16e2.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"poliestr\",\"sintetika\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"zima\"],\"tsvet\":\"chernyj\"}'), ('135', '1454322286', '1454322300', '1', '0', 'Женские мокасины Tods Riancess белые', 'zhenskie-mokasiny-tods-riancess-belye', '34', '0', '0', '0', '2', '', '', '', '', '1920', '0', '', '0', 'benetton', null, '3bfdb6e9c6abd9d58741a9afd69a73d1.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"silikon\",\"sintetika\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"white\"}'), ('136', '1454322332', '1454322352', '1', '0', 'Женские мокасины Tods Riancess черные', 'zhenskie-mokasiny-tods-riancess-chernye', '34', '0', '0', '0', '2', '', '', '', '', '1920', '0', '', '0', 'hm', null, '98a88f9a611f19fda6d0507bfeb84ef4.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"polimer\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('137', '1454322385', '1465597478', '1', '0', 'Мокасины Tods серые замшевые', 'mokasiny-tods-serye-zamshevye', '34', '1', '0', '0', '1', '', '', '', '', '1840', '0', '', '3', 'newbalance', null, '2ab31caa02f13ca7fe793fc6ffde3935.jpg', '{\"material\":[\"zamsha\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"0\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"seryj\"}'), ('138', '1454322435', '1454605014', '1', '0', 'Мужские мокасины Prada', 'muzhskie-mokasiny-prada', '34', '0', '0', '1', '1', '', '', '', '', '1350', '0', '', '1', 'next', null, 'dd84e3af3ac388c165830cf5c2a99be1.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"poliestr\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('139', '1454322489', '1479125703', '1', '0', 'Мужские мокасины Tods темно синие', 'muzhskie-mokasiny-tods-temno-sinie', '34', '1', '0', '0', '1', '', '', '', '', '1100', '0', '', '1', 'nike', null, '33d7d2b06133a49ca5fe0f732ebf5d5b.jpg', '{\"material\":[\"nubuk\",\"polimer\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"sinij\"}');
INSERT INTO `catalog` VALUES ('140', '1454323547', '1454323565', '1', '0', 'Туфли Giuseppe Zanotti бежевые кожаные', 'tufli-giuseppe-zanotti-bezhevye-kozhanye', '33', '0', '0', '0', '1', '', '', '', '', '2100', '0', '', '0', 'benetton', null, 'ce8d895d2c71cfb9a98da843140265a7.jpg', '{\"material\":[\"kozha\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"beige\"}'), ('141', '1454323591', '1454323600', '1', '0', 'Туфли Gianmarco Lorenzi чёрные', 'tufli-gianmarco-lorenzi-chernye', '33', '0', '0', '0', '1', '', '', '', '', '2400', '0', '', '0', 'newbalance', null, 'da41ddcc27ee136176fdce29738682b2.jpg', '{\"material\":[\"kozha\",\"silikon\",\"sintetika\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"chernyj\"}'), ('142', '1454323628', '1454323643', '1', '0', 'Туфли классические Givenchy черные', 'tufli-klassicheskie-givenchy-chernye', '33', '0', '0', '0', '1', '', '', '', '', '2200', '0', '', '0', 'nike', null, '0677e1ad1f6541eee5747d28058c8180.jpg', '{\"material\":[\"zamsha\",\"lateks\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('143', '1454323679', '1459953240', '1', '0', 'Мужские туфли Louis Vuitton коричневые', 'muzhskie-tufli-louis-vuitton-korichnevye', '33', '0', '0', '0', '1', '', '', '', '', '1800', '0', '', '1', 'benetton', null, '68d868374b6db3d1cf9bea2f72027e31.jpg', '{\"material\":[\"lateks\",\"poliestr\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"korichnevyj\"}'), ('144', '1454323726', '1454323737', '1', '0', 'Мужские туфли Louis Vuitton черные', 'muzhskie-tufli-louis-vuitton-chernye', '33', '0', '0', '0', '1', '', '', '', '', '1800', '0', '', '0', 'newbalance', null, '43de9d0f84d292f30c5ac0451faff7de.jpg', '{\"material\":[\"kozha\",\"lateks\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"chernyj\"}'), ('145', '1454324137', '1454324226', '1', '0', 'Мужские sneakers Buscemi', 'muzhskie-sneakers-buscemi', '32', '0', '0', '0', '1', '', '', '', '', '1700', '0', '', '0', 'newbalance', null, '2c57611d39d0339269ecba567a749967.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"osen\"],\"tsvet\":\"chernyj\"}'), ('146', '1454324425', '1454324440', '1', '0', 'Кроссовки Giuseppe Zanotti', 'krossovki-giuseppe-zanotti', '32', '0', '0', '0', '1', '', '', '', '', '1440', '0', '', '0', 'nike', null, '120a2393910ce4cad4acd9300c124ca4.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"lateks\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('147', '1454324475', '1454324495', '1', '0', 'Кроссовки Y-3 ', 'krossovki-y-3-yohji-yamamoto', '32', '0', '0', '0', '1', '', '', '', '', '1160', '0', '', '0', 'newbalance', null, '2e70fbd4a6418b0d25f58157243ec680.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"osen\"],\"tsvet\":\"yellow\"}'), ('148', '1454324542', '1454324558', '1', '0', 'Мужские кроссовки Louis Vuitton замшевые красные', 'muzhskie-krossovki-louis-vuitton-zamshevye-krasnye', '32', '0', '0', '0', '2', '', '', '', '', '2400', '0', '', '0', 'nike', null, '7e6ed9ff208f16a45ab64257ba9f3a72.jpg', '{\"material\":[\"zamsha\",\"nubuk\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"krasnyj\"}'), ('149', '1454324590', '1454324601', '1', '0', 'Мужские кроссовки Prada чёрные', 'muzhskie-krossovki-prada-chernye', '32', '0', '0', '0', '1', '', '', '', '', '1300', '0', '', '0', 'newbalance', null, '8b80482102762d94fb58b6db7e57550c.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('150', '1454324982', '1454324999', '1', '0', 'Женская сумка Roberto Cavalli', 'zhenskaja-sumka-roberto-cavalli', '35', '0', '0', '0', '1', '', '', '', '', '1280', '0', '', '0', 'benetton', null, 'f7202f3d2445ba1f4fa2839437e2b9b9.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"silikon\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"krasnyj\"}'), ('151', '1454325034', '1454325047', '1', '0', 'Клатч Alexander Mcqueen', 'klatch-alexander-mcqueen', '35', '0', '0', '0', '1', '', '', '', '', '1300', '0', '', '0', 'hm', null, '07ee6659e7a415e9d34f6a93b540908e.jpg', '{\"material\":[\"kozha\",\"silikon\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('152', '1454325084', '1454325108', '1', '0', 'Женская сумка Zara', 'zhenskaja-sumka-zara', '35', '0', '0', '0', '1', '', '', '', '', '900', '0', '', '0', 'benetton', null, '0a4eff388b67a57a083f654ac8149b0b.jpg', '{\"material\":[\"nejlon\",\"nubuk\",\"polimer\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('153', '1454325135', '1454325151', '1', '0', 'Мужская сумка Calvin Klein', 'muzhskaja-sumka-calvin-klein', '35', '0', '0', '0', '1', '', '', '', '', '827', '0', '', '0', 'hm', null, '698e3ecaee23d4e5f2f1530d0c240ad8.jpg', '{\"material\":[\"kozha\",\"nubuk\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('154', '1454325181', '1455806204', '1', '0', 'Мужская сумка GUCCI', 'muzhskaja-sumka-gucci', '35', '0', '0', '0', '1', '', '', '', '', '760', '0', '', '0', 'benetton', null, 'a1263630a87a89b5226af70435ba8254.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"sintetika\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"zima\",\"leto\"],\"tsvet\":\"chernyj\"}'), ('155', '1462879116', '1462890105', '1', '0', 'Nke Air Huarache', 'nke-air-huarache', '32', '1', '0', '0', '1', 'Nike Air Huarache', 'Nike Air Huarache', 'Nike Nike Nike ', '', '1999', '0', '', '10', 'nike', 'airhuarache', '8d2ec33b46ebc352b67098759eebe5fc.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"nubuk\",\"tekstil\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('156', '1462879768', '1462890101', '1', '0', 'Ruma Creepers ', 'ruma-creepers-', '32', '1', '1', '0', '1', '', '', '', '', '1499', '1795', 'Ruma Creepers ', '6', null, null, 'eab6d6b4851e790b52d71813e8537b8c.jpg', '{\"material\":[\"zamsha\",\"iskusstvennaja-zamsha\",\"poliestr\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'), ('157', '1463996817', '1464614312', '1', '0', 'Подушка крассная', 'podushka-krassnaja', '40', '0', '0', '0', '1', '', '', '\r\n', '', '40', '0', '444', '14', null, null, '0cdf3a8e39c4cfb545c4e0da3a719c6f.jpg', '{\"sex\":\"0\"}'), ('158', '1464612611', '1464618713', '1', '0', 'Одеяло из шерсти', 'odejalo-iz-shersti', '41', '0', '0', '0', '1', '', '', '', '', '1000', '0', '1111', '3', 'fabricfrontline', null, '876558828e8bf44fd34c1638e82bfdb4.jpg', '{\"material\":[\"zamsha\",\"iskusstvennaja-zamsha\",\"iskusstvennaja-kozha\",\"kozha\",\"lateks\",\"nejlon\",\"nubuk\",\"poliestr\",\"polimer\",\"silikon\",\"tekstil\",\"hlopok\"],\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"fioletovyj\"}'), ('159', '1464618360', '1464618360', '1', '0', 'терка для овощей', 'terka-dlja-ovoschej', '44', '0', '0', '0', '1', '', '', '', '', '35', '0', '123дж', '0', null, null, null, '{\"material\":[\"zamsha\",\"iskusstvennaja-zamsha\",\"iskusstvennaja-kozha\",\"kozha\",\"lateks\",\"nejlon\",\"nubuk\",\"poliestr\",\"polimer\",\"silikon\",\"sintetika\",\"tekstil\",\"hlopok\"],\"sex\":\"0\",\"proizvodstvo\":\"ukraina\",\"tsvet\":\"0\"}'), ('160', '1464618597', '1464685063', '1', '0', 'Терка универсальная', 'terka-universalnaja', '44', '0', '0', '0', '1', '', '', '', '', '55', '0', '1235дж', '0', null, null, '1b22ffdbad1330328b791e4a253e6d97.jpg', '{\"sex\":\"0\",\"proizvodstvo\":\"kitaj\"}'), ('161', '1464627001', '1464684951', '1', '0', 'Терки акция', 'terki-aktsija', '45', '0', '0', '1', '1', '', '', '', '', '50', '55', '2323', '3', null, null, 'c6121f9633e13ce34552ba96c741d096.jpg', '{\"tsvet\":\"seryj\"}'), ('162', '1464685147', '1464696138', '1', '0', 'Ложка', 'lozhka', '47', '0', '0', '0', '1', '', '', '', '', '25', '0', '222', '3', 'caf', null, 'ae7cd5e751f1f6c5305e881d8a38622d.jpg', '{\"tsvet\":\"0\"}'), ('163', '1465035027', '1507541665', '1', '0', ' Тёплая', 'teplaja', '51', '0', '0', '1', '1', '', '', '', '', '1400', '0', '111', '3', 'aldobrue', null, 'db6bf874a183ef6709366777e38b5f8b.jpg', '{\"tsvet\":\"beige\",\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\"}');
COMMIT;

-- ----------------------------
-- Table structure for `catalog_images1`
-- ----------------------------
DROP TABLE IF EXISTS `catalog_images1`;
CREATE TABLE `catalog_images1` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`sort`  int(10) NOT NULL DEFAULT 0 ,
`catalog_id`  int(10) NOT NULL DEFAULT 0 ,
`main`  tinyint(1) NOT NULL DEFAULT 0 ,
`image`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `catalog_id` (`catalog_id`) USING BTREE ,
INDEX `image` (`image`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1099

;

-- ----------------------------
-- Records of catalog_images1
-- ----------------------------
BEGIN;
INSERT INTO `catalog_images1` VALUES ('611', '1447094678', null, '0', '40', '1', '31c79213d5aceed90eed954307c44416.jpg'), ('612', '1447094678', null, '0', '40', '0', '38ff50f13f667a0e6500555f58db75e0.jpg'), ('613', '1447094683', null, '0', '40', '0', 'a8939e7b09f8347786e0f3e830c3a94a.jpg'), ('614', '1447094684', null, '0', '40', '0', 'c4513d5088f6374a2df90dc6a7a76ac1.jpg'), ('615', '1447094778', null, '0', '41', '0', 'c65bc1609165bf2eaf28fb76755cfb3a.jpg'), ('616', '1447094778', null, '0', '41', '0', '9c20b99496ac618fd064561234fdf753.jpg'), ('617', '1447094778', '1447094785', '0', '41', '1', '3faf38fccbe35f31b603141be04317f3.jpg'), ('618', '1447094779', null, '0', '41', '0', '7fd7e096c6eb0e37c2ba1322f84a404f.jpg'), ('619', '1447094884', null, '0', '42', '0', '8c7705ee980b733f90bebc2e15637494.jpg'), ('620', '1447094884', '1447094889', '0', '42', '1', 'fe75cb26c4ae4c06af7d7fdef455fe1b.jpg'), ('621', '1447094884', null, '0', '42', '0', 'ec61f00800b0aeaf06758204adaa1107.jpg'), ('622', '1447094884', null, '0', '42', '0', '52380165b1463bad08ad2c201a010f24.jpg'), ('623', '1447095185', null, '0', '43', '0', '06f084f113e5b5248af77fc0afb53656.jpg'), ('624', '1447095185', '1447095192', '0', '43', '0', 'e8e63648d7a99d6ac02973c1f79fdc20.jpg'), ('625', '1447095186', null, '0', '43', '0', '2401a36f9678329c6a799b52845614d6.jpg'), ('626', '1447095186', '1447095197', '0', '43', '1', '21f15ca55231a0db69dad9a2f1c6d2cd.jpg'), ('627', '1447095290', null, '0', '44', '0', '46e3ad3bb52ad060888db5a26cf38fb0.jpg'), ('628', '1447095290', '1447095295', '0', '44', '1', '43c658475e0a09cb4fc1c63b46bdc5a9.jpg'), ('629', '1447095290', null, '0', '44', '0', '593c5d47b1233bf928d841e35ceda328.jpg'), ('630', '1447095291', null, '0', '44', '0', '104e058a23b1aeeded9aa4ba16b3613c.jpg'), ('631', '1447095375', null, '0', '45', '0', '7aaf1cc025af46186ef6105af07302aa.jpg'), ('632', '1447095375', null, '0', '45', '0', 'c6ed2a324ba41120cde5b43691e6fa1c.jpg'), ('633', '1447095375', '1453983879', '0', '45', '1', '85b47014b4b719452a06dc924426491f.jpg'), ('634', '1447095376', null, '0', '45', '0', '30e1b40a46d741c377846b089dc1b5d2.jpg'), ('647', '1447095935', null, '0', '49', '1', 'd56dbeb19c27a828e5762b1aabd956a2.jpg'), ('648', '1447095935', null, '0', '49', '0', '794e27e91e3827d06c9a275f2d5fdf67.jpg'), ('649', '1447095935', null, '0', '49', '0', '0c2a3494e44ff805f0aa0101acc731b7.jpg'), ('650', '1447095936', null, '0', '49', '0', '890457273def0c8f792aea62fb9e024b.jpg'), ('651', '1447096008', null, '0', '50', '0', '782c69989db1782087e4db39a775339d.jpg'), ('652', '1447096009', null, '0', '50', '0', '4fc5db935e0b553e9313f41a6039f38f.jpg'), ('653', '1447096009', '1453988344', '0', '50', '1', '147378e984f87fbc42a0b80bd52899a2.jpg'), ('654', '1447096009', null, '0', '50', '0', 'e5ba68f0c3d4204607f810388584dffc.jpg'), ('655', '1447096112', null, '0', '51', '0', '1547105c5adc86058857a80c8c0c9385.jpg'), ('656', '1447096112', null, '0', '51', '0', '17bd4f3fb5b9ebb4b440aa93ea501ce5.jpg'), ('657', '1447096113', '1447096118', '0', '51', '1', '528d7c85efd8247ba056e4a1c29b4bca.jpg'), ('658', '1447096113', null, '0', '51', '0', '9913d50a7fc4f0647673024fcc1520e4.jpg'), ('659', '1447096192', null, '0', '52', '1', '3e402891cdc71ca41bc7f7f09e042d4b.jpg'), ('660', '1447096192', null, '0', '52', '0', '15fe0291226b53ccf22139b82ea0e3b3.jpg'), ('661', '1447096192', null, '0', '52', '0', '5143399809e92177a6b245a13eaf1040.jpg'), ('662', '1447096193', null, '0', '52', '0', 'd82dfae402a5be04f1e5428418b03899.jpg'), ('663', '1447096193', null, '0', '52', '0', 'fc6d6e8fa2573e57820c6f9831589c70.jpg'), ('664', '1447096257', null, '0', '53', '0', '5570dd2844ef1b6537ef54cf8201d017.jpg'), ('665', '1447096258', null, '0', '53', '0', '4021ff43abd896ed381a93e3f20d2eee.jpg'), ('666', '1447096258', '1447096264', '0', '53', '1', 'ffeee2baa0eb681477b1b4ae95c92e79.jpg'), ('667', '1447096258', null, '0', '53', '0', '695335f5e51e38b722e7245e7e5e7418.jpg'), ('668', '1447096351', '1448356268', '1', '54', '0', '834c53733e652ec1f70cd1317ef82139.jpg'), ('669', '1447096351', '1449923786', '4', '54', '1', '3b3c8e2b8b4c89d1fbb12c3e12de9d5a.jpg'), ('670', '1447096351', null, '2', '54', '0', '7a6d9f600684aa0b7d8131e2788bc1b5.jpg'), ('671', '1447096352', null, '5', '54', '0', 'eba225f85fd9ab2132e0ba7fb68efa36.jpg'), ('672', '1447096352', '1447096358', '6', '54', '0', 'fd180a4c7b269eff9987c17be93ed57f.jpg'), ('673', '1447096440', null, '0', '55', '0', '535707cf9761cf5dda7b4b8cfa8412d9.jpg'), ('674', '1447096441', null, '0', '55', '0', 'bc45391efa8abb4ad22e6b03675b0687.jpg'), ('675', '1447096441', null, '0', '55', '0', '25bf84f8b8034c3ed6f12634955ef9ee.jpg'), ('676', '1447096441', '1454062939', '0', '55', '1', 'ff92a4371fdb1b55022573290fc5ad2f.jpg'), ('677', '1447096442', null, '0', '55', '0', '27c09b1972e05bab1a05bd8a6c644e77.jpg'), ('678', '1448355514', '1448356114', '3', '54', '0', '1eef990b83ea781b7259171943f6a5d0.jpg'), ('707', '1453982710', null, '0', '84', '0', '03a4bd46da830c79d48098bc30f410ec.jpg'), ('708', '1453982710', null, '0', '84', '0', '2c036c65e187abac51b1839ca115b7dd.jpg'), ('709', '1453982710', '1453982715', '0', '84', '1', '878a7e4e4ce127d11d0f01e49b165543.jpg'), ('710', '1453983182', null, '0', '56', '0', 'aabe0e6e9da54870d2b850f5bc3100a1.jpg'), ('711', '1453983184', null, '0', '56', '0', 'bbcf6f4835aefdfe6cab1ea8fa5c4cb5.jpg'), ('712', '1453983184', '1453983188', '0', '56', '1', '9bd9e2c0cb862c21ba8b33160fa544b7.jpg'), ('713', '1453983301', null, '0', '57', '1', '72db86cef6b5e1901bd0dabf72b1735e.jpg'), ('714', '1453983302', null, '0', '57', '0', 'cced5c99377923d74a7a88b8f0841e62.jpg'), ('715', '1453983303', null, '0', '57', '0', '0f8f7897e053de651ea53b5a02c00165.jpg'), ('716', '1453983415', null, '0', '58', '1', '46d9d8f8e3be8cb4f5049aa0b11b796c.jpg'), ('717', '1453983416', null, '0', '58', '0', '8b1a1e324010527b44a7fb2f371fab0f.jpg'), ('718', '1453983417', null, '0', '58', '0', 'da7f86fdc6b745745f2421deb46bc851.jpg'), ('719', '1453983538', null, '0', '59', '0', 'd49e029935ae7db636ccb2841cc077db.jpg'), ('720', '1453983539', null, '0', '59', '0', 'a88a4f24315ce916d1eaa9da5d723797.jpg'), ('721', '1453983540', null, '0', '59', '0', '72b78db192ee58dcde018a4d7f312b57.jpg'), ('722', '1453983540', '1453983544', '0', '59', '1', 'feee3908006fdc4ab715cd327d00a745.jpg'), ('723', '1453983822', null, '0', '60', '0', '12dc48c811c05fc2c3d0347727f914f5.jpg'), ('724', '1453983823', '1453983892', '0', '60', '1', '371f58a9be9b8faba677741ac6b12050.jpg'), ('725', '1453983824', null, '0', '60', '0', '2973ce21c82746f252ac45c693d6fd90.jpg'), ('726', '1453983824', null, '0', '60', '0', 'c88a9dffe14254ee22e1a18f1afd811f.jpg'), ('727', '1453985374', null, '0', '85', '1', '1e45fd1d34c616984a94829c43c9d251.jpg'), ('728', '1453985374', null, '0', '85', '0', '0ecedd75804257dde4437de71425ce49.jpg'), ('729', '1453985375', null, '0', '85', '0', '767100b541d222386b6908d7b7962c2c.jpg'), ('730', '1453985375', null, '0', '85', '0', 'a77ae6f3635bb1bb07c0814c32e2134c.jpg'), ('731', '1453985627', null, '0', '86', '1', '8de18a7355287270b3df43c05a1048c0.jpg'), ('732', '1453985628', null, '0', '86', '0', 'f5cead36ddfd1bb67a551d21a4a189bf.jpg'), ('733', '1453985628', null, '0', '86', '0', 'c302f303c96788afa04ca5f37a9f4996.jpg'), ('734', '1453985737', null, '0', '87', '0', '9c780810973a8c9934fd5b152e3eb678.jpg'), ('735', '1453985738', '1453985742', '0', '87', '1', '2130622858813fec5259b99f8b452e75.jpg'), ('736', '1453985738', null, '0', '87', '0', '5dd556a5eb29a31bfac08bd48a34231a.jpg'), ('737', '1453987094', null, '0', '61', '0', '7a97c8e3764b1f7f8926b61b1299963a.jpg'), ('738', '1453987095', null, '0', '61', '0', '048b13f12356c4c5e9e262ca6deb8ad8.jpg'), ('739', '1453987096', '1453987101', '0', '61', '1', '744f8a702159b78c379ed35dc8200eb9.jpg'), ('740', '1453987222', null, '0', '62', '1', '8aafb240eb2626fb201353890a8d9035.jpg'), ('741', '1453987222', null, '0', '62', '0', '2acd1acec7117ac8e0cd66f46bcc89fa.jpg'), ('742', '1453987223', null, '0', '62', '0', 'e5e1db7303758f31dcc458ba512a4f70.jpg'), ('743', '1453987340', null, '0', '63', '1', '69fb49c55e1dbae657920116c3121aca.jpg'), ('744', '1453987340', null, '0', '63', '0', '43a255e3e909090a1fac9373c2fc397e.jpg'), ('745', '1453987341', null, '0', '63', '0', '351457c1b5cc6b56b3ebbf7f555f008f.jpg'), ('746', '1453987425', null, '0', '64', '1', 'e88ca78f3d83a09ec4d9a087f7488465.jpg'), ('747', '1453987425', null, '0', '64', '0', '0978227f164919ba8f133902efc80082.jpg'), ('748', '1453987426', null, '0', '64', '0', 'b644bb174c4ae4927f95ada221fbc89f.jpg'), ('749', '1453987538', null, '0', '65', '1', '343f2ed7a53c89842f76680def0ff912.jpg'), ('750', '1453987539', null, '0', '65', '0', '57e65d5a9b2fa85373eb495875815dce.jpg');
INSERT INTO `catalog_images1` VALUES ('751', '1453987539', null, '0', '65', '0', 'e30a0bdda65f94b6b1a783cf3836032e.jpg'), ('752', '1453987606', '1453987619', '0', '66', '1', '4000f9750e94224c62a0c5a4e155a753.jpg'), ('753', '1453987608', '1453987613', '0', '66', '0', '342aa1e8a73c09e0eea9567c78513f98.jpg'), ('754', '1453987608', null, '0', '66', '0', 'af7d547f0e53fee0e3cf9315d6120633.jpg'), ('755', '1453988011', null, '0', '67', '1', 'cf92f6be7ef042db8506fd217337ec04.jpg'), ('756', '1453988012', null, '0', '67', '0', '1c955d65100250aa23c32b1251ae4172.jpg'), ('757', '1453988012', null, '0', '67', '0', 'ef590a4c74997d465572a9992b8e9d0e.jpg'), ('758', '1453988013', null, '0', '67', '0', '51abdf80e96777fe3e2942af40be5302.jpg'), ('759', '1453988030', null, '0', '68', '1', '9b2bd4df4c45164ff451721eec1393a4.jpg'), ('760', '1453988031', null, '0', '68', '0', 'c1998bb0dbfe40e6f67bc32950926ac8.jpg'), ('761', '1453988031', null, '0', '68', '0', '0f996bb3bf77a488209574a4778316ce.jpg'), ('762', '1453988044', null, '0', '69', '0', 'a8fe67dce288d4a18c8bf978c45343a0.jpg'), ('763', '1453988044', '1453988048', '0', '69', '1', 'af5e3e1d7e0cefb9d417502078673523.jpg'), ('764', '1453988045', null, '0', '69', '0', 'd01d39d9d4ce8d4a4cda06c13332fe75.jpg'), ('765', '1453988162', null, '0', '88', '1', '650e1c2c39fcd2db0fe5552ce0a96813.jpg'), ('766', '1453988163', null, '0', '88', '0', '3be3d76ed2062c15a1cc2f205d0d0a04.jpg'), ('767', '1453988163', null, '0', '88', '0', '5135307d552ab87e7572ab44441a7f17.jpg'), ('768', '1453988164', null, '0', '88', '0', '712a656d0502072dad0aba167bd7854c.jpg'), ('799', '1454003730', null, '0', '72', '1', '0e6af6118a01535c6d6b8cb9b9b1cc95.jpg'), ('800', '1454003730', null, '0', '72', '0', '694f6b9c879a24b47533cc84694595bf.jpg'), ('801', '1454003730', null, '0', '72', '0', '2a04ccf5c609406ec5b421f24f12ee6b.jpg'), ('802', '1454004414', null, '0', '71', '0', '220ae0756f73e333b0ca2ae962d00890.jpg'), ('803', '1454004415', null, '0', '71', '0', '9515a9318697032022a1c5f1e5f1a217.jpg'), ('804', '1454004415', '1454004421', '0', '71', '1', '99d4ab2baa6efb497393b8caa546b7d2.jpg'), ('805', '1454004532', null, '0', '70', '1', '0d8ad8265bc795e5f0b1e46f1872f01d.jpg'), ('806', '1454004533', null, '0', '70', '0', '83a111408b7fd7dc7278f858d89c978d.jpg'), ('807', '1454004641', null, '0', '89', '1', 'e16e898eaab10b2a8288cc73e4da458c.jpg'), ('808', '1454004641', null, '0', '89', '0', 'd72dab98811c694f9cebe61a7fec7b9b.jpg'), ('809', '1454004641', null, '0', '89', '0', '1237b20593ad76cb11ac52076f5ffc31.jpg'), ('810', '1454004641', null, '0', '89', '0', '759f19c66c4c6e2f3300360c08fa4dff.jpg'), ('811', '1454004739', null, '0', '90', '0', 'd6b65ca03dd2c7d5f368e31da0b76bb1.jpg'), ('812', '1454004739', '1454004744', '0', '90', '1', '996c9737bb19cec0cdf8545920a35024.jpg'), ('813', '1454004739', null, '0', '90', '0', '1940a37c8014ada7474000a6564813c7.jpg'), ('814', '1454004919', null, '0', '91', '0', '4216eeaa83749e2ea84ef3fac9b41b4a.jpg'), ('815', '1454004919', null, '0', '91', '0', 'b23d2cc98743135d288b59a2c6d9fcd7.jpg'), ('816', '1454004919', '1454004923', '0', '91', '1', '1a89bd77c02b30bb960a2758628ff129.jpg'), ('817', '1454004919', null, '0', '91', '0', 'f1029157d3165c37e5b0e1f2aa15955a.jpg'), ('818', '1454004920', null, '0', '91', '0', '1356f04ebf05b76e1a9891b3d995bb71.jpg'), ('819', '1454005068', null, '0', '92', '1', '27386468296175ab291d15df6925661d.jpg'), ('820', '1454005069', null, '0', '92', '0', '3bdbda6f6678aba8ac4ab48fc9396276.jpg'), ('821', '1454005351', null, '0', '93', '0', 'ca3d750b429a89dc44f2a0057ddb43fd.jpg'), ('822', '1454005351', '1454005358', '0', '93', '1', '222d27610fe00ae41888d76765c172b5.jpg'), ('823', '1454005351', null, '0', '93', '0', '07aa04206a12deac7d9af34d90bbe5ad.jpg'), ('824', '1454005352', null, '0', '93', '0', 'df8a7f28b68d6d22a9493103d4418a45.jpg'), ('825', '1454005352', null, '0', '93', '0', '2ba4f0cf91c8cbf93f1c8ac2c6213560.jpg'), ('826', '1454005518', null, '0', '94', '0', '3115991de405f0586cca786ed9a506f3.jpg'), ('827', '1454005518', null, '0', '94', '0', 'e89a43be8afdeb58c1548ab6e2bbc624.jpg'), ('828', '1454005518', '1454005524', '0', '94', '1', '0123777411cf5317bb713d9afdc3c9c1.jpg'), ('829', '1454005519', null, '0', '94', '0', 'a60bc82fc028454e26fbccb06032cc0b.jpg'), ('830', '1454006065', null, '0', '74', '1', '2b58dd8bf1316ed14ede88d7fe1a9a92.jpg'), ('831', '1454006065', null, '0', '74', '0', '8e41132d8c00cba37625f9a136fbb60d.jpg'), ('832', '1454006066', null, '0', '74', '0', '9b7a69731bf43646e1cbdb2140663231.jpg'), ('833', '1454006080', null, '0', '75', '1', '6acb9c32679cf8b76b7d4a1138f4ea24.jpg'), ('834', '1454006080', null, '0', '75', '0', 'fc8cb8f57e42d9d20d840ab99309de72.jpg'), ('835', '1454006080', null, '0', '75', '0', 'a86e19f0861685cfee4a182b11b579b2.jpg'), ('836', '1454006103', null, '0', '73', '1', 'f61df09878d457ee66fc7e1fd14addbd.jpg'), ('837', '1454006103', null, '0', '73', '0', '100ebfa1515dae2e87a40e2215d27cd3.jpg'), ('838', '1454006103', null, '0', '73', '0', 'd1b157543ec2e0536e2531c8099ca05d.jpg'), ('839', '1454006176', null, '0', '95', '0', 'a17d7ce694cb0c2fcc7304f8c829030e.jpg'), ('840', '1454006176', null, '0', '95', '0', '706140e5d025c19f1d273deeaf757e7c.jpg'), ('841', '1454006176', '1454006179', '0', '95', '1', 'aedca7492b0710de73e4d3553a0b317c.jpg'), ('842', '1454006176', null, '0', '95', '0', '9e7f08b943a5925c2aa3937d8ead6e40.jpg'), ('843', '1454006245', null, '0', '96', '0', 'b575c6391044b7d0a45f041c6ead7163.jpg'), ('844', '1454006246', null, '0', '96', '0', 'ee4095167f127a5938daa8315ddf12ad.jpg'), ('845', '1454006246', '1454006252', '0', '96', '1', '296c2f7b532ab1905d9813cd4843d955.jpg'), ('846', '1454006246', null, '0', '96', '0', 'd98d23d8342ece8fe8a9a3da2b95f736.jpg'), ('847', '1454006529', null, '0', '97', '1', 'f733af1bc7c73d6def2e19d94fcebd15.jpg'), ('848', '1454006529', null, '0', '97', '0', 'ddac1fb506514965e7c818ea145c9d54.jpg'), ('849', '1454006584', null, '0', '98', '1', 'ce1d58dc0da917077043c1edf6de746a.jpg'), ('850', '1454006585', null, '0', '98', '0', 'f4005f006cd1b560458a5e56ce6a000e.jpg'), ('851', '1454006586', null, '0', '98', '0', '34382630bd380991c61b913778c75747.jpg'), ('852', '1454006659', null, '0', '99', '1', 'eb148d52f4cc6c272ee88bacf2776732.jpg'), ('853', '1454006660', null, '0', '99', '0', '7e3939220d61f4fe145869f7ebe3ddad.jpg'), ('854', '1454058548', null, '0', '100', '1', '400f456673efc583825c497288dbdca7.jpg'), ('855', '1454058548', null, '0', '100', '0', '6430eebeec685dfd9e0377e4a4b1ca42.jpg'), ('856', '1454058548', null, '0', '100', '0', '69f85a02d7e557cfce2803831b8a8bbb.jpg'), ('857', '1454058725', null, '0', '101', '0', '91fc0afa9fde681971476b2f569a90f4.jpg'), ('858', '1454058725', null, '0', '101', '0', '9ac1437c3d25cd9f1172eccdd710944a.jpg'), ('859', '1454058725', null, '0', '101', '0', '82ad5be4bed112cc6d2d23dd72718dd7.jpg'), ('860', '1454058726', '1454058729', '0', '101', '1', 'bd1b148c4d131aaa82c04b236c01de3c.jpg'), ('861', '1454058987', null, '0', '102', '0', '5eb6d9d837cb2cb4f855feb94bd3c37d.jpg'), ('862', '1454058987', null, '0', '102', '0', '2fe2af665db7a16a74fc53b737da9da5.jpg'), ('863', '1454058987', '1454058992', '0', '102', '1', 'c75370b832affc3be9d433178f864ead.jpg'), ('864', '1454058988', null, '0', '102', '0', 'd531b27fef7c738e88924427a446b806.jpg'), ('865', '1454058988', null, '0', '102', '0', '2f92add341e2bba7a50c7bc2bc95d7e4.jpg'), ('866', '1454058988', null, '0', '102', '0', 'e03de269b92e127c5676a8a50a8c8945.jpg'), ('867', '1454059085', null, '2', '103', '0', 'ff99a21eb210baf5b737a1118a6f1732.jpg'), ('868', '1454059086', '1454059089', '1', '103', '1', '73487bc7d72b9ca7f581b1e2c87ed78d.jpg'), ('869', '1454059190', null, '0', '104', '0', '79716a60d4dc7b2cd2ce6f7f2944d7a8.jpg'), ('870', '1454059190', null, '0', '104', '0', 'ed678a262c53b7d59de4008ad8a1d61e.jpg'), ('871', '1454059190', null, '0', '104', '0', '681363239b8832a7b025394d7727e9e2.jpg'), ('872', '1454059191', '1454059195', '0', '104', '1', '348b0a5a662583ff1543ba0703034249.jpg'), ('873', '1454059499', null, '0', '105', '0', '6f1f2bda9b0c537fb2fbba398f846fb9.jpg'), ('874', '1454059500', null, '0', '105', '0', 'f0697a66790f884a48ea377ab78c91db.jpg'), ('875', '1454059500', '1454059504', '0', '105', '1', '3ee3635b29378b2c1b92d0ee8a8262ae.jpg'), ('876', '1454059500', null, '0', '105', '0', '310d75d92bff31e1b2ed9cddafd6a4f2.jpg'), ('877', '1454059805', null, '0', '76', '1', 'aa1d555bcf57d51b72b13f70e12d5036.png'), ('878', '1454059806', null, '0', '76', '0', 'f8abb9f1d2c623ce76fa99015f02ed2d.png'), ('879', '1454059807', null, '0', '76', '0', '2e2b87b1db1abfbb238315250efe109c.png'), ('880', '1454059932', null, '0', '77', '1', 'ac95a9367b31d93cf5c8f6e5bf1a34de.jpg');
INSERT INTO `catalog_images1` VALUES ('881', '1454059933', null, '0', '77', '0', '29dcefd737992e68577493f2fc25bd1b.jpg'), ('882', '1454059933', null, '0', '77', '0', 'a084f9c45351e90ae273078e2c9ad7ae.jpg'), ('883', '1454059933', null, '0', '77', '0', 'c034e66061a0ea06df768ef31a715136.jpg'), ('884', '1454059934', null, '0', '77', '0', '23ac4dc4e934985790049d92413f3f1f.jpg'), ('885', '1454060055', null, '0', '78', '1', 'f30fe5c63fd4f0f9e382e275606aa7e5.jpg'), ('886', '1454060055', null, '0', '78', '0', '2177dcf84396e89a84ec03c9ca7fe947.jpg'), ('887', '1454060055', null, '0', '78', '0', '82e7948122a5339eafdf39c1984f0c4e.jpg'), ('888', '1454060055', null, '0', '78', '0', '9e00e5d1fd378ffa2d33377c098035b1.jpg'), ('889', '1454060290', null, '0', '46', '1', '6891ab8f29e5eeded563f514d42279f4.jpg'), ('890', '1454060291', null, '0', '46', '0', '6f792362ea19c0293b3f8964e8e2744e.jpg'), ('891', '1454060291', null, '0', '46', '0', '2ca9b31103dd3841c9d2ed6620748b9e.jpg'), ('892', '1454060427', null, '0', '47', '1', '4454777def4b6cf80459a15566e23770.jpg'), ('893', '1454060428', null, '0', '47', '0', '658a009a62674b13832e088c81fc3c56.jpg'), ('894', '1454060519', null, '0', '48', '1', 'c2c9d0b441f241040ff329880f7840e5.jpg'), ('895', '1454060520', null, '0', '48', '0', '72acee67ac7d096953ed7e80e9c19091.jpg'), ('896', '1454060641', null, '0', '106', '0', 'c42d9959125e80eb70759301e84a2f87.jpg'), ('897', '1454060641', '1454060646', '0', '106', '1', 'bcedcb826fce171cb73c27dcf27e2b85.jpg'), ('898', '1454060641', '1454060645', '0', '106', '0', 'fde09d1c8dde81cd1a97b982939d7831.jpg'), ('899', '1454060765', null, '0', '107', '0', 'b8dd61fda4136456a950e3588db2725d.jpg'), ('900', '1454060766', null, '0', '107', '0', 'c35cdae2bdd3bcd425326c3d14d0bb97.jpg'), ('901', '1454060766', '1454060773', '0', '107', '1', '1e2e2f992f6e9d1aad557278cd9d56b7.jpg'), ('902', '1454060896', null, '0', '108', '0', '9c08b55f586bed4b24c4db5f3940face.jpg'), ('903', '1454060896', null, '0', '108', '0', '5114f3b8896e1cd6f8589c06ee2b82f4.jpg'), ('904', '1454060897', '1454060902', '0', '108', '1', 'a4211640da4bb4ad88bf464938bdda63.jpg'), ('905', '1454060897', null, '0', '108', '0', 'c3f02f658d19e6277079eba3c830dea5.jpg'), ('906', '1454061009', null, '0', '109', '0', '9c62729b8903346f47fbadec2a603d54.jpg'), ('907', '1454061010', '1454061016', '0', '109', '0', 'cbe0b1362992f9a611732fa3c628a283.jpg'), ('908', '1454061010', '1454061017', '0', '109', '1', '40d2333010aa10e1f683eaee65ffeb1f.jpg'), ('909', '1454061010', null, '0', '109', '0', '7cf2bb2259fc83105dd54b5f45164e7c.jpg'), ('910', '1454061480', null, '0', '82', '0', 'c137b3097e8e27c8b37ddb50ba4f5d60.jpg'), ('911', '1454061486', '1454061492', '0', '82', '1', '81734926419a4401f8322ece3a5fe691.jpg'), ('912', '1454061488', null, '0', '82', '0', 'de6139486400f05812ff930e070a7219.jpg'), ('913', '1454061664', null, '0', '83', '0', '45b1ee668c6b32211024e653592d6eab.jpg'), ('914', '1454061664', null, '0', '83', '0', 'd4a752509012e86a1eba61ddbcd56ac2.jpg'), ('915', '1454061664', '1454061668', '0', '83', '1', '06020e5f548b14bbe0d3cddd1ddb9609.jpg'), ('916', '1454061665', null, '0', '83', '0', '7f7308b85cf8860bf87688003c341c4d.jpg'), ('917', '1454061805', null, '0', '110', '1', '8243a32288f42683b19c2e6246d32434.jpg'), ('918', '1454061805', null, '0', '110', '0', '5e8803702804b5191526f617a9872bfc.jpg'), ('919', '1454061805', null, '0', '110', '0', 'cf13c4865c9d525707be1380fa12fee9.jpg'), ('920', '1454061918', null, '0', '111', '0', '3bc233bb01df0372a9bfec43bc40808b.jpg'), ('921', '1454061919', '1454061923', '0', '111', '1', '2bc9719c017326e92508f46b404f342f.jpg'), ('922', '1454061920', null, '0', '111', '0', 'cc18d065868523ddbaecfea60658a927.jpg'), ('923', '1454061920', null, '0', '111', '0', '14ad7b75a49aaa94e79498940e95c46e.jpg'), ('924', '1454061920', null, '0', '111', '0', 'a73c11aa2c911000dad0adbe67389636.jpg'), ('925', '1454062059', null, '0', '112', '0', 'e3331e4ecff91a020e6faf5855f6c106.jpg'), ('926', '1454062059', '1454062062', '0', '112', '1', '82a09f8eae1e2e17f6f4d38a116fe1f9.jpg'), ('927', '1454062059', null, '0', '112', '0', 'c7b1f5eede7218e7ce3928ce75bebf80.jpg'), ('928', '1454062149', null, '0', '113', '0', '862bbc95fd9d5931cc45224a5594912b.jpg'), ('929', '1454062150', null, '0', '113', '0', 'c04388b2a78d26f58494f64bb85a20a2.jpg'), ('930', '1454062150', '1454062155', '0', '113', '1', '027e8a2609c1af3e188c8638f016fe97.jpg'), ('931', '1454062150', null, '0', '113', '0', '3decac2f8a418b001541276a997e4449.jpg'), ('932', '1454062267', null, '0', '114', '0', '39a508af70aec16c9ecdbd65332851f6.jpg'), ('933', '1454062268', null, '0', '114', '0', '18de15e953baa59ad1f7c1b346816baf.jpg'), ('934', '1454062268', null, '0', '114', '0', '3690cf2b90c165992677c2d3d61dbad0.jpg'), ('935', '1454062268', null, '0', '114', '0', 'b55851bfba00ea6741ca568034728662.jpg'), ('936', '1454062268', '1454062272', '0', '114', '1', '992e36604939e00adee689ee475add0a.jpg'), ('937', '1454062268', null, '0', '114', '0', '6a7dc3478f86309394a89873b4d743ea.jpg'), ('938', '1454062618', null, '0', '79', '1', '9286138925af6f01592d369df039cffc.jpg'), ('939', '1454062618', null, '0', '79', '0', '7ab297364a9518ea16a6b030e9ab7b8a.jpg'), ('940', '1454062618', null, '0', '79', '0', '19140df14a0a5ceb42b42f80764fc355.jpg'), ('941', '1454062618', null, '0', '79', '0', '9c4ecd02c5ed6b28563d844ca9b07c9b.jpg'), ('942', '1454062773', null, '0', '80', '0', '0c9f7afa3eb6e0f2a3d65e471d5da307.jpg'), ('943', '1454062774', null, '0', '80', '0', '9b2f1cf3b00966ff3111dbc7b4021fe6.jpg'), ('944', '1454062774', null, '0', '80', '0', '50a6b10f77b582361cbdfb1496daf1a0.jpg'), ('945', '1454062774', '1454062778', '0', '80', '1', '27a7915a23137d61f99bc2a20df0dc61.jpg'), ('946', '1454062891', null, '0', '81', '1', '3e7b9e0e1683ca0fe9a851c4c3051520.jpg'), ('947', '1454062891', null, '0', '81', '0', 'e3a6bd35af72c12bc71ee2a54a1a3de1.jpg'), ('948', '1454062891', null, '0', '81', '0', 'dd3352263b67f9a846b282d93e050701.jpg'), ('949', '1454063107', null, '0', '115', '0', '250a8fe994e364a3773d7a37c57c9985.jpg'), ('950', '1454063107', '1454063112', '0', '115', '1', '5bb962a05bf5a36940a77ba77e496ac6.jpg'), ('951', '1454063107', null, '0', '115', '0', '672a7e3dda7c1c8d497aab89cfbcb35c.jpg'), ('952', '1454063263', null, '0', '116', '0', 'ca3de76337a3902847896ed0a1b06d64.jpg'), ('953', '1454063264', '1454063268', '0', '116', '1', 'bf2ae8500081b45c16b214f51e50d39e.jpg'), ('954', '1454063264', null, '0', '116', '0', '486702ea00e43dd359416babcc76d2da.jpg'), ('955', '1454063264', null, '0', '116', '0', '3e15bc76d50ac4b91791d5decf6eeda7.jpg'), ('956', '1454063363', null, '0', '117', '0', 'f013ce4cf791d03679469aa718f6f849.jpg'), ('957', '1454063363', null, '0', '117', '0', '1178795b54c1a91f9daef568e5bbc4ec.jpg'), ('958', '1454063363', '1454063367', '0', '117', '1', '3d087c18ee28a88fc99dc78933d842a1.jpg'), ('959', '1454063363', null, '0', '117', '0', '5642ce231fadd8fdc28d4d0a3d0b3eea.jpg'), ('960', '1454063483', null, '0', '118', '0', 'bf00cbf5b566f593ae21362a13b0305a.jpg'), ('961', '1454063483', '1454063486', '0', '118', '1', '69efd778c57c7f5e8561eec011853107.jpg'), ('962', '1454063483', null, '0', '118', '0', '8eae61a75de204bd069f97771d9ce798.jpg'), ('963', '1454063563', null, '0', '119', '0', 'f37787bc2ccff45d2112176b593251a0.jpg'), ('964', '1454063564', '1454063568', '0', '119', '1', '92f3b935559751b2265a53c2fecf62ef.jpg'), ('965', '1454063564', null, '0', '119', '0', '5e2b688fc6f3264f69f0a35dfce03bcf.jpg'), ('966', '1454319171', null, '0', '120', '1', 'c0fec459280ee93b12b4e4a711705309.jpg'), ('967', '1454319171', null, '0', '120', '0', 'ff807ce787c61c6f1986c3673f181e97.jpg'), ('968', '1454319171', null, '0', '120', '0', 'b71ca9198c831d5bbb7aa7313f96432b.jpg'), ('969', '1454319242', null, '0', '121', '0', '1015f5cb50794306d2b4125ca59f5584.jpg'), ('970', '1454319243', '1454319246', '0', '121', '0', '2a9e943fa03374c77eea2d57ab7bea8b.jpg'), ('971', '1454319243', '1454319248', '0', '121', '1', '971c2e8906aa445de06e2a0bf33f0b87.jpg'), ('972', '1454319243', null, '0', '121', '0', 'f9a8f692a43aa33d4a34398966ec8efb.jpg'), ('973', '1454319244', null, '0', '121', '0', 'ae75b887c7022d28ae4c68fd3a05c566.jpg'), ('974', '1454319314', null, '0', '122', '0', 'e189a03cc2b6174e231a98d96aece6d0.jpg'), ('975', '1454319314', '1454319318', '0', '122', '1', '8c7b7cecca6ff092982454f7334ab879.jpg'), ('976', '1454319315', null, '0', '122', '0', '4ccf2c4bb581f46f9e536c824fb1456f.jpg'), ('977', '1454319365', null, '0', '123', '0', '0c6d6feef5e9c20657a192dd10236dbe.jpg'), ('978', '1454319366', null, '0', '123', '0', '5302e9694024fe5558e1146096437aae.jpg'), ('979', '1454319367', null, '0', '123', '0', '4238115c559a911f53334943e9227da6.jpg'), ('980', '1454319367', '1454319371', '0', '123', '1', 'a0977a205a70991491100a55f09d1368.jpg');
INSERT INTO `catalog_images1` VALUES ('981', '1454319412', null, '0', '124', '0', '2b8bc8adcaef88284d0b14e634ae7a45.jpg'), ('982', '1454319412', null, '0', '124', '0', '48bef535720a1f08c4bce4cf9490bc6d.jpg'), ('983', '1454319412', null, '0', '124', '0', 'c5bda8b9fb179cdeec5c7eb74f389c08.jpg'), ('984', '1454319412', null, '0', '124', '0', '24d0cf99fcccb969d5aecf34116109cc.jpg'), ('985', '1454319412', null, '0', '124', '0', '81f02fbc87f9c57d1e5249ca5fb5f2e4.jpg'), ('986', '1454319412', '1454319415', '0', '124', '1', '00538151c9f5b0c2548a2dd7aff6339e.jpg'), ('987', '1454319856', null, '0', '125', '1', '59d38fdc0ca405a5b33d3c7841fd842b.jpg'), ('988', '1454319857', null, '0', '125', '0', 'ea4af92df0545e742442fd9521557295.jpg'), ('989', '1454319857', null, '0', '125', '0', '8e2d0cec4b76fefc8612be3cba8f68c4.jpg'), ('990', '1454319905', null, '0', '126', '0', '6ce02f5777176954b1ffc94d953d65ec.jpg'), ('991', '1454319905', '1454319908', '0', '126', '1', '654581ae81effacc3a69f675c1fe3a94.jpg'), ('992', '1454319905', null, '0', '126', '0', 'c82f273fd9e369474092ae0a9c462962.jpg'), ('993', '1454319950', null, '0', '127', '0', 'dd1880d4a59d9bc70554db980450f937.jpg'), ('994', '1454319950', null, '0', '127', '0', '85fcae91f7f12197f0d6098462d727b3.jpg'), ('995', '1454319951', '1454319954', '0', '127', '1', '720d0d3efa105c644950af32522f4045.jpg'), ('996', '1454320009', null, '0', '128', '0', 'a9578d07b90f63897ee57d84785341b1.jpg'), ('997', '1454320009', '1454320016', '0', '128', '1', 'b22809828871f8e09526177f0baed2b1.jpg'), ('998', '1454320010', '1454320013', '0', '128', '0', '242d03aca626cf78fbca10d7bb14afc8.jpg'), ('999', '1454320070', null, '0', '129', '0', 'f414790001dee545a59e82d57c48e446.jpg'), ('1000', '1454320070', null, '0', '129', '0', 'ab943c7b21343b3e5269ecb6d4e2be8f.jpg'), ('1001', '1454320070', null, '0', '129', '0', '18b8a15810f4557c7e42175a12bf0683.jpg'), ('1002', '1454320070', '1454320074', '0', '129', '1', 'b872e6cbc18e01ce6047b418aa0b2f88.jpg'), ('1003', '1454320433', null, '0', '130', '0', 'e0846d35b79b97059ac928e8784759af.jpg'), ('1004', '1454320434', '1454320437', '0', '130', '1', '4c81d2327a33a5aa58e18501eccf17a5.jpg'), ('1005', '1454320492', null, '0', '131', '0', 'be913b0dc5694f62c5f4326f5ce17d7e.jpg'), ('1006', '1454320492', null, '0', '131', '0', 'c62608b28945e55efad0c9318ad97844.jpg'), ('1007', '1454320493', '1454320496', '0', '131', '1', '2e49d83ce27e526f8e7cc578406fb5a8.jpg'), ('1008', '1454320557', null, '0', '132', '0', 'e210d5846ba2eceec2ba942d4002a5ea.jpg'), ('1009', '1454320557', null, '0', '132', '0', '095b4891b67b0872d09dcfdea6cf2a62.jpg'), ('1010', '1454320557', '1454320560', '0', '132', '1', 'cb0ab2f6740708dff3d7607f41163153.jpg'), ('1011', '1454320558', null, '0', '132', '0', 'c59d05b415af1a10a7b35f26e84fdbeb.jpg'), ('1012', '1454320615', null, '0', '133', '0', '26222e1e97d9b8873897856e6938dbef.jpg'), ('1013', '1454320616', '1454320619', '0', '133', '1', '7991833d4e097829b93dd9f278b40c14.jpg'), ('1014', '1454320668', null, '0', '134', '0', '46906e45201ceb883ca4b3e43fb0a08b.jpg'), ('1015', '1454320668', null, '0', '134', '0', '6614b56452da935d34639892ad2328d5.jpg'), ('1016', '1454320668', null, '0', '134', '0', '890c520e810507ccd696bb43f1f699e2.jpg'), ('1017', '1454320668', '1454320672', '0', '134', '1', '857b1bc39a911a1a24ae239ee59d16e2.jpg'), ('1018', '1454322295', null, '0', '135', '0', '17086e50d45528d8c80a0bad0d36d003.jpg'), ('1019', '1454322295', null, '0', '135', '0', 'e6f10d735dc72e567c9e93d0d8339744.jpg'), ('1020', '1454322295', '1454322299', '0', '135', '1', '3bfdb6e9c6abd9d58741a9afd69a73d1.jpg'), ('1021', '1454322346', null, '0', '136', '0', '0b49df8cb67727af6eb26fdb795415a3.jpg'), ('1022', '1454322346', '1454322350', '0', '136', '1', '98a88f9a611f19fda6d0507bfeb84ef4.jpg'), ('1023', '1454322347', null, '0', '136', '0', '62b142faa5eb460446b7435d05b7dd5f.jpg'), ('1024', '1454322395', null, '0', '137', '0', '53ff3671332a1d6662d7097ef02447d6.jpg'), ('1025', '1454322395', '1454322399', '0', '137', '1', '2ab31caa02f13ca7fe793fc6ffde3935.jpg'), ('1026', '1454322396', null, '0', '137', '0', '8ea3fa1cb1e70c61d7699e32dd62543f.jpg'), ('1027', '1454322444', null, '0', '138', '0', '5f93fd8536213e1b493fba0c0cb1128b.jpg'), ('1028', '1454322444', null, '0', '138', '0', 'f82cea06efc222807a7c4c818b2c53f0.jpg'), ('1029', '1454322444', null, '0', '138', '0', '4f939283bcfade8414883cd63f9ed2b1.jpg'), ('1030', '1454322444', '1454322448', '0', '138', '1', 'dd84e3af3ac388c165830cf5c2a99be1.jpg'), ('1031', '1454322445', null, '0', '138', '0', 'c1a9e596b0f7f1be6cd0327f2f2803d8.jpg'), ('1032', '1454322445', null, '0', '138', '0', 'c5575242fd29e5361bec9af5654d22d4.jpg'), ('1033', '1454322495', null, '0', '139', '0', '7383116a90c0ffc6a3b3879afd21c9db.jpg'), ('1034', '1454322495', null, '0', '139', '0', 'ae1cce1c0b3063dcad4aaea5e97c4d24.jpg'), ('1035', '1454322495', null, '0', '139', '0', '851352032583039a485fc68b17fb9605.jpg'), ('1036', '1454322495', '1454322499', '0', '139', '1', '33d7d2b06133a49ca5fe0f732ebf5d5b.jpg'), ('1037', '1454323558', null, '0', '140', '1', 'ce8d895d2c71cfb9a98da843140265a7.jpg'), ('1038', '1454323558', null, '0', '140', '0', 'e7c130e6bc23cc2e6909dd44a2b8edd8.jpg'), ('1039', '1454323597', null, '0', '141', '1', 'da41ddcc27ee136176fdce29738682b2.jpg'), ('1040', '1454323597', null, '0', '141', '0', '40438702f208dc7a846de7fd044fdc3f.jpg'), ('1041', '1454323638', null, '0', '142', '1', '0677e1ad1f6541eee5747d28058c8180.jpg'), ('1042', '1454323638', null, '0', '142', '0', 'e9ed81d1338738690da8a66ccfe64caa.jpg'), ('1043', '1454323638', null, '0', '142', '0', '8d12653145ce327fd9fd529bf97ed832.jpg'), ('1044', '1454323638', null, '0', '142', '0', '575f35dc33c347d187b07d0ba140483c.jpg'), ('1045', '1454323690', null, '0', '143', '0', '326971d7fdcea2ebc896edba1289c5b8.jpg'), ('1046', '1454323691', null, '0', '143', '0', 'fea9c60dc43c4d63e41564e3f5195222.jpg'), ('1047', '1454323691', '1454323695', '0', '143', '1', '68d868374b6db3d1cf9bea2f72027e31.jpg'), ('1048', '1454323732', null, '0', '144', '0', '5c982d0f22ca28a981b1279b07071224.jpg'), ('1049', '1454323732', null, '0', '144', '0', 'f5912eca33cdd38c8eb926dfc3a4a515.jpg'), ('1050', '1454323733', '1454323736', '0', '144', '1', '43de9d0f84d292f30c5ac0451faff7de.jpg'), ('1051', '1454324150', null, '0', '145', '0', '45353260ac145a792c6473702dc3a245.jpg'), ('1052', '1454324150', null, '0', '145', '0', '0014bf9b863376bac33ba1cf220ef9c1.jpg'), ('1053', '1454324221', '1454324224', '0', '145', '1', '2c57611d39d0339269ecba567a749967.jpg'), ('1054', '1454324435', null, '0', '146', '0', 'bb0fb34187c83bc9bce342222d5e6761.jpg'), ('1055', '1454324436', null, '0', '146', '0', '521f766892d6953a9be16b9ba10b016c.jpg'), ('1056', '1454324436', '1454324439', '0', '146', '1', '120a2393910ce4cad4acd9300c124ca4.jpg'), ('1057', '1454324483', null, '0', '147', '0', 'a09d61fb3daa2187d6b0a973be323fae.jpg'), ('1058', '1454324483', null, '0', '147', '0', '962c84e6f34d053265bde8e9ee17cebc.jpg'), ('1059', '1454324483', null, '0', '147', '0', 'c61fa782753805642c5bbc08772aec33.jpg'), ('1060', '1454324483', '1454324486', '0', '147', '1', '2e70fbd4a6418b0d25f58157243ec680.jpg'), ('1061', '1454324549', null, '0', '148', '0', '0e21d9a8d4d8443d5121adb22c7a3c48.jpg'), ('1062', '1454324550', '1454324554', '0', '148', '1', '7e6ed9ff208f16a45ab64257ba9f3a72.jpg'), ('1063', '1454324550', null, '0', '148', '0', '113cdbec9b7ebb3e1bc063ca79d47584.jpg'), ('1064', '1454324595', null, '0', '149', '0', '9840481770e3971b1e3eb0602e4ef8fa.jpg'), ('1065', '1454324596', '1454324599', '0', '149', '1', '8b80482102762d94fb58b6db7e57550c.jpg'), ('1066', '1454324596', null, '0', '149', '0', 'c90cb4ce6c0405c147528e3f54e05a15.jpg'), ('1067', '1454324992', null, '0', '150', '1', 'f7202f3d2445ba1f4fa2839437e2b9b9.jpg'), ('1068', '1454324992', null, '0', '150', '0', 'a11e493b6e6499bed3eefea74fa7b8eb.jpg'), ('1069', '1454324992', null, '0', '150', '0', '9b1c229f467cf4fd476137d50ab7621f.jpg'), ('1070', '1454324992', null, '0', '150', '0', 'f77d7fcd5276da5d38b69dd2ecc946ef.jpg'), ('1071', '1454325041', null, '0', '151', '0', 'b13b198a8f241222e11d3dbf1f39945d.jpg'), ('1072', '1454325041', '1454325045', '0', '151', '1', '07ee6659e7a415e9d34f6a93b540908e.jpg'), ('1073', '1454325041', null, '0', '151', '0', '3aa351779f28a84504dff5cbc609d13a.jpg'), ('1074', '1454325042', null, '0', '151', '0', 'a8b5fd53b719c7d91db04e164bacfc09.jpg'), ('1075', '1454325090', null, '0', '152', '0', 'f51fa7d3e0c760a5261a21d55b9043bb.jpg'), ('1076', '1454325090', '1454325095', '0', '152', '1', '0a4eff388b67a57a083f654ac8149b0b.jpg'), ('1077', '1454325091', null, '0', '152', '0', '76617532a08b25cc659055fac6e55cb9.jpg'), ('1078', '1454325091', null, '0', '152', '0', 'd9ddcf29ba1a7f649373f5b979c58301.jpg'), ('1079', '1454325146', null, '0', '153', '0', 'd871aa2b96326c15066e08b700517ed3.jpg'), ('1080', '1454325146', '1454325149', '0', '153', '1', '698e3ecaee23d4e5f2f1530d0c240ad8.jpg');
INSERT INTO `catalog_images1` VALUES ('1081', '1454325146', null, '0', '153', '0', '73bfd89a892a80532ca90638490b6310.jpg'), ('1082', '1454325188', '1455271808', '0', '154', '1', 'a1263630a87a89b5226af70435ba8254.jpg'), ('1083', '1454325188', '1455271807', '0', '154', '0', 'acbc57eebac7833ff4b8b9922cccfa83.jpg'), ('1084', '1454325188', '1454325191', '0', '154', '0', '0ab43126cd4348da5a5c2cf762f680e8.jpg'), ('1085', '1455806196', null, '0', '154', '0', 'ec8c6a0a41584b9206686503bbf15a4b.jpg'), ('1086', '1462879188', null, '0', '155', '1', '8d2ec33b46ebc352b67098759eebe5fc.jpg'), ('1087', '1462879776', null, '0', '156', '1', 'eab6d6b4851e790b52d71813e8537b8c.jpg'), ('1088', '1463996837', null, '0', '157', '1', '0cdf3a8e39c4cfb545c4e0da3a719c6f.jpg'), ('1089', '1464162034', null, '0', '156', '0', 'd55ccad693f675098834733e4ff31e57.JPG'), ('1090', '1464612672', null, '0', '158', '1', '876558828e8bf44fd34c1638e82bfdb4.jpg'), ('1091', '1464684600', null, '0', '161', '1', 'c6121f9633e13ce34552ba96c741d096.jpg'), ('1092', '1464685042', null, '0', '160', '1', '1b22ffdbad1330328b791e4a253e6d97.jpg'), ('1093', '1464685157', null, '0', '162', '1', 'ae7cd5e751f1f6c5305e881d8a38622d.jpg'), ('1094', '1465035038', null, '0', '163', '1', 'db6bf874a183ef6709366777e38b5f8b.jpg'), ('1095', '1485243789', null, '0', '163', '0', '78913ab853a7861d461bd1cb2660244f.jpg'), ('1096', '1485346633', null, '0', '48', '0', 'a051147ac0abb55ecf2fb0573ca1d767.jpg'), ('1097', '1492005412', null, '0', '163', '0', '70097c952dff895f9ed113ff205ed87d.jpg'), ('1098', '1492005432', null, '0', '163', '0', '2b26bbd625f28853c8c72e539dc56108.jpg');
COMMIT;

-- ----------------------------
-- Table structure for `catalog_tree1`
-- ----------------------------
DROP TABLE IF EXISTS `catalog_tree1`;
CREATE TABLE `catalog_tree1` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`status`  tinyint(1) NOT NULL DEFAULT 0 ,
`sort`  int(10) NOT NULL DEFAULT 0 ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`parent_id`  int(10) NOT NULL DEFAULT 0 ,
`image`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`h1`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`keywords`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`text`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`views`  int(10) NOT NULL DEFAULT 0 ,
PRIMARY KEY (`id`),
UNIQUE INDEX `alias` (`alias`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=55

;

-- ----------------------------
-- Records of catalog_tree1
-- ----------------------------
BEGIN;
INSERT INTO `catalog_tree1` VALUES ('26', '1447093601', '1465031632', '1', '4', 'Одежда', 'odezhda', '0', '48aae17790ec529e0deb450087d5a22a.jpg', '', '', '', '', '', '85'), ('27', '1447093637', '1491318749', '1', '5', 'Обувь', 'obuv', '0', '8f8f36d63e317825d5e1edfd8d075f83.jpg', '', '', '', '', '', '40'), ('28', '1447093670', '1465031632', '1', '6', 'Аксессуары', 'aksessuary', '0', '09a2324dbaac9188f59a757813092cb0.jpg', '', '', '', '', '', '9'), ('29', '1447093805', '1492169250', '1', '2', 'Верхняя одежда', 'verhnjaja-odezhda', '26', 'ec2be2aacbdc7ff22fa118fa4eddfe47.jpg', '', '', '', '', '', '24743'), ('30', '1447093854', '1465031632', '1', '1', 'Рубашки', 'rubashki', '26', '9aac3ade273a7cad5fb4902617d809f8.jpg', '', '', '', '', '', '1468'), ('31', '1447093893', '1492434998', '1', '0', 'Джинсы', 'dzhinsy', '26', 'ef9e44074700978e659eb3d91fe6e3e0.jpg', '', '', '', '', '', '1028'), ('32', '1447093942', '1492429493', '1', '2', 'Кроссовки', 'krossovki', '27', '6227fc9615898a617aa48711effe7680.jpg', '', '', '', '', '', '4190'), ('33', '1447093982', '1465031632', '1', '1', 'Туфли', 'tufli', '27', 'b06349dbd853de3031d9badbbe351841.jpeg', '', '', '', '', '', '185'), ('34', '1447094016', '1485500477', '1', '0', 'Мокасины', 'mokasiny', '27', 'adaa76210381b3db6eec65c764a34ec8.jpg', '', '', '', '', '', '80'), ('35', '1447094058', '1465031632', '1', '1', 'Сумки', 'sumki', '28', 'a722cded1894ae53dfa4dddc32161678.jpg', '', '', '', '', '', '63433'), ('36', '1447094096', '1465031632', '1', '0', 'Очки', 'ochki', '28', '30864db826139a5a2669e15d71eed076.jpg', '', '', '', '', '', '130'), ('37', '1451394013', '1465031632', '0', '3', 'Одежда', 'odezhda5300', '0', null, '', '', '', '', '', '3'), ('38', '1451394085', '1465031632', '0', '2', 'Обувь', 'obuv2145', '0', '4847829b7e2ca3b3f50ccea16e83b2ed.jpg', '', '', '', '', '', '8'), ('39', '1463996587', '1507625736', '1', '1', 'Товары для дома', 'tovary-dlja-doma', '0', '104ddf07007d0d6e99809e3e2c1023b6.png', '', '', '', '', '', '10'), ('40', '1463996672', '1507542073', '1', '3', 'Подушки', 'podushki', '39', '77c73ff05c047984aea14d74cb0ded10.jpg', '', '', '', '', '', '20'), ('41', '1464612090', '1507625733', '1', '2', 'Одеяла', '111', '39', 'c7a248c03d2f31672867dd873fab50d4.jpg', '', '', '', '', '', '5'), ('42', '1464615493', '1465031632', '1', '1', 'одеяла', 'odejala', '39', '83aa45d30efc1dec00ace38b1cd00488.jpg', '', '', '', '', '', '2'), ('43', '1464617842', '1465031632', '0', '0', 'кухонные принадлежности', 'kuhonnye-prinadlezhnosti', '39', '2d50de58d70cc0ffa2218be233ec93bc.jpg', '', '', '', '', '', '38'), ('44', '1464618172', '1465031632', '0', '4', 'терки', 'terki', '43', '579ea07c47bdb7efb23efd9b7fb7167a.jpg', '', '', '', '', '', '3'), ('45', '1464619087', '1465031632', '0', '3', 'терки', 'terki5074', '43', '0b7490c22ae85328e4e2f485a374abee.jpg', '', '', '', '', '', '3'), ('46', '1464619392', '1465031632', '0', '2', 'мельница', 'melnitsa', '43', '60cf262f591d9d3e77d1814568fcbff6.jpg', '', '', '', '', '', '0'), ('47', '1464626593', '1465031632', '0', '1', 'ложки', 'lozhki', '43', '6f7aaa4feb4d8d0abf417a4b473dab25.jpg', '', '', '', '', '', '5'), ('48', '1464626769', '1465031632', '0', '0', 'ложки деревянные', 'lozhki-derevjannye', '43', '48cbbc9bb8b91431fb7f72cb3fcbdc55.jpg', '', '', '', '', '', '4'), ('49', '1465024995', '1507618465', '1', '0', 'Куртки женские', 'kurtki-zhenskie', '0', null, '', '', '', '', '', '14'), ('50', '1465025360', '1507618463', '1', '3', 'Куртка демисезонная', 'kurtka-demisezonnaja', '49', '30d9f42a17360cb933e02fad5917e77f.jpg', '', '', '', '', '', '4'), ('51', '1465025936', '1465222064', '1', '2', 'новинка', 'novinka', '49', 'defe6b378d5ed0f063a4a2ab4ecc9e49.jpg', '', '', '', '', '', '7'), ('52', '1465026055', '1507617436', '1', '1', 'Теплые ', 'teplye-', '49', '0dc84a51d44455d52d63dc1929fc4387.jpg', '', '', '', '', '', '6'), ('53', '1465026124', '1507541653', '1', '0', 'Комфорт', 'komfort', '49', '31efac6cace062443170643d5560e7a5.jpg', '', '', '', '', '', '6'), ('54', '1465031600', '1465031632', '1', '0', 'модница', 'modnitsa', '50', 'ebbb80c52652c2761f4af8e874b1fd5d.jpg', '', '', '', '', '', '0');
COMMIT;

-- ----------------------------
-- Table structure for `config`
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`zna`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`status`  int(1) NULL DEFAULT NULL ,
`sort`  int(11) NULL DEFAULT NULL ,
`key`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`valid`  tinyint(1) NOT NULL DEFAULT 1 ,
`type`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`values`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Возможные значения в json массиве ключ => значение. Для селекта и радио' ,
`group`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Группа настроек' ,
PRIMARY KEY (`id`),
FOREIGN KEY (`group`) REFERENCES `config_groups` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE,
FOREIGN KEY (`type`) REFERENCES `config_types1` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE,
INDEX `block` (`group`) USING BTREE ,
INDEX `type` (`type`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=42

;

-- ----------------------------
-- Records of config
-- ----------------------------
BEGIN;
INSERT INTO `config` VALUES ('1', 'E-Mail администратора сайта (отправитель по умолчанию)', 'veselovskaya.wezom@gmail.com', '1434885560', '1', '1', 'admin_email', '1', 'input', null, 'mail'), ('2', 'Название сайта', 'CMS v4.0', '1434885560', '1', '1', 'name_site', '1', 'input', null, 'basic'), ('4', 'Copyright', '2014 © Интернет магазин спортивной обуви и одежды', '1434885560', '1', '4', 'copyright', '1', 'input', null, 'static'), ('5', 'Отображение всплывающих сообщений на сайте', 'topRight', '1434835221', '0', '2000', 'message_output', '1', 'radio', '[{\"key\":\"Отображать вверху\",\"value\":\"top\"},{\"key\":\"Отображать вверху слева\",\"value\":\"topLeft\"},{\"key\":\"Отображать вверху по центру\",\"value\":\"topCenter\"},{\"key\":\"Отображать вверху справа\",\"value\":\"topRight\"},{\"key\":\"Отображать по центру слева\",\"value\":\"centerLeft\"},{\"key\":\"Отображать по центру\",\"value\":\"center\"},{\"key\":\"Отображать по центру справа\",\"value\":\"centerRight\"},{\"key\":\"Отображать внизу слева\",\"value\":\"bottomLeft\"},{\"key\":\"Отображать внизу по центру\",\"value\":\"bottomCenter\"},{\"key\":\"Отображать внизу справа\",\"value\":\"bottomRight\"},{\"key\":\"Отображать внизу\",\"value\":\"bottom\"}]', 'basic'), ('6', 'Номер телефона в шапке сайта', '+7(495) 506-22-84', '1434885560', '1', '6', 'phone', '0', 'input', null, 'static'), ('7', 'Количество товаров на странице', '15', '1434885560', '1', '7', 'limit', '1', 'input', null, 'basic'), ('8', 'Количество статей на главной странице', '1', '1434885560', '1', '8', 'limit_articles_main_page', '1', 'input', null, 'basic'), ('9', 'Количество строк в админ-панели', '10', '1434885560', '1', '9', 'limit_backend', '1', 'input', null, 'basic'), ('10', 'VK.com', 'http://vk.com/airpacgroup', '1434885560', '1', '10', 'vk', '0', 'input', null, 'socials'), ('11', 'FB.com', 'https://www.facebook.com', '1434885560', '1', '11', 'fb', '0', 'input', null, 'socials'), ('12', 'Instagram', 'http://instagram.com', '1434885560', '1', '12', 'instagram', '0', 'input', null, 'socials'), ('13', 'Надпись в подвале сайта для подписчиков', 'Хочешь быть в числе первых, кому мы сообщим об акциях и новинках?!', '1434885560', '1', '5', 'subscribe_text', '0', 'input', null, 'static'), ('17', 'Количество новостей / статей на странице', '1', '1434885560', '1', '7', 'limit_articles', '1', 'input', null, 'basic'), ('18', 'Количество групп товаров на странице', '5', '1434885560', '1', '7', 'limit_groups', '1', 'input', null, 'basic'), ('19', 'Использовать СМТП', '1', '1434885560', '1', '3', 'smtp', '1', 'radio', '[{\"key\":\"Да\",\"value\":1},{\"key\":\"Нет\",\"value\":0}]', 'mail'), ('20', 'SMTP server', '', '1434885560', '1', '4', 'host', '0', 'input', null, 'mail'), ('22', 'Логин', '1111', '1434885560', '1', '5', 'username', '0', 'input', null, 'mail'), ('23', 'Пароль', '1111', '1434885560', '1', '6', 'pass', '0', 'password', null, 'mail'), ('24', 'Тип подключения', 'tls', '1434885560', '1', '7', 'secure', '0', 'select', '[{\"key\":\"TLS\",\"value\":\"tls\"},{\"key\":\"SSL\",\"value\":\"ssl\"}]', 'mail'), ('25', 'Порт. Например 465 или 587. (587 по умолчанию)', '587', '1434885560', '1', '8', 'port', '0', 'input', null, 'mail'), ('26', 'Имя латинницей (отображается в заголовке письма)', 'Info', '1434885560', '1', '2', 'name', '1', 'input', null, 'mail'), ('27', 'Запаролить сайт', '1', '1434885560', '1', '0', 'auth', '1', 'radio', '[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]', 'security'), ('28', 'Логин', '1', '1434885560', '1', '2', 'username', '0', 'input', null, 'security'), ('29', 'Пароль', '1', '1434885560', '1', '3', 'password', '0', 'password', null, 'security'), ('30', 'Сократить CSS u JavaScript', '0', '1434885561', '1', '1', 'minify', '1', 'radio', '[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]', 'speed'), ('31', 'Сократить HTML', '0', '1434885561', '1', '2', 'compress', '1', 'radio', '[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]', 'speed'), ('32', 'Кеширование размера изображений', '0', null, '1', '3', 'cache_images', '1', 'select', '[{\"key\":\"Выключить\",\"value\":\"0\"},{\"key\":\"12 часов\",\"value\":\"0.5\"},{\"key\":\"День\",\"value\":\"1\"},{\"key\":\"3 дня\",\"value\":\"3\"},{\"key\":\"Неделя\",\"value\":\"7\"},{\"key\":\"2 недели\",\"value\":\"14\"},{\"key\":\"Месяц\",\"value\":\"30\"},{\"key\":\"Год\",\"value\":\"365\"}]', 'speed'), ('33', 'Наименование организации', 'wezom', null, '1', '1', 'organization', '1', 'input', null, 'microdata'), ('34', 'Автор статей', 'wezom author', null, '1', '2', 'author', '1', 'input ', null, 'microdata'), ('35', 'Количество отзывов на странице', '10', null, '1', '8', 'limit_reviews', '1', 'input', null, 'basic'), ('36', 'Наименование магазина', 'cms4', null, '1', '1', 'name', '1', 'input', null, 'export'), ('37', 'Наименование компании', 'wezom', null, '1', '2', 'company', '1', 'input', null, 'export'), ('38', 'Email разработчиков CMS или агентства, осуществляющего техподдержку.', 'alyohina.i.wezom@gmail.com', null, '1', '3', 'email', '1', 'input', null, 'export'), ('40', 'Использовать защищённое ssl соединение?', '0', null, '1', '5', 'ssl', '1', 'radio', '[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]', 'security'), ('41', 'Ключ googlemaps', 'AIzaSyDGMr7U3w0bDthcw1jqIQRLbz-zXJmd0K4', '1505459047', '1', '100', 'googlemapskey', '0', 'input', null, 'basic');
COMMIT;

-- ----------------------------
-- Table structure for `config_groups`
-- ----------------------------
DROP TABLE IF EXISTS `config_groups`;
CREATE TABLE `config_groups` (
`id`  int(4) NOT NULL AUTO_INCREMENT ,
`name`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`alias`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`side`  varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'left' COMMENT 'left, right' ,
`status`  tinyint(1) NOT NULL DEFAULT 1 ,
`sort`  int(4) NOT NULL DEFAULT 0 ,
PRIMARY KEY (`id`),
UNIQUE INDEX `alias` (`alias`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=9

;

-- ----------------------------
-- Records of config_groups
-- ----------------------------
BEGIN;
INSERT INTO `config_groups` VALUES ('1', 'Почта', 'mail', 'right', '1', '1'), ('2', 'Базовые', 'basic', 'left', '1', '1'), ('3', 'Статика', 'static', 'left', '1', '2'), ('4', 'Соц. сети', 'socials', 'left', '1', '3'), ('5', 'Безопасность', 'security', 'right', '1', '2'), ('6', 'Быстродействие', 'speed', 'right', '1', '3'), ('7', 'Микроразметка', 'microdata', 'left', '1', '4'), ('8', 'Выгрузки', 'export', 'left', '1', '5');
COMMIT;

-- ----------------------------
-- Table structure for `config_types1`
-- ----------------------------
DROP TABLE IF EXISTS `config_types1`;
CREATE TABLE `config_types1` (
`id`  int(4) NOT NULL AUTO_INCREMENT ,
`name`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`alias`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
PRIMARY KEY (`id`),
UNIQUE INDEX `alias` (`alias`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=7

;

-- ----------------------------
-- Records of config_types1
-- ----------------------------
BEGIN;
INSERT INTO `config_types1` VALUES ('1', 'Однострочное текстовое поле', 'input'), ('2', 'Текстовое поле', 'textarea'), ('3', 'Выбор значения из списка', 'select'), ('4', 'Пароль', 'password'), ('5', 'Радиокнопка', 'radio'), ('6', 'Текстовое поле с редактором', 'tiny');
COMMIT;

-- ----------------------------
-- Table structure for `contactsInfoUSE`
-- ----------------------------
DROP TABLE IF EXISTS `contactsInfoUSE`;
CREATE TABLE `contactsInfoUSE` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`text`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`status`  int(11) NULL DEFAULT NULL ,
`updated_at`  int(11) NULL DEFAULT NULL ,
`sort`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of contactsInfoUSE
-- ----------------------------
BEGIN;
INSERT INTO `contactsInfoUSE` VALUES ('1', 'phone', '+38(050)552-88-02', '1', null, '0'), ('2', 'adress', 'г. Славутич, Черниговский кв, д. 35', '1', null, '1');
COMMIT;

-- ----------------------------
-- Table structure for `content`
-- ----------------------------
DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`name`  varchar(250) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL DEFAULT NULL ,
`alias`  varchar(250) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL DEFAULT NULL ,
`title`  text CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL ,
`description`  text CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL ,
`keywords`  text CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL ,
`text`  text CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL ,
`status`  int(1) NULL DEFAULT 1 ,
`created_at`  int(10) NULL DEFAULT NULL ,
`h1`  varchar(250) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`sort`  int(11) NULL DEFAULT 0 ,
`parent_id`  int(10) NOT NULL DEFAULT 0 ,
`class`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`views`  int(10) NOT NULL DEFAULT 0 ,
PRIMARY KEY (`id`),
UNIQUE INDEX `action` (`alias`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=31

;

-- ----------------------------
-- Records of content
-- ----------------------------
BEGIN;
INSERT INTO `content` VALUES ('27', 'Оплата и доставка', 'payment-and-delivery', 'Оплата и доставка', 'Оплата и доставка', 'Оплата и доставка', '<p>Компания OSIS MASSAGE TECHNOLOGIES высоко ценит время и настроение здоровья своих клиентов. Поэтому все расходы и трудности связанные с доставкой и установкой массажного оборудования берет на себя.</p>\r\n<p>Все что Вам необходимо сделать, это оформить заказ на сайте или по телефону.</p>\r\n<p>Наши установщики согласуют с Вами дату и время установки, и все - Вы становитесь счастливым клиентом OSIS.</p>\r\n<p>После установки Вы обязательно получите гарантийный сертификат, товарный чек и инструкцию.</p>\r\n<p>Для наших клиентов доставка и установка по всей Украине - бесплатна!</p>', '1', '1500980791', 'Оплата и доставка', '1507698846', '3', '0', null, '40'), ('28', 'О нас', 'about-us', 'О нас', 'О нас', 'О нас', '<h3>OSIS - настроение здоровья!</h3>\r\n<p>OSIS MASSAGE TECHNOLOGIES - это крупнейший производитель массажного оборудования в США. Лидер в технологиях производства, качестве продукции и уровня сервиса. Но есть лидерство, которое вызывает особенную гордость - более 90% покупателей продукции OSIS становятся настоящими поклонниками продукции!</p>\r\n<p>OSIS MASSAGE TECHNOLOGIES располагает всеми ресурсами, чтобы обеспечить широчайший ассортимент, высокое качество, лучшие цены и сроки поставки. Такими ресурсами являются: собственные фабрики, обширные международные связи, собственный отдел логистики, позволяющий самостоятельно доставлять продукцию с завода-изготовителя. Мощное и высокотехнологичное производство оснащено автоматическими линиями последнего поколения и современными программами оптимизации.</p>\r\n<p><img class=\"content-image content-image--width-100-and-more content-image--width-200-and-more content-image--width-300-and-more content-image--width-400-and-more content-image--width-500-and-more content-image--width-600-and-more content-image--width-700-and-more content-image--width-800-and-more content-image--width-900-and-more\" src=\"http://inkubator.ks.ua/html/osis/images/about-1.jpg?v1500980030405\" alt=\"\" width=\"936\" height=\"805\" /></p>\r\n<p>OSIS MASSAGE TECHNOLOGIES - это:</p>\r\n<ul>\r\n<li>Самое инновационное и современное массажное оборудование</li>\r\n<li>Передовые технологии производства</li>\r\n<li>Использование лучших конструкторских и дизайнерских разработок</li>\r\n<li>Самый широкий ассортимент серийной продукции</li>\r\n<li>Кратчайшие сроки поставки</li>\r\n</ul>\r\n<p>Наша цель - создавать условия для Вашего отдыха. Становясь клиентом компании OSIS, Вы получаете заботу о своём здоровье с использованием профессиональных массажных технологий, объединивших многолетние традиции и инновационные разработки.</p>\r\n<p>Команда доброжелательных квалифицированных консультантов поможет Вам сориентироваться во всем многообразии массажной продукции. Любой менеджер нашей компании поможет Вам подобрать оптимальную модель, исходя из Ваших потребностей, даст профессиональные советы по использованию, и примет заявку на доставку продукции в удобное для Вас время и место.</p>', '1', '1500981108', 'О нас', '1507020639', '1', '0', null, '51'), ('29', 'Видео', 'video', 'Видео', 'Видео', 'Видео', '<p>Видео</p>', '1', '1501566148', 'Видео', '1506949618', '0', '0', null, '3'), ('30', 'test', 'test', '', '', '', '', '1', '1504684825', '', '1505127781', '0', '0', null, '2');
COMMIT;

-- ----------------------------
-- Table structure for `controlUSE`
-- ----------------------------
DROP TABLE IF EXISTS `controlUSE`;
CREATE TABLE `controlUSE` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`h1`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`keywords`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`text`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`alias`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`status`  tinyint(1) NULL DEFAULT NULL ,
`other`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`updated_at`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=27

;

-- ----------------------------
-- Records of controlUSE
-- ----------------------------
BEGIN;
INSERT INTO `controlUSE` VALUES ('1', 'Главная страница', 'Тест верстка', 'Тест Верстка', 'Тест верстка', 'Тест верстка', '<h1>Интернет-магазин спортивной одежды и обуви</h1>\r\n<p>На сегодняшний день здоровый образ жизни стал не просто привилегией, а неким культом. Интернет магазин спортивной одежды позволяет без лишних затрат времени приобрести экипировку для любимого дела. Занимаетесь ли вы бегом, боксом, плаваньем, выбираете активный отдых или вас просто интересует спортивная одежда, интернет магазин &nbsp;&laquo;одежды и обувиpac&raquo; обеспечит вас всем необходимым. Главное при занятии спортом &ndash; это максимальный комфорт. Интернет магазин спортивной обуви позволит &laquo;обуть&raquo; ваши ножки, и они будут чувствовать себя по-королевски в любых условиях!</p>\r\n<h2>Спортивная обувь и одежда в наличии и под заказ</h2>\r\n<p>Наш каталог спортивной одежды удовлетворит запросы даже самых требовательных покупателей. Тут можно найти вещи на любой вкус, сделанные из разнообразных материалов: хлопок, полиэстер, эластан. На сегодня наш онлайн супермаркет - один из самых перспективных, мы заботимся о своей репутации, так что на наших страницах вас ждёт только фирменная обувь, аксессуары и вещи.</p>\r\n<h2>Ассортимент товаров</h2>\r\n<p>Украина с каждым годом всё ближе и ближе к европейскому уровню развития, а ведь именно там ударными темпами увеличивается уровень онлайн покупок. Интернет магазин спортивной одежды и обуви одежды и обувиpac с 2012 года добился впечатляющих успехов, а именно:</p>\r\n<ul>\r\n<li>до 4 тысяч пар спортивной обуви &ndash; мы лидируем среди интернет магазинов спортивной обуви;</li>\r\n<li>огромное количество курток;</li>\r\n<li>пуховики разных расцветок и фасонов, с разнообразной символикой, где присутствует не только Украина, но и другие страны;</li>\r\n<li>футболки;</li>\r\n<li>фирменная толстовка;</li>\r\n<li>кепки;</li>\r\n<li>разнообразные аксессуары.</li>\r\n</ul>\r\n<p>Купить спортивную одежду в Украине можно на любом рынке. Но где гарантия того, что она будет действительной качественная, фирменная и не навредит вместо того, чтоб принести пользу? Именно поэтому мы предоставляем все необходимые сертификаты качества &ndash; наша одежда брендовая, как и спортивная обувь, интернет магазин полностью уверен в тех материалах, из которых она состоит. С каждым днём мы стараемся предоставить клиентам ещё больше новинок и интересных моделей, повысить уровень обслуживания. Покупайте спортивную одежду, аксессуары и спортивную обувь в нашем интернет магазине, ведь вас ежедневно ждут акционные предложения, скидки и просто сюрпризы, чтоб покупки стали для вас ещё более приятным занятием!</p>\r\n<h2>Приобретайте брендовую спортивную одежду</h2>\r\n<p>Почему вам остановить выбор именно на нашем интернет магазине спортивной одежды и обуви? одежды и обувиpac даёт клиентам возможность оплатить свою покупку несколькими способами. Во-первых, вы можете передать деньги курьеру или же оплатить покупку в отделении перевозчика, то есть осуществить оплату наложенным платежом. Такой способ наиболее безопасный, но он несёт определённые потери &ndash; необходимо оплатить обратную доставку денег. Второй способ купить спортивную одежду в Украине &ndash; электронный перевод. В таком случае менеджер одежды и обувиpac отправляет вам реквизиты для оплаты на электронную почту или на мобильный телефон, и счёт можно оплатить через Приват24. Обязательное условие при переводе &ndash; сообщение нам точной суммы и времени перечисления денег.</p>\r\n<p>Совершив покупку в нашем интернет магазине спортивной обуви, доставку товара можно ожидать в течение 14-28 дней, если он отсутствует на Украине. Если же товар в наличии &ndash; его можно получить через 1-3 дня. Получить спортивную одежду из нашего интернет магазина можно с помощью Новой Почты, либо же с курьером, если вы живёте в Днепропетровске.</p>\r\n<p>Какие же гарантии, что вас ждёт именно брендовая спортивная одежда? Прежде всего, наша репутация как интернет магазина спортивной одежды для молодёжи и наработанная годами клиентская база говорит сама за себя. Также вы получаете двухмесячную гарантию на сам товар, который можно вернуть или обменять в случае возникновения непредвиденных дефектов, таких как расклеивание или отрывы по швам.</p>\r\n<p>Также у нас есть электронные страницы и сообщества в социальных сетях, где вы можете оставить свои отзывы, пожелания, разместить претензии или выразить неудовлетворённость.</p>\r\n<p>Интернет-магазин одежды и обувиpac &ndash; это только лучшие предложения, которые имеют доступную цену и высокое качество. Загляните к нам, и в каталоге спортивной одежды вы точно найдёте что-то подходящее для вас!</p>', 'index', '1', null, '1490347485'), ('2', 'Контакты', '', 'Контакты', 'Контакты', 'Контакты', '<p><strong>Контактная информация компании Интернет-магазин \"Airpac\"</strong></p>\r\n<p><br />Название:&nbsp;&nbsp; &nbsp;Интернет-магазин одежды и обуви<br />Контактное лицо:&nbsp;&nbsp;&nbsp; Администратор<br />Адрес:&nbsp;&nbsp; &nbsp;Только online-магазин, Украина<br />Телефон:&nbsp;&nbsp; &nbsp;<br />+38(XXX)XXX-XX-XX<br />+38(XXX)XXX-XX-XX<br />Email:&nbsp;&nbsp;&nbsp; <a href=\"mailto:office@wezom.com.u\">office@wezom.com.ua</a></p>', 'contacts', '1', '{\"longitude\":\"31.993292570114136\",\"latitude\":\"46.96953516353399\",\"zoom\":\"17\"}', '1505821278'), ('7', 'Карта сайта', 'Карта сайта', 'Карта сайта', 'Карта сайта', 'Карта сайта', null, 'sitemap', '1', null, null), ('9', 'Каталог продукции', '', 'Каталог', 'Каталог', 'Каталог', null, 'products', '1', null, null), ('17', 'Доставка', '', 'Доставка', 'Доставка', 'Доставка', 'Доставка', 'delivery', '1', null, null), ('18', 'О компании', null, 'О компании', 'О компании', 'О компании', 'О компании', 'aboutUs', '1', null, null), ('19', 'Прайс-лист', null, 'Прайс-лист', 'Прайс-лист', 'Прайс-лист', 'Прай-дист', 'priceList', '1', null, null), ('20', 'Карта сайта', null, 'Карта сайта', 'Карта сайта', 'Карта сайта ', 'Карта сайта', 'sitemap', '1', null, null), ('21', 'Текстовая страница', null, 'Текстовая страница', 'Текстовая страница', 'Текстовая страница', 'Текстовая страница', 'textPage', '1', null, null), ('22', 'Войти', null, 'Войти', 'Войти', 'Войти', 'Войти', 'logInForm', '1', null, null), ('23', null, null, null, null, null, null, 'login', '1', null, null), ('24', null, null, null, null, null, null, 'registF', '1', null, null), ('25', null, null, null, null, null, null, 'reg', '1', null, null), ('26', null, null, null, null, null, null, 'admin', '1', null, null);
COMMIT;

-- ----------------------------
-- Table structure for `cron`
-- ----------------------------
DROP TABLE IF EXISTS `cron`;
CREATE TABLE `cron` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`email`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`subject`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`text`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of cron
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `i18n`
-- ----------------------------
DROP TABLE IF EXISTS `i18n`;
CREATE TABLE `i18n` (
`id`  int(2) NOT NULL AUTO_INCREMENT ,
`alias`  varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`name`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`short_name`  varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`locale`  varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`default`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
PRIMARY KEY (`id`),
UNIQUE INDEX `alias_2` (`alias`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of i18n
-- ----------------------------
BEGIN;
INSERT INTO `i18n` VALUES ('1', 'ru', 'Русский', 'РУС', 'ru-ru', '1');
COMMIT;

-- ----------------------------
-- Table structure for `instructionsUSE`
-- ----------------------------
DROP TABLE IF EXISTS `instructionsUSE`;
CREATE TABLE `instructionsUSE` (
`id`  int(5) NOT NULL AUTO_INCREMENT ,
`alias`  varchar(75) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`text`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=7

;

-- ----------------------------
-- Records of instructionsUSE
-- ----------------------------
BEGIN;
INSERT INTO `instructionsUSE` VALUES ('1', 'callback', 'Заказать обратный звонок'), ('2', 'priceList', 'Посмотреть прайс-лист'), ('3', 'takeOrder', 'Оставить заявку'), ('4', 'aboutComany', 'Подробнее о компании'), ('5', 'download', 'Загрузить'), ('6', 'write', 'Написать нам');
COMMIT;

-- ----------------------------
-- Table structure for `models1`
-- ----------------------------
DROP TABLE IF EXISTS `models1`;
CREATE TABLE `models1` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`status`  tinyint(1) NOT NULL DEFAULT 0 ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`brand_alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`brand_alias`) REFERENCES `brands1` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE,
INDEX `alias` (`alias`) USING BTREE ,
INDEX `model_brand_alias` (`brand_alias`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=100

;

-- ----------------------------
-- Records of models1
-- ----------------------------
BEGIN;
INSERT INTO `models1` VALUES ('90', '1447092911', null, '1', '1300', '1300', 'newbalance'), ('91', '1447092920', null, '1', '1400', '1400', 'newbalance'), ('92', '1447092926', null, '1', '1500', '1500', 'newbalance'), ('93', '1447092933', null, '1', '1600', '1600', 'newbalance'), ('94', '1447092957', '1491372178', '1', 'Air Foamposite', 'airfoamposite', 'nike'), ('95', '1447092969', null, '1', 'Air Force', 'airforce', 'nike'), ('96', '1447092981', null, '1', 'Air Huarache ', 'airhuarache', 'nike'), ('97', '1447092991', null, '1', 'Air Jordan ', 'airjordan', 'nike'), ('98', '1462882106', '1462882111', '1', 'Puma Creepers', 'pumacreepers', 'puma'), ('99', '1464615947', '1483518838', '1', 'одеяло', 'odejalo', 'lancome');
COMMIT;

-- ----------------------------
-- Table structure for `seo_links`
-- ----------------------------
DROP TABLE IF EXISTS `seo_links`;
CREATE TABLE `seo_links` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`status`  tinyint(1) NULL DEFAULT NULL ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`link`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`h1`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`keywords`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`text`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
PRIMARY KEY (`id`),
INDEX `link` (`link`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of seo_links
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `seo_redirects`
-- ----------------------------
DROP TABLE IF EXISTS `seo_redirects`;
CREATE TABLE `seo_redirects` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`status`  tinyint(1) NOT NULL DEFAULT 1 ,
`link_from`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`link_to`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`type`  int(4) NOT NULL DEFAULT 300 ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of seo_redirects
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `seo_scripts`
-- ----------------------------
DROP TABLE IF EXISTS `seo_scripts`;
CREATE TABLE `seo_scripts` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`script`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`status`  int(1) NULL DEFAULT 0 ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`place`  varchar(31) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'head' ,
PRIMARY KEY (`id`),
INDEX `place` (`place`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of seo_scripts
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `sitemenuUSE`
-- ----------------------------
DROP TABLE IF EXISTS `sitemenuUSE`;
CREATE TABLE `sitemenuUSE` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`status`  tinyint(1) NOT NULL DEFAULT 0 ,
`sort`  int(10) NOT NULL DEFAULT 0 ,
`name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`url`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=16

;

-- ----------------------------
-- Records of sitemenuUSE
-- ----------------------------
BEGIN;
INSERT INTO `sitemenuUSE` VALUES ('1', '1447091549', '1465597372', '1', '5', 'О компании', '/aboutUs'), ('2', '1447091558', '1465597371', '1', '1', 'Продукция', '/products'), ('3', '1447091606', '1483519302', '1', '2', 'Прайс-лист', '/priceList'), ('4', '1447091617', '1465597372', '1', '3', 'Доставка', '/delivery'), ('5', '1447091626', '1465597372', '1', '4', 'Контакты', '/contacts'), ('6', null, null, '1', '0', 'Главная', '/'), ('13', null, null, '2', '6', 'Карта сайта', '/sitemap'), ('14', null, null, '2', '7', 'Страница ошибки 404', '/404'), ('15', null, null, '2', '8', 'Текстовая страница', '/textPage');
COMMIT;

-- ----------------------------
-- Table structure for `slider1`
-- ----------------------------
DROP TABLE IF EXISTS `slider1`;
CREATE TABLE `slider1` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`status`  tinyint(1) NOT NULL DEFAULT 1 ,
`sort`  int(10) NOT NULL DEFAULT 0 ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`url`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`image`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=6

;

-- ----------------------------
-- Records of slider1
-- ----------------------------
BEGIN;
INSERT INTO `slider1` VALUES ('2', '1462880145', '1465462205', '1', '1', 'Nike Air Huarache', 'http://cms.wezom.ks.ua/nke-air-huarache/p155', 'a029c068df62637619a62e821227e8c8.jpg'), ('3', '1462880202', '1465462205', '1', '0', 'Puma Creepers', 'http://cms.wezom.ks.ua/ruma-creepers-/p156', '99315da388f70cc1fe55b02e19a37c24.jpg'), ('5', '1465464168', '1483519397', '0', '0', 'yut', '', '8f3dd72ccd5bc75929172b97a5d48f8d.jpg');
COMMIT;

-- ----------------------------
-- Table structure for `specifications_types1`
-- ----------------------------
DROP TABLE IF EXISTS `specifications_types1`;
CREATE TABLE `specifications_types1` (
`id`  int(2) NOT NULL AUTO_INCREMENT ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=4

;

-- ----------------------------
-- Records of specifications_types1
-- ----------------------------
BEGIN;
INSERT INTO `specifications_types1` VALUES ('1', 'Цвет'), ('2', 'Обычная'), ('3', 'Мультивыбор');
COMMIT;

-- ----------------------------
-- Table structure for `specifications_values1`
-- ----------------------------
DROP TABLE IF EXISTS `specifications_values1`;
CREATE TABLE `specifications_values1` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`status`  tinyint(1) NOT NULL DEFAULT 0 ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`sort`  int(10) NULL DEFAULT 0 ,
`alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`specification_id`  int(10) NOT NULL ,
`color`  varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`specification_id`) REFERENCES `specifications1` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
UNIQUE INDEX `alias` (`alias`) USING BTREE ,
INDEX `specification_id` (`specification_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=103

;

-- ----------------------------
-- Records of specifications_values1
-- ----------------------------
BEGIN;
INSERT INTO `specifications_values1` VALUES ('61', '1447093079', null, '1', 'Замша', '0', 'zamsha', '19', null), ('62', '1447093084', null, '1', 'Искусственная замша', '0', 'iskusstvennajazamsha', '19', null), ('63', '1447093089', null, '1', 'Искусственная кожа', '0', 'iskusstvennajakozha', '19', null), ('64', '1447093092', null, '1', 'Кожа', '0', 'kozha', '19', null), ('65', '1447093097', null, '1', 'Латекс', '0', 'lateks', '19', null), ('66', '1447093101', null, '1', 'Нейлон', '0', 'nejlon', '19', null), ('67', '1447093107', null, '1', 'Нубук', '0', 'nubuk', '19', null), ('68', '1447093112', null, '1', 'Полиестр', '0', 'poliestr', '19', null), ('69', '1447093116', null, '1', 'Полимер', '0', 'polimer', '19', null), ('70', '1447093120', null, '1', 'Силикон', '0', 'silikon', '19', null), ('71', '1447093125', null, '1', 'Синтетика', '0', 'sintetika', '19', null), ('72', '1447093130', null, '1', 'Текстиль', '0', 'tekstil', '19', null), ('73', '1447093142', null, '1', 'Хлопок', '0', 'hlopok', '19', null), ('74', '1447093164', null, '1', 'Китай', '0', 'kitaj', '20', null), ('75', '1447093169', null, '1', 'Украина', '0', 'ukraina', '20', null), ('76', '1447093177', null, '1', 'США', '0', 'ssha', '20', null), ('77', '1447093182', null, '1', 'Италия', '0', 'italija', '20', null), ('78', '1447093190', null, '1', 'Вьетнам', '0', 'vetnam', '20', null), ('79', '1447093196', null, '1', 'Индонезия', '0', 'indonezija', '20', null), ('80', '1447093205', null, '1', 'Польша', '0', 'polsha', '20', null), ('81', '1447093215', null, '1', 'Лето', '1', 'leto', '21', null), ('82', '1447093220', null, '1', 'Зима', '0', 'zima', '21', null), ('83', '1447093225', null, '1', 'Весна', '2', 'vesna', '21', null), ('84', '1447093230', null, '1', 'Осень', '3', 'osen', '21', null), ('85', '1447093257', null, '1', 'Бежевый', '1', 'beige', '22', '#f0daad'), ('86', '1447093270', null, '1', 'Белый', '0', 'white', '22', '#ffffff'), ('87', '1447093288', null, '1', 'Бирюзовый', '2', 'turquoise', '22', '#4ff58c'), ('88', '1447093303', null, '1', 'Бордовый', '3', 'claret', '22', '#7a0b21'), ('89', '1447093316', null, '1', 'Голубой', '4', 'light_blue', '22', '#00bfff'), ('90', '1447093343', null, '1', 'Желтый', '5', 'yellow', '22', '#f2ff00'), ('91', '1447093355', null, '1', 'Зеленый', '6', 'green', '22', '#0dbd00'), ('92', '1447093370', null, '1', 'Коралловый', '7', 'coral', '22', '#ff6666'), ('93', '1447093388', null, '1', 'Коричневый', '8', 'korichnevyj', '22', '#802400'), ('94', '1447093397', null, '1', 'Красный', '9', 'krasnyj', '22', '#ff0000'), ('95', '1447093413', null, '1', 'Розовый', '10', 'rozovyj', '22', '#ff0077'), ('96', '1447093429', null, '1', 'Серый', '11', 'seryj', '22', '#8c8c8c'), ('97', '1447093438', null, '1', 'Синий', '12', 'sinij', '22', '#4000ff'), ('98', '1447093453', null, '1', 'Фиолетовый', '13', 'fioletovyj', '22', '#9900ff'), ('99', '1447093461', null, '1', 'Черный', '14', 'chernyj', '22', '#000000'), ('100', '1447093527', null, '1', 'Мужской', '0', 'muzhskoj', '23', null), ('101', '1447093531', null, '1', 'Женский', '0', 'zhenskij', '23', null), ('102', '1447093536', null, '1', 'Унисекс', '0', 'uniseks', '23', null);
COMMIT;

-- ----------------------------
-- Table structure for `specifications1`
-- ----------------------------
DROP TABLE IF EXISTS `specifications1`;
CREATE TABLE `specifications1` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`status`  tinyint(1) NOT NULL DEFAULT 0 ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`sort`  int(10) NULL DEFAULT 0 ,
`alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`type_id`  int(2) NOT NULL DEFAULT 1 ,
PRIMARY KEY (`id`),
FOREIGN KEY (`type_id`) REFERENCES `specifications_types1` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
UNIQUE INDEX `alias` (`alias`) USING BTREE ,
INDEX `type_id` (`type_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=26

;

-- ----------------------------
-- Records of specifications1
-- ----------------------------
BEGIN;
INSERT INTO `specifications1` VALUES ('19', '1447093027', '1464684912', '1', 'Материал', '6', 'material', '3'), ('20', '1447093044', null, '1', 'Производство', '5', 'proizvodstvo', '2'), ('21', '1447093054', null, '1', 'Сезон', '3', 'sezon', '3'), ('22', '1447093064', '1447095073', '1', 'Цвет', '2', 'tsvet', '1'), ('23', '1447093519', null, '1', 'Половой признак', '4', 'sex', '2'), ('24', '1463997483', '1464196645', '1', 'Описание', '1', 'opisanie', '2'), ('25', '1465039912', '1483518854', '1', 'новинка', '0', 'novinka', '1');
COMMIT;

-- ----------------------------
-- Table structure for `titlesUSE`
-- ----------------------------
DROP TABLE IF EXISTS `titlesUSE`;
CREATE TABLE `titlesUSE` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`alias`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`text`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`h1`  int(2) NULL DEFAULT NULL ,
`h2`  int(2) NULL DEFAULT NULL ,
`h3`  int(2) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=69

;

-- ----------------------------
-- Records of titlesUSE
-- ----------------------------
BEGIN;
INSERT INTO `titlesUSE` VALUES ('1', null, null, 'firstLineTop', 'Плиты и блоки', '0', '0', '0'), ('2', null, null, 'secondLineTop', 'пенополистирольные', '0', '0', '0'), ('3', null, null, 'thirdLineTop', 'от производителя', '0', '0', '0'), ('4', null, null, 'paragTop', 'Предлагаем листы пенополистирола толщиной от 10 до 500 мм. Максимальный размер листа 2000х1000 мм.', '0', '0', '0'), ('5', null, null, 'sliderTop', 'Подробнее', '0', '0', '0'), ('6', null, null, 'firstLineSect2', 'Полимер Славутич – Ваш надежный партнер', '0', '0', '0'), ('7', null, null, 'secondLineSect2', 'Наша компания производит плиты пенополистирольные типа ПСБ-С-15, ПСБ-С-25, ПСБ-С-35 и ПСБ-С-50 согласно ДСТУ Б В.2.7-8-94', '0', '0', '0'), ('8', null, null, 'paragSect2', 'Оборудование по раскрою блоков позволяет выпускать листы толщиной от 10 до 500 мм. Максимальный размер листа 2000х1000 мм.', '0', '0', '0'), ('9', null, null, 'paragSect2', 'Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.', '0', '0', '0'), ('10', null, null, 'paragSect2', 'Готовы рассмотреть и выполнить в кратчайшие сроки любые Ваши заявки. Для этого Вам необходимо всего лишь заполнить бланк заказа или связаться с нами по контактным телефонам.', '0', '0', '0'), ('11', null, null, 'firstLineSect3', 'Ассортимент нашей продукции', '0', '0', '0'), ('12', null, null, 'secondLineSect3', 'Сертификаты можно посмотреть ниже. Плиты пенополистирольные типа ПСБ-С-15, ПСБ-С-25, ПСБ-С-35 и ПСБ-С-50', '0', '0', '0'), ('13', null, null, 'imgTitleSect3', 'Плиты пенополистирольные', '0', '0', '0'), ('14', null, null, 'titleSect4', 'Наши преимущества', null, null, null), ('15', null, null, 'smallTitle1Sect4', 'Низкая цена', null, null, null), ('16', null, null, 'parag1Sect4', 'При сотрудничестве с производителем вы определенно будете иметь преимущества в цене. Потому что отсутствует наценка посредника.', null, null, null), ('17', null, null, 'smallTitle2Sect4', 'Высокое качество и профессионализм', null, null, null), ('18', null, null, 'parag2Sect4', 'Лучшие в своей сфере специалисты, оборудование и инструменты. Вся наши продукция полностью сертифицирована.', null, null, null), ('19', null, null, 'smallTitle3Sect4', 'Быстрая доставка', null, null, null), ('20', null, null, 'parag3Sect4', 'В зависимости от объемов заказа сроки на его изготовление занимают 1-5 дней. Скорость доставки для вас позволяет обеспечивать собственный автотранспорт.', null, null, null), ('21', null, null, 'titleSect5', 'Вся продукция сертифицирована', null, null, null), ('22', null, null, 'descSect5', 'Деятельность Компании осуществляется на основании следующих разрешительных документов', null, null, null), ('23', null, null, 'sert1Sect5', 'Ліцензія Державної архітектурно-будівельної інспекції України', null, null, null), ('24', null, null, 'sert2Sect5', 'Ліцензія Державної інспекції ядерного регулювання України', null, null, null), ('25', null, null, 'sert3Sect5', 'Ліцензія Державної служби України з надзвичайних ситуацій', null, null, null), ('26', null, null, 'sert4Sect5', 'Ліцензія Державної служби України з надзвичайних ситуацій', null, null, null), ('27', null, null, 'sert5Sect5', 'іцензія Державної служби України з надзвичайних ситуацій', null, null, null), ('28', null, null, 'titleSect6', 'Оставить заявку', null, null, null), ('29', null, null, 'descSect6', 'Мы готовы рассмотреть и выполнить в кратчайшие сроки любые Ваши заявки. Оставьте заявку, мы перезвоним и всё с вами обсудим!', null, null, null), ('30', null, null, 'footerText1', 'Плиты и блоки пенополистирольные от производителя', null, null, null), ('31', null, null, 'footerAdress', 'Украина, Киевская область, Славутич, 07100, Черниговский квартал, д. 35', null, null, null), ('32', null, null, 'footerTel1', '+38(045)792-97-00', null, null, null), ('33', null, null, 'footerTel2', '+38(050)552-88-02', null, null, null), ('34', null, null, 'footerCopyright', 'Copyright © 2017. Полимер Славутич, ООО', null, null, null), ('35', null, null, 'titleLine1Sect8', 'Доставка', null, null, null), ('36', null, null, 'titleLine2Sect8', 'пенополистирольных плит и блоков', null, null, null), ('37', null, null, 'paragSect8', 'Доставка осуществляется на следующий день, 7 дней в неделю. Скорость доставки обеспечивает собственный автотранспорт.', null, null, null), ('38', null, null, 'titleSect12', 'КАЧЕСТВО ВАШЕГО ЗАКАЗА В БЕЗОПАСНОСТИ!', null, null, null), ('39', null, null, 'descSect12', 'Осуществляем доставку по всей территории Украины в кратчайшие сроки.', null, null, null), ('40', null, null, 'paragSect12', 'Оборудование по раскрою блоков позволяет выпускать листы толщиной от 10 до 500 мм. Максимальный размер листа 2000х1000 мм.', null, null, null), ('41', null, null, 'paragSect12', 'Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.', null, null, null), ('42', null, null, 'strongSect12', 'Мы обеспечиваем надежную доставку вашего заказа:', null, null, null), ('43', null, null, 'liSect12', 'листы упаковываются в пачки и заворачиваются в плёнку;', null, null, null), ('44', null, null, 'liSect12', 'укладываются на деревянные поддоны, либо стягиваются брусками;', null, null, null), ('45', null, null, 'liSect12', 'дополнительно защищаем упаковочной пленкой;', null, null, null), ('46', null, null, 'liSect12', 'чтобы избежать соскальзывания паллет с кузова, наши такелажники укрепляют их с помощью стяжных ремней и накрывают полиэтиленом или брезентом;', null, null, null), ('47', null, null, 'liSect12', 'водители соблюдают скоростной режим и объезжают неровности.', null, null, null), ('48', null, null, 'titleSect9', 'Полимер Славутич – Ваш надежный партнер', null, null, null), ('49', null, null, 'descSect9', 'Наша компания производит плиты пенополистирольные типа ПСБ-С-15, ПСБ-С-25, ПСБ-С-35 и ПСБ-С-50 согласно ДСТУ Б В.2.7-8-94', null, null, null), ('50', null, null, 'paragSect9', 'Оборудование по раскрою блоков позволяет выпускать листы толщиной от 10 до 500 мм. Максимальный размер листа 2000х1000 мм.', null, null, null), ('51', null, null, 'paragSect9', 'Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.', null, null, null), ('52', null, null, 'paragSect9', 'Готовы рассмотреть и выполнить в кратчайшие сроки любые Ваши заявки. Для этого Вам необходимо всего лишь заполнить бланк заказа или связаться с нами по контактным телефонам.', null, null, null), ('53', null, null, 'titleSect13', 'Контакты', null, null, null), ('54', null, null, 'descSect13', 'Перед заказом вы можете приехать, проверить качество продукции и наличие сертификатов', null, null, null), ('55', null, null, 'adress1Sect14', '07100, Киевская область, г. Славутич, Киевский квартал, д.6, кв.14', null, null, null), ('56', null, null, 'mainTelSect14', '+38 (04479) 2-69-99', null, null, null), ('57', null, null, 'mainEmailSect14', 'polimer@slavutich.kiev.ua', null, null, null), ('58', null, null, 'mainSiteSect14', 'http://www.penoplast.maket.net/', null, null, null), ('59', null, null, 'mob1Sect14', '(050) 310-97-17', null, null, null), ('60', null, null, 'email11Sect14', '00069km@ukr.net', null, null, null), ('61', null, null, 'mob2Sect14', '(050) 358-98-02', null, null, null), ('62', null, null, 'rab2Sect14', '(04479) 2-69-99', null, null, null), ('63', null, null, 'email2Sect14', 'polimer@slavutich.kiev.ua', null, null, null), ('64', null, null, 'mob3Sect14', '(050) 552-88-02', null, null, null), ('65', null, null, 'rab3Sect14', '(04479) 2-69-99', null, null, null), ('66', null, null, 'email3Sect14', 'polimer@slavutich.kiev.ua', null, null, null), ('67', null, null, 'priceTitle', 'Прайс-лист', null, null, null), ('68', null, null, 'productsTitle', 'Наша продукция', null, null, null);
COMMIT;

-- ----------------------------
-- Table structure for `users_roles1`
-- ----------------------------
DROP TABLE IF EXISTS `users_roles1`;
CREATE TABLE `users_roles1` (
`id`  int(2) NOT NULL AUTO_INCREMENT ,
`name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`alias`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=8

;

-- ----------------------------
-- Records of users_roles1
-- ----------------------------
BEGIN;
INSERT INTO `users_roles1` VALUES ('1', 'Пользователь', 'Обычный пользователь, зарегистрировавшийся на сайте', 'user', null, null), ('3', 'Разработчик', 'Полный доступ ко всему + к тому к чему нет доступа у главного администратора', 'developer', null, null), ('4', 'Суперадмин', 'Доступ ко всем разделам, кроме тех, к которым имеет доступ только разработчик', 'superadmin', null, null), ('5', '1', '1', 'admin', '1447421386', null), ('6', 'rol1', 'запрещено все', 'admin', '1447662341', null), ('7', 'все разрешено', '', 'admin', '1460728748', '1483520750');
COMMIT;

-- ----------------------------
-- Table structure for `users1`
-- ----------------------------
DROP TABLE IF EXISTS `users1`;
CREATE TABLE `users1` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`name`  varchar(64) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL DEFAULT NULL ,
`login`  varchar(128) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL DEFAULT NULL ,
`password`  varchar(128) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL DEFAULT NULL ,
`created_at`  int(10) NULL DEFAULT NULL ,
`updated_at`  int(10) NULL DEFAULT NULL ,
`hash`  varchar(255) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL DEFAULT NULL ,
`email`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`status`  tinyint(1) NOT NULL DEFAULT 0 ,
`role_id`  int(2) NOT NULL DEFAULT 1 ,
`ip`  varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`phone`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`last_login`  int(10) NULL DEFAULT NULL ,
`logins`  int(10) NULL DEFAULT 0 ,
`last_name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`middle_name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
FOREIGN KEY (`role_id`) REFERENCES `users_roles1` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
UNIQUE INDEX `login` (`login`) USING BTREE ,
UNIQUE INDEX `hash` (`hash`) USING BTREE ,
INDEX `role_id` (`role_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=46

;

-- ----------------------------
-- Records of users1
-- ----------------------------
BEGIN;
INSERT INTO `users1` VALUES ('1', 'Администратор', 'admin', 'c2bcb46de4d99a0ce346ee0a6530b85bb6f0fb80656406a23d3e60c1158a22e8', '1418300546', '1430939378', '48e00180ccc77b94d73ec413b015a4cfb9aa58ba10d8c1b63aad5c8317847d9f', 'palenaya.v.wezom111@gmail.com', '1', '4', null, '+38 (111) 111-11-11', null, '0', null, null), ('2', 'weZom', 'wezom', '4958070fab7cebd8b1000c6c8cb1bca4aa23b509ee9bc4b70570b5c0e3dfe64a', null, '1435164507', 'c2bcb46de4d99a0ce346ee0a6530b85bb6f0fb80656406a23d3e60c1158a22e8', 'vitaliy.demyjkane3nko.1991@gmail.com', '1', '3', null, '+38 (567) 567-56-75', null, '0', null, null), ('21', 'Виталя', null, 'd7957677dc4ec0e9e425c8b99e477aa70244ae512fc2583af6a1ab2ee3b01fbc', '1447096765', '1448350164', 'd6fb647d44ada79149c6732227667ea44b84ab78706cffe94027f960d0bb1124', 'demyanenko.v.wezom@ukr.net', '1', '1', '127.0.0.1', '', '1448350164', '3', 'Демяненко', '444'), ('22', 'Test1', 'admin1', 'c2bcb46de4d99a0ce346ee0a6530b85bb6f0fb80656406a23d3e60c1158a22e8', '1447426756', '1447426756', null, null, '1', '5', null, null, null, '0', null, null), ('23', 'Anton', null, '3e61c90127ae20abbcc8b1b7cfabf301a0269de0460890d5ec33373870c4e172', '1447515858', null, '409fe348ba859b01e942532e108ff6479dd435feaae09d81adc9aa91871a0de0', 'crocoash@gmail.com', '1', '1', '109.86.230.160', null, '1447515857', '1', 'Tsvetkov', null), ('24', 'Vitaliy', null, '339e548c63121ea40d9b8d9a7e6bc43279ba802991a1d2dd54ec8c080328b826', '1448910941', null, '3d63d55d0f1a64f2ad0bd1255b4ecbdbfc0820362d2f1456b64556439fc1e5a8', 'demyanenko.v.wezom@gmail.com', '1', '1', '46.175.163.12', null, '1448910941', '1', 'Demianenko', null), ('25', 'zorya', 'zorya', '131cf002c1c268432c00b026a5182ada3c14a3cbd3ec2f112ea2aff21f4e621e', '1450795155', '1483520734', null, null, '1', '7', null, null, null, '0', null, null), ('26', 'test', null, '99bbed544406cf05b82dd1e9f2affaebaf189fca8c2cfef0ee3ffa2f5250611f', '1461673130', null, '73ffbeef661a2707b9bea04b39979a2a99655a8ae4eed79a35869f20bdec40a3', 'test.wezom@mail.ru', '0', '1', null, '', null, '0', '', ''), ('27', 'Екатерина', null, 'ce7500fb2f28f5d742c9821a1bd7847e8375065070d0e137c401cd151f8b7564', '1464541221', null, 'd9201dfdc8a7585db6d597b570cbaddf8e8288408e5713f887baff81ec9daadf', 'katia-katerinka_@mail.ru', '1', '1', '83.142.111.151', null, '1464541221', '1', 'Баськова', null), ('28', 'test', null, '99bbed544406cf05b82dd1e9f2affaebaf189fca8c2cfef0ee3ffa2f5250611f', '1465467766', '1483520720', '398518fd32869473b46bb3f54c02cb85e86421138514e01b9e85a23b79edc9e9', 'test1@mail.ru', '0', '1', null, '', null, '0', '', ''), ('29', 'test', 'test', 'test', null, null, null, 'test@test.com', '1', '1', null, null, null, '0', null, null), ('37', 'Bolgar', 'Bolgar', '2670e8c6ad98236b5e080a1fa4f0b9d3bcc73fa107aa88804623aca22dc3ac98', '1507797833', null, null, null, '0', '1', null, null, null, '0', null, null), ('38', 'Maxim', 'Maxim', 'a3a1b47310817fd16c44981f1298aba2c64c2da6807ec1a6ea35a37efce0d368', '1507797890', null, null, null, '0', '1', null, null, null, '0', null, null), ('39', 'fooo', 'fooo', 'c4b4fbb2e84f0bf7bf379c939bd6473f81ed702f51e311dd04d690bc43313ec5', '1507798039', null, null, null, '0', '1', null, null, null, '0', null, null), ('40', 'bol', 'bol', 'ad08d18514a4a8540253f96d56fddeb7ca90cc96daaa0c759dc2baf0e57f038b', '1507798526', null, null, null, '0', '1', null, null, null, '0', null, null), ('42', 'MBolgar', 'MBolgar', '071c196b620f2977fa5fbb0e57ba60df771c91a85ddb8badb5319b108b3eae25', '1507800491', null, null, 'MBolgar@MBolgar.MBolgar', '0', '1', null, null, null, '0', null, null), ('43', 'Тест Пользователь', 'utest', '13ae13b7ee0b63321430a285d290bb73db5fa5cd273b63d646e1314a8b00ee1d', '1507807270', null, null, 'utest@utest.utest', '0', '1', null, null, null, '0', null, null), ('44', 'adminP', 'adminP', '7265b830432b8c93044bad3751a2ea4f4b165ea17ee7c83c0b6349261840371d', '1507808319', null, null, 'adminP@adminP.adminP', '0', '1', null, null, null, '0', null, null), ('45', 'newUser', 'newUser', '5ca81ca4b235b8165bbc7d31be8813439cf8311d78f3492d73a9b1a9ed5e20e2', '1507811264', null, null, 'newUser@newUser.newUser', '0', '1', null, '111313', null, '0', null, null);
COMMIT;

-- ----------------------------
-- Auto increment value for `blog_rubrics1`
-- ----------------------------
ALTER TABLE `blog_rubrics1` AUTO_INCREMENT=13;

-- ----------------------------
-- Auto increment value for `brands1`
-- ----------------------------
ALTER TABLE `brands1` AUTO_INCREMENT=47;

-- ----------------------------
-- Auto increment value for `carts`
-- ----------------------------
ALTER TABLE `carts` AUTO_INCREMENT=22;

-- ----------------------------
-- Auto increment value for `carts_items`
-- ----------------------------
ALTER TABLE `carts_items` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `catalog`
-- ----------------------------
ALTER TABLE `catalog` AUTO_INCREMENT=164;

-- ----------------------------
-- Auto increment value for `catalog_images1`
-- ----------------------------
ALTER TABLE `catalog_images1` AUTO_INCREMENT=1099;

-- ----------------------------
-- Auto increment value for `catalog_tree1`
-- ----------------------------
ALTER TABLE `catalog_tree1` AUTO_INCREMENT=55;

-- ----------------------------
-- Auto increment value for `config`
-- ----------------------------
ALTER TABLE `config` AUTO_INCREMENT=42;

-- ----------------------------
-- Auto increment value for `config_groups`
-- ----------------------------
ALTER TABLE `config_groups` AUTO_INCREMENT=9;

-- ----------------------------
-- Auto increment value for `config_types1`
-- ----------------------------
ALTER TABLE `config_types1` AUTO_INCREMENT=7;

-- ----------------------------
-- Auto increment value for `contactsInfoUSE`
-- ----------------------------
ALTER TABLE `contactsInfoUSE` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for `content`
-- ----------------------------
ALTER TABLE `content` AUTO_INCREMENT=31;

-- ----------------------------
-- Auto increment value for `controlUSE`
-- ----------------------------
ALTER TABLE `controlUSE` AUTO_INCREMENT=27;

-- ----------------------------
-- Auto increment value for `cron`
-- ----------------------------
ALTER TABLE `cron` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `i18n`
-- ----------------------------
ALTER TABLE `i18n` AUTO_INCREMENT=2;

-- ----------------------------
-- Auto increment value for `instructionsUSE`
-- ----------------------------
ALTER TABLE `instructionsUSE` AUTO_INCREMENT=7;

-- ----------------------------
-- Auto increment value for `models1`
-- ----------------------------
ALTER TABLE `models1` AUTO_INCREMENT=100;

-- ----------------------------
-- Auto increment value for `seo_links`
-- ----------------------------
ALTER TABLE `seo_links` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `seo_redirects`
-- ----------------------------
ALTER TABLE `seo_redirects` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `seo_scripts`
-- ----------------------------
ALTER TABLE `seo_scripts` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `sitemenuUSE`
-- ----------------------------
ALTER TABLE `sitemenuUSE` AUTO_INCREMENT=16;

-- ----------------------------
-- Auto increment value for `slider1`
-- ----------------------------
ALTER TABLE `slider1` AUTO_INCREMENT=6;

-- ----------------------------
-- Auto increment value for `specifications_types1`
-- ----------------------------
ALTER TABLE `specifications_types1` AUTO_INCREMENT=4;

-- ----------------------------
-- Auto increment value for `specifications_values1`
-- ----------------------------
ALTER TABLE `specifications_values1` AUTO_INCREMENT=103;

-- ----------------------------
-- Auto increment value for `specifications1`
-- ----------------------------
ALTER TABLE `specifications1` AUTO_INCREMENT=26;

-- ----------------------------
-- Auto increment value for `titlesUSE`
-- ----------------------------
ALTER TABLE `titlesUSE` AUTO_INCREMENT=69;

-- ----------------------------
-- Auto increment value for `users_roles1`
-- ----------------------------
ALTER TABLE `users_roles1` AUTO_INCREMENT=8;

-- ----------------------------
-- Auto increment value for `users1`
-- ----------------------------
ALTER TABLE `users1` AUTO_INCREMENT=46;
