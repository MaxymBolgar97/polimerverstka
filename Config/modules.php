<?php

return [
    'frontend' => [
        'Index', 'Delivery', 'AboutUs', 'Contacts', 'PriceList', 'Products', 'Sitemap', 'TextPage', 'LogInForm', 'Login',
         'RegistF', 'Reg', 'Admin', 'Content',
    ],
    'backend' => [
        'Index', 'Contacts', 'Delivery', 'AboutUs', 'PriceList', 'Products', 'Sitemap', 'TextPage', 'LogInForm', 'Login',
         'RegistF', 'Reg', 'Admin',  'Content', 
    ],
];