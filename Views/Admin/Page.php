<?php
use Core\HTML; 
use Core\Widgets;
?>
<style>
    a:hover {
        text-decoration: underline;
    }
    input {
        border: 1px #000 solid;
        margin: 5px;
    }
</style>
<section>
    <h2>Все пользователи</h2>
    <ul>
        <?php foreach ($users as $user): ?>
        <li>
            <a href="/admin/<?php echo $user->id?>"><?php echo $user->name?></a>
        </li>
        <?php endforeach;?>
    </ul>
    <?php echo Widgets::get('Scripts'); ?>
    
    <h3>Добавить нового пользователя</h3>
    <form action="admin/add" method="POST">
        <input type="text" name="name" placeholder="name"/>
        <input type="text" name="login" placeholder="login"/>
        <input type="email" name="email" placeholder="email"/>
        <input type="password" name="password" placeholder="password"/>
        <input type="text" name="phone" placeholder="phone"/>
        <input type="submit" name="send" value="Добавить"/>
    </form>
</section>