<?php

use Core\Widgets; ?>
<!DOCTYPE html>
<html lang="ru" dir="ltr" class="no-js">
    <head>
        <?php echo Widgets::get('Head', $_seo); ?>
        <?php echo $GLOBAL_MESSAGE; ?>
    </head>
    <body class="page-index">
        <main>
            <?php echo Widgets::get('Header'); ?>
            <section>
                <?php echo Widgets::get('Index_Section1'); ?>
                <?php echo Widgets::get('Index_Section2'); ?>
                <?php echo Widgets::get('Index_Section3'); ?>
                <?php echo Widgets::get('Index_Section4'); ?>
                <?php echo Widgets::get('Index_Section5'); ?>
                <?php echo Widgets::get('Index_Section6'); ?>
                <?php echo Widgets::get('Index_Section7'); ?>
            </section>
            <?php echo Widgets::get('Footer'); ?>
        </main>
        <?php echo Widgets::get('Scripts'); ?>
    </body>
</html>
