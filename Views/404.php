<?php

use Core\Widgets; ?>
<!DOCTYPE html>
<html lang="ru" dir="ltr" class="no-js">

    <head>
        <?php echo Widgets::get('Head', $_seo); ?>

    </head>

    <body class="page-404">
        <main>
            <?php echo Widgets::get('Header', ['config' => $_config]); ?>

            <section class="error-page">
                <table class="error-view">
                    <tr>
                        <td>
                            <div class="error-view__block">
                                <div class="error-view__code">404</div>
                                <div class="error-view__msg">
                                    <p>
                                        <strong>Страница не найдена.</strong>
                                        <br>
                                        <small>
                                            <em>К сожалению, страница, которую Вы запросили, не была найдена</em>.</small>
                                    </p>
                                    <p>Возможные причины такой "ошибки":</p>
                                    <ul style="opacity: .8;">
                                        <li>
                                            <small>Неверный URL-адресс страницы - проверьте его на наличие ошибок</small>
                                        </li>
                                        <li>
                                            <small>Страница не существует</small>
                                        </li>
                                        <li>
                                            <small>Страница временно недоступна или удалена</small>
                                        </li>
                                    </ul>
                                    <p>Вы можете перейти на
                                        <a href="index.html">Главную страницу</a> или ознакомиться с нашей
                                        <a href="index.html">продукцией</a>.</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </section>
            <?php echo Widgets::get('Footer'); ?>
        </main>

        <?php echo Widgets::get('Scripts'); ?>

    </body>

</html>
