<?php

use Core\HTML;
?>
<script>
    var wConfig = {
        svgSprite: '<?php echo HTML::media('svg/sprite.svg')?>',
        cart: {
            actions: {
                list: {
                    update: {
                        url: '<?php echo HTML::media('hidden/cart-update.php')?>'
                    },
                    change: {
                        url: '<?php echo HTML::media('hidden/cart-change.php')?>'
                    },
                    add: {
                        url: '<?php echo HTML::media('hidden/cart-add.php')?>'
                    },
                    remove: {
                        url: '<?php echo HTML::media('hidden/cart-remove.php')?>'
                    },
                }
            },
            templates: {
                list: {
                    window: {
                        url: '<?php echo HTML::media('templates/cart-window.ejs')?>'
                    },
                    popup: {
                        url: '<?php echo HTML::media('templates/cart-popup.ejs')?>'
                    },
                    order: {
                        url: '<?php echo HTML::media('templates/cart-order.ejs')?>'
                    }
                }
            }
        }
    };
</script>
<script src="<?php echo HTML::media('js/programmer/translate-ru.js')?>"></script>
<script src="<?php echo HTML::media('js/modernizr.js')?>"></script>
<script src="<?php echo HTML::media('js/jquery.js')?>"></script>
<script src="<?php echo HTML::media('js/jquery-plugins.js')?>"></script>
<script src="<?php echo HTML::media('js/init.js')?>"></script>
<script src="<?php echo HTML::media('js/common.js')?>"></script>
<script src="<?php echo HTML::media('js/programmer/ajax.js')?>"></script>
<script src="<?php echo HTML::media('js/programmer/form-validation.js')?>" defer></script>
<script src="<?php echo HTML::media('js/jquery-1.11.1.min.js')?>"></script>

<!-- @TODO @all Изменить путь к изображению на относительный от корня сайта, в переменной `$wzmOld_URL_IMG` -->
<script>var $wzmOld_URL_IMG = '<?php echo HTML::media('pic/wezom-info-red.gif')?>';</script>
<script async="async" defer="defer" src="<?php echo HTML::media('js/wold.js')?>"></script>

<!-- noscript-msg -->
<noscript>
<link rel="stylesheet" href="<?php echo HTML::media('css/noscript-msg.css')?>">
<input id="noscript-msg__close" type="checkbox" title="Закрити">
<div id="noscript-msg">
    <label id="noscript-msg__times" for="noscript-msg__close" title="Закрити">&times;</label>
    <a href="http://wezom.com.ua/" target="_blank" title="Cтудія Wezom" id="noscript-msg__link">&nbsp;</a>
    <div id="noscript-msg__block">
        <div id="noscript-msg__text">
            <p>В Вашем браузере отключен
                <strong>JavaScript</strong>! Для корректной работы с сайтом необходима поддержка Javascript.</p>
            <p>Мы рекомендуем Вам включить использование JavaScript в настройках вашего браузера.</p>
        </div>
    </div>
</div>
</noscript>
