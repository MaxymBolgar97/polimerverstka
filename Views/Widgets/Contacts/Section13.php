<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<div class="section13 section13--background js-section">
    <div class="section13__block">
        <div class="grid grid--col grid--nowrap grid--jcenter iv-15 iv-md10 iv-ms5 js-section-block">
            <div class="cell cell--24">
                <div class="section13__title revealator-slideright revealator-delay1 revealator-once">
                    <span><?php echo $titleSect13->text ?></span>
                </div>
            </div>
            <div class="cell cell--24">
                <div class="section13__text revealator-slideleft revealator-delay2 revealator-once">
                    <span><?php echo $descSect13->text ?></span>
                </div>
            </div>
            <div class="cell cell--sm24">
                <div class="section13__btn revealator-slideright revealator-delay4 revealator-once">
                    <div class="btn btn--big js-link" data-link=".section14">
                        <span><?php echo $write->text ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section13__right">
        <div class="section13__right-inner">
            <div class="hide js-map-script" data-key="AIzaSyDL6xIhFeOJeE9nXsObhPKfD1wRV4xFknE" data-language="ru" data-marker="<?php HTML::media('pic/map-marker.png')?>"></div>
            <div class="section13__right-map js-map" data-lat="51.5155164" data-lng="30.7577667" data-address="ПОЛИМЕР-СЛАВУТИЧ, ООО<br>Адрес: Киевская обл., г. Славутич, квар. Черниговский, 35"><p>Тут карта, но она почему-то не грузится</p></div>
            <div class="section13__right-triangle">
                <img src="<?php HTML::media('pic/icon-triangle-right-small.png')?>" alt="">
            </div>
        </div>
    </div>
    <div class="section13__left">
        <div class="section13__left-inner">
            <div class="section13__left-svg">
                <svg viewBox="0 0 1220 640">
                <image xlink:href="<?php echo HTML::media('pic/section13-bg-1.jpg') ?>" x="0" y="0" width="1220" height="680" clip-path="url(#icon-triangle-left)"></image>
                <path fill="#2A3343" opacity="0.7" d="M0,0h1220L848,640H0V0z" />
                </svg>
            </div>
        </div>
    </div>
</div>