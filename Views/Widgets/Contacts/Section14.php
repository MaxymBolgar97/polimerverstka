<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<div class="section14 pv-50 pv-lg30">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="grid i-20">
                <div class="cell cell--12 cell--sm24">
                    <div class="grid i-20 i-sm10">
                        <div class="cell cell--24">
                            <div class="section14__title">
                                <span>Контактная информация</span>
                            </div>
                            <div class="section14__line mt-20"></div>
                        </div>
                        <div class="cell cell--12 cell--ms24">
                            <div class="grid i-10">
                                <div class="cell cell--24">
                                    <div class="section14__head">
                                        <span>Юридический адрес:</span>
                                    </div>
                                    <div class="section14__text">
                                        <div>
                                            <p><?php echo $adress1Sect14->text ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell cell--24">
                                    <div class="section14__head">
                                        <span>Контактные телефоны:</span>
                                    </div>
                                    <div class="section14__text">
                                        <div>
                                            <a href="tel:+380447926999" class="section14__tel"><?php echo $mainTelSect14->text ?></a>,
                                            <a href="tel:+380447929700" class="section14__tel">2-97-00</a>
                                        </div>
                                        <div>
                                            <span>E-mail:</span>
                                            <a href="mailto:polimer@slavutich.kiev.ua" class="section14__email"><?php echo $mainEmailSect14->text ?></a>
                                        </div>
                                        <div>
                                            <a href="http://www.penoplast.maket.net/" class="section14__email"><?php echo $mainSiteSect14->text ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--12 cell--ms24">
                            <div class="grid i-10">
                                <div class="cell cell--24">
                                    <div class="section14__head">
                                        <span>Директор - Якусевич Сергей</span>
                                    </div>
                                    <div class="section14__text">
                                        <div>
                                            <span>Мобильный:</span>
                                            <a href="tel:+380503109717" class="section14__tel"><?php echo $mob1Sect14->text ?></a>
                                        </div>
                                        <div>
                                            <span>E-mail:</span>
                                            <a href="mailto:00069km@ukr.net" class="section14__email"><?php echo $email11Sect14->text ?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell cell--24">
                                    <div class="section14__head">
                                        <span>Заместитель директора по продажам - Катамай Анатолий</span>
                                    </div>
                                    <div class="section14__text">
                                        <div>
                                            <span>Мобильный:</span>
                                            <a href="tel:+380503589802" class="section14__tel"><?php echo $mob2Sect14->text ?></a>
                                        </div>
                                        <div>
                                            <span>Рабочий:</span>
                                            <a href="tel:+380447926999" class="section14__tel"><?php echo $rab2Sect14->text ?></a>
                                        </div>
                                        <div>
                                            <span>E-mail:</span>
                                            <a href="mailto:polimer@slavutich.kiev.ua" class="section14__email"><?php echo $email2Sect14->text ?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell cell--24">
                                    <div class="section14__head">
                                        <span>Менеджер по продажам - Радченко Елена</span>
                                    </div>
                                    <div class="section14__text">
                                        <div>
                                            <span>Мобильный:</span>
                                            <a href="tel:+380505528802" class="section14__tel"><?php echo $mob3Sect14->text ?></a>
                                        </div>
                                        <div>
                                            <span>Рабочий:</span>
                                            <a href="tel:+380447926999" class="section14__tel"><?php echo $rab3Sect14->text ?></a>,
                                            <a href="tel:+380447929700" class="section14__tel">2-97-00</a>
                                        </div>
                                        <div>
                                            <span>E-mail:</span>
                                            <a href="mailto:00069km@ukr.net" class="section14__email"><?php echo $email3Sect14->text ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cell cell--12 cell--sm24">
                    <div class="grid i-20 i-sm10">
                        <div class="cell cell--24">
                            <div class="section14__title">
                                <span>Контактная форма</span>
                            </div>
                            <div class="section14__line mt-20"></div>
                        </div>
                        <div class="cell cell--24">
                            <div class="section14__form form js-form" data-form="true" data-errors-by-step="true" data-ajax="<?php HTML::media('hidden/request.php')?>">
                                <div class="grid grid--acenter i-10">
                                    <div class="cell cell--12 cell--ms24">
                                        <div class="grid i-10">
                                            <div class="cell cell--24">
                                                <div class="form__label">Продукты и услуги *</div>
                                                <div class="control-holder control-holder--text">
                                                    <select name="type" class="js-select2" required="">
                                                        <option value="">Выбрать</option>
                                                        <option value="1">Продукты</option>
                                                        <option value="2">Услуги</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="cell cell--24">
                                                <div class="form__label">Ваше имя *</div>
                                                <div class="control-holder control-holder--text">
                                                    <input type="text" name="name" data-name="name" required="" data-rule-minlength="2" data-rule-word="true">
                                                </div>
                                            </div>
                                            <div class="cell cell--24">
                                                <div class="form__label">Номер телефона *</div>
                                                <div class="control-holder control-holder--text">
                                                    <input type="tel" name="tel" data-name="tel" required="" data-rule-phone="true" class="js-inputmask" data-mask="+38(099)999-99-99">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cell cell--12 cell--ms24">
                                        <div class="form__label">Сообщение *</div>
                                        <div class="control-holder control-holder--text">
                                            <textarea name="message" data-name="message" required="" data-rule-minlength="10"></textarea>
                                        </div>
                                    </div>
                                    <div class="cell cell--14 cell--lg12 cell--ms24">
                                        <div class="control-holder control-holder--flag">
                                            <label>
                                                <input type="checkbox" name="agree" data-name="agree" value="1" required>
                                                <ins>
                                                    <svg>
                                                    <use xlink:href="<?php HTML::media('svg/sprite.svg#icon-check')?>" />
                                                    </svg>
                                                </ins>
                                                <span>Я согласен с
                                                    <a href="text.html" class="form__link" target="_blank">политикой конфиденциальности</a>
                                                </span>
                                            </label>
                                            <div>
                                                <label for="agree" class="has-error" style="display: none;"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cell cell--10 cell--lg12 cell--ms24">
                                        <div class="btn btn--full btn--big js-form-submit">
                                            <span>Отправить</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>