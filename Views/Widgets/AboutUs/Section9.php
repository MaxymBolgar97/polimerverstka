<?php

use \Core\HTML;
use \Core\Config;
use \Core\Widgets;
?>
<div class="section9 pv-50 pv-ms30">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="grid i-10 js-mfp-container">
                <div class="cell cell--24">
                    <div class="section9__title revealator-slideright revealator-delay1 revealator-once">
                        <span><?php echo $titleSect9->text ?></span>
                    </div>
                </div>
                <div class="cell cell--24">
                    <div class="section9__text revealator-slideleft revealator-delay2 revealator-once">
                        <span><?php echo $descSect9->text ?></span>
                    </div>
                </div>
                <div class="cell cell--24">
                    <div class="section9__desc view-text mv-30 mv-lg0">
                        <?php foreach ($paragSect9 as $p): ?>
                        <p><?php echo $p->text ?></p>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="cell cell--8 cell--md12 cell--sm24">
                    <div class="section9__item revealator-zoomin revealator-delay1 revealator-once">
                        <!-- @TODO Изображение должно быть самое большое, которое есть -->
                        <div class="section9__image js-mfp-image" data-url="<?php echo HTML::media('images/section9-gallery-1.jpg') ?>">
                            <div class="section9__image-inner">
                                <!-- @TODO Изображение должно быть таких размеров: 650х427 -->
                                <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section9-gallery-1.jpg') ?>" alt=""> </div>
                        </div>
                    </div>
                </div>
                <div class="cell cell--8 cell--md12 cell--sm24">
                    <div class="section9__item revealator-zoomin revealator-delay1 revealator-once">
                        <!-- @TODO Изображение должно быть самое большое, которое есть -->
                        <div class="section9__image js-mfp-image" data-url="<?php echo HTML::media('images/section9-gallery-2.jpg') ?>">
                            <div class="section9__image-inner">
                                <!-- @TODO Изображение должно быть таких размеров: 650х427 -->
                                <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section9-gallery-2.jpg') ?>" alt=""> </div>
                        </div>
                    </div>
                </div>
                <div class="cell cell--8 cell--md12 cell--sm24">
                    <div class="section9__item revealator-zoomin revealator-delay1 revealator-once">
                        <!-- @TODO Изображение должно быть самое большое, которое есть -->
                        <div class="section9__image js-mfp-image" data-url="<?php echo HTML::media('images/section9-gallery-3.jpg') ?>">
                            <div class="section9__image-inner">
                                <!-- @TODO Изображение должно быть таких размеров: 650х427 -->
                                <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section9-gallery-3.jpg') ?>" alt=""> </div>
                        </div>
                    </div>
                </div>
                <div class="cell cell--8 cell--md12 cell--sm24">
                    <div class="section9__item revealator-zoomin revealator-delay1 revealator-once">
                        <!-- @TODO Изображение должно быть самое большое, которое есть -->
                        <div class="section9__image js-mfp-image" data-url="<?php echo HTML::media('images/section9-gallery-4.jpg') ?>">
                            <div class="section9__image-inner">
                                <!-- @TODO Изображение должно быть таких размеров: 650х427 -->
                                <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section9-gallery-4.jpg') ?>" alt=""> </div>
                        </div>
                    </div>
                </div>
                <div class="cell cell--8 cell--md12 cell--sm24">
                    <div class="section9__item revealator-zoomin revealator-delay1 revealator-once">
                        <!-- @TODO Изображение должно быть самое большое, которое есть -->
                        <div class="section9__image js-mfp-image" data-url="<?php echo HTML::media('images/section9-gallery-5.jpg') ?>">
                            <div class="section9__image-inner">
                                <!-- @TODO Изображение должно быть таких размеров: 650х427 -->
                                <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section9-gallery-5.jpg') ?>" alt=""> </div>
                        </div>
                    </div>
                </div>
                <div class="cell cell--8 cell--md12 cell--sm24">
                    <div class="section9__item revealator-zoomin revealator-delay1 revealator-once">
                        <!-- @TODO Изображение должно быть самое большое, которое есть -->
                        <div class="section9__image js-mfp-image" data-url="<?php echo HTML::media('images/section9-gallery-6.jpg') ?>">
                            <div class="section9__image-inner">
                                <!-- @TODO Изображение должно быть таких размеров: 650х427 -->
                                <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section9-gallery-6.jpg') ?>" alt=""> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>