<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<div class="section12 pv-50 pv-lg30">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="grid i-10">
                <div class="cell cell--24">
                    <div class="section12__title revealator-slideright revealator-delay1 revealator-once">
                        <span><?php echo $titleSect12->text ?></span>
                    </div>
                </div>
                <div class="cell cell--24">
                    <div class="section12__text revealator-slideleft revealator-delay2 revealator-once">
                        <span><?php echo $descSect12->text ?></span>
                    </div>
                </div>
                <div class="cell cell--24">
                    <div class="section12__content view-text mt-30 mt-lg0">
                        <?php foreach ($paragSect12 as $item): ?>
                        <p><?php echo $item->text ?></p>
                        <?php endforeach; ?>
                        <p>
                            <strong><?php echo $strongSect12->text ?></strong>
                            <img style="float: right; margin: 30px;" src="<?php echo HTML::media('images/section12-1.png') ?>" width="400" height="329">
                        </p>
                        <ul>
                            <?php foreach($liSect12 as $li):?>
                            <li><?php echo $li->text ?></li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>