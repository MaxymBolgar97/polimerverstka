<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>

<header>
    <div class="header-block">
        <div class="grid grid--xl pg-30">
            <div class="cell cell--24">
                <div class="grid grid--jbetween grid--aend grid--msacenter i-10">
                    <div class="cell cell--lg5 cell--sm6 cell--xs7">
                        <div class="header-block__logo pv-10">
                            <a href="/"><img src="<?php echo HTML::media('pic/logo.png') ?>" alt=""></a>
                        </div>
                    </div>
                    <div class="cell cell--lg19 cell--sm18 cell--xs17">
                        <div class="header-block__top pv-10">
                            <div class="grid grid--acenter grid--jend i-10">
                                <div class="cell cell--ms0">
                                    <?php foreach ($phones as $phone): ?>
                                        <a href="tel:<?php echo $phone->text ?>" class="header-block__tel"><?php echo $phone->text ?></a>
                                    <?php endforeach; ?>
                                </div>
                                <div class="cell cell--ms0">
                                    <div class="header-block__text">
                                        <?php foreach ($adresses as $adress): ?>
                                            <span><?php echo $adress->text ?></span>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="cell cell--md8 cell--ms cell--xs18 gcenter">
                                    <div class="btn js-mfp-ajax" data-url="<?php echo HTML::media('hidden/call.php') ?>">
                                        <span><?php echo $callBack->text ?></span>
                                    </div>
                                </div>
                                <div class="cell cell--ms0">
                                    <div class="select js-select">
                                        <div class="select__head">
                                            <span>РУС</span>
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-down') ?>" />
                                            </svg>
                                        </div>
                                        <div class="select__list">
                                            <a href="?lang=uk" class="select__item">
                                                <span>УКР</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell cell--0 cell--ms">
                                    <a href="#mobile-menu" class="menu-link" title="Меню">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-list') ?>" />
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="main-menu pt-10 ms-hide">
                            <div class="grid grid--acenter grid--jend ig-25 ig-lg10">
                                <?php foreach ($contentMenu as $obj): ?>
                                    <div class="cell">
                                        <a href="<?php echo HTML::link($obj->url); ?>" class="main-menu__item ">
                                            <span><?php echo $obj->name; ?></span>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                                <div class="cell">
                                    <?php if (!$user): ?>
                                        <a href="/logInForm" class="main-menu__item" style="color: red;">Вход</a>
                                    <?php else: ?>
                                        <a href="/logout" class="main-menu__item" style="color: red;"><?php echo $user->name ?></a>
                                        <?php if($user->name == 'adminP'):?>
                                        <a href="/admin" class="main-menu__item" style="color: red;">Админ-панель</a>
                                        <?php endif;?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>