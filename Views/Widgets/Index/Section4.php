<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<div class="section4 js-section">
    <div class="section4__bg">
        <img src="<?php echo HTML::media('pic/section4-bg-1.jpg')?>" alt="">
    </div>
    <div class="section4__block">
        <div class="grid grid--lg pg-30">
            <div class="cell cell--24">
                <div class="section4__grid grid grid--acenter iv-10 js-section-block">
                    <div class="cell cell--11 cell--lg12 cell--md14 cell--ms16 cell--sm24">
                        <div class="grid i-15 i-lg10">
                            <div class="cell cell--24">
                                <div class="section4__title revealator-slideleft revealator-delay2 revealator-once">
                                    <span><?php echo $titleSect4->text ?></span>
                                </div>
                            </div>
                            <div class="cell cell--24">
                                <div class="grid grid--nowrap i-15">
                                    <div class="cell cell--nogrow cell--noshrink">
                                        <div class="section4__icon js-drawing-svg">
                                            <svg viewBox="0 0 600 600" width="100%" height="100%">
                                            <path fill="none" stroke-width="10" d="M300,7.6c161.5,0,292.4,130.9,292.4,292.4S461.6,592.5,300,592.5  S7.6,461.5,7.6,300S138.5,7.6,300,7.6z"></path>
                                            <path d="M108,300c0-106,86-192,192-192c26,0,50.9,5.2,73.5,14.6l-18.4,44.4c-73.5-30.4-157.7,4.5-188.1,77.9s4.5,157.7,77.9,188.1  c73.5,30.4,157.7-4.5,188.1-77.9c15.2-36.7,14.1-76.2,0-110.2l44.4-18.4c9.4,22.6,14.6,47.5,14.6,73.5c0,106-86,192-192,192  S108,406,108,300L108,300z M345,240.1l-69,120h-21l69-120H345L345,240.1z M255,276c0,5,4,9,9,9s9-4,9-9v-12c0-5-4-9-9-9s-9,4-9,9  V276L255,276z M240,261c0-11.6,9.4-21,21-21h6c11.6,0,21,9.4,21,21v18c0,11.6-9.4,21-21,21h-6c-11.6,0-21-9.4-21-21V261L240,261z   M327,336c0,5,4,9,9,9s9-4,9-9v-12c0-5-4-9-9-9s-9,4-9,9V336L327,336z M312,321c0-11.6,9.4-21,21-21h6c11.6,0,21,9.4,21,21v18  c0,11.6-9.4,21-21,21h-6c-11.6,0-21-9.4-21-21V321L312,321z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="cell cell--grow">
                                        <div class="section4__item">
                                            <div class="section4__name">
                                                <span><?php echo $smallTitle1Sect4->text ?></span>
                                            </div>
                                            <div class="section4__desc">
                                                <p><?php echo $parag1Sect4->text ?></p>
                                            </div>
                                            <div class="section4__line revealator-slideright revealator-delay2 revealator-once"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--24">
                                <div class="grid grid--nowrap i-15">
                                    <div class="cell cell--nogrow cell--noshrink">
                                        <div class="section4__icon js-drawing-svg">
                                            <svg viewBox="0 0 600 600" width="100%" height="100%">
                                            <path fill="none" stroke-width="10" d="M300,7.6c161.5,0,292.4,130.9,292.4,292.4S461.6,592.5,300,592.5  S7.6,461.5,7.6,300S138.5,7.6,300,7.6z"></path>
                                            <path d="M221.7,262.1h159.9v-15.7H221.7V262.1z M221.7,293.4h69v-15.7h-69V293.4z M221.7,229.5h159.9v-15.7H221.7  V229.5z M381.6,182.5h-160v15.7h159.9L381.6,182.5L381.6,182.5z M272.8,481.3l25.8-13.6l25.8,13.6v-67.2h-32.6L272.8,481.3z   M166.3,127v327.2h97.5l4-14.2h-83.1V144h231.6v296.4l-18.4-0.1l3.9,13.8h31.6l0.2-94.9V127H166.3z M332.8,330  c-14.3,0-26,11.6-26,26c0,14.3,11.6,26,26,26s26-11.7,26-26C358.8,341.7,347.2,330,332.8,330z M343.9,414.1v67.2l25.8-13.6  l25.7,13.6l-18.9-67.2H343.9z M361.2,405.1l3.4-17.4l17.4-3.4l-5.8-16.7l13.4-11.6l-13.4-11.6l5.8-16.7l-17.4-3.4l-3.4-17.4  l-16.7,5.8l-11.6-13.4l-11.6,13.4l-16.7-5.8l-3.4,17.4l-17.4,3.4l5.8,16.7L276.1,356l13.4,11.6l-5.8,16.7l17.4,3.4l3.4,17.4  l16.7-5.8l11.6,13.4l11.6-13.4L361.2,405.1z M297.4,356c0-19.5,15.8-35.4,35.4-35.4c19.5,0,35.4,15.8,35.4,35.4  c0,19.5-15.8,35.4-35.4,35.4C313.3,391.4,297.4,375.5,297.4,356z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="cell cell--grow">
                                        <div class="section4__item">
                                            <div class="section4__name">
                                                <span><?php echo $smallTitle2Sect4->text ?></span>
                                            </div>
                                            <div class="section4__desc">
                                                <p><?php echo $parag2Sect4->text ?></p>
                                            </div>
                                            <div class="section4__line revealator-slideright revealator-delay2 revealator-once"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--24">
                                <div class="grid grid--nowrap i-15">
                                    <div class="cell cell--nogrow cell--noshrink">
                                        <div class="section4__icon js-drawing-svg">
                                            <svg viewBox="0 0 600 600" width="100%" height="100%">
                                            <path fill="none" stroke-width="10" d="M300,7.6c161.5,0,292.4,130.9,292.4,292.4S461.6,592.5,300,592.5  S7.6,461.5,7.6,300S138.5,7.6,300,7.6z"></path>
                                            <path d="M489.1,295.1l-25.8-17.7l-46.9-74c-1.8-2.8-4.9-4.5-8.2-4.5h-2.9H350H191.9v19.4h148.2v66c0,5.4,4.4,9.7,9.7,9.7h103.3  l20.8,14.2v47.5h-9.5c-4.9-18.2-21.5-31.7-41.2-31.7s-36.3,13.5-41.2,31.7h-73.7c-4.9-18.2-21.5-31.7-41.2-31.7  s-36.3,13.5-41.2,31.7h-34v19.4h33.4c3.9,19.5,21.2,34.2,41.8,34.2s37.9-14.7,41.8-34.2h72.4c3.9,19.5,21.2,34.2,41.8,34.2  c20.6,0,37.9-14.7,41.8-34.2h18.6c5.4,0,9.7-4.4,9.7-9.7V303C493.3,299.9,491.7,296.9,489.1,295.1z M359.5,218.3H400l37.9,56.3  h-78.3L359.5,218.3L359.5,218.3z M267.1,389.9c-12.8,0-23.2-10.4-23.2-23.2c0-12.8,10.4-23.2,23.2-23.2c12.8,0,23.2,10.4,23.2,23.2  C290.3,379.5,279.9,389.9,267.1,389.9z M423.2,389.9c-12.8,0-23.2-10.4-23.2-23.2c0-12.8,10.4-23.2,23.2-23.2  c12.8,0,23.2,10.4,23.2,23.2C446.4,379.5,436,389.9,423.2,389.9z"></path>
                                            <rect x="167.3" y="240.6" width="66.9" height="19.4"></rect>
                                            <rect x="122" y="282.1" width="85.3" height="19.4"></rect>
                                            <rect x="143.8" y="316.8" width="49.8" height="19.4"></rect>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="cell cell--grow">
                                        <div class="section4__item">
                                            <div class="section4__name">
                                                <span><?php echo $smallTitle3Sect4->text ?></span>
                                            </div>
                                            <div class="section4__desc">
                                                <p><?php echo $parag3Sect4->text ?></p>
                                            </div>
                                            <div class="section4__line revealator-slideright revealator-delay2 revealator-once"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>