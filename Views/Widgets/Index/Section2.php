<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<div class="section2 pt-30 pb-50 pb-ms30">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="grid i-10">
                <div class="cell cell--24">
                    <div class="section2__title revealator-slideright revealator-delay1 revealator-once">
                        <span><?php echo $firstLineSect2->text ?></span>
                    </div>
                </div>
                <div class="cell cell--24">
                    <div class="section2__text revealator-slideleft revealator-delay2 revealator-once">
                        <span><?php echo $secondLineSect2->text ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section2__block js-inview-div mt-30" data-background="<?php echo HTML::media('pic/section2-bg-1.png')?>">
        <div class="grid grid--lg pg-30">
            <div class="cell cell--24">
                <div class="grid grid--mdjcenter i-10">
                    <div class="cell cell--12 cell--md24">
                        <div class="section2__content mb-50 mb-md20">
                            <div class="section2__quote revealator-slidedown revealator-delay4 revealator-once">
                                <svg>
                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-quote')?>" />
                                </svg>
                            </div>
                            <div class="section2__desc view-text">
                                <?php foreach ($paragSect2 as $item): ?>
                                    <p><?php echo $item->text ?></p>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="section2__btn">
                            <div class="grid grid--smjcenter i-10">
                                <div class="cell">
                                    <div class="btn btn--big revealator-slideright revealator-once js-link" data-link=".section6">
                                        <span><?php echo $btn1Sect2->text ?></span>
                                    </div>
                                </div>
                                <div class="cell">
                                    <a href="about.html" class="btn btn--big btn--light revealator-slideleft revealator-once">
                                        <span><?php echo $btn2Sect2->text ?></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cell cell--12 cell--sm24">
                        <div class="section2__slider js-section2-slider">
                            <div>
                                <div class="section2__image">
                                    <div class="section2__image-inner">
                                        <!-- @TODO Изображение должно быть таких размеров: 595х374 -->
                                        <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section2-slider-1.jpg')?>" alt="">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="section2__image">
                                    <div class="section2__image-inner">
                                        <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section3-gallery-4.jpg')?>" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>