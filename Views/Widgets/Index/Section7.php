<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<div class="section7">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="section7__slider js-section7-slider">
                <div>
                    <div class="section7__item">
                        <a href="/" class="section7__image image image--contain" target="_blank">
                            <!-- @TODO Изображение должно быть таких размеров: 352х70 -->
                            <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section7-slider-1.png')?>" alt="">
                        </a>
                    </div>
                </div>
                <div>
                    <div class="section7__item">
                        <a href="/" class="section7__image image image--contain" target="_blank">
                            <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section7-slider-2.png')?>" alt="">
                        </a>
                    </div>
                </div>
                <div>
                    <div class="section7__item">
                        <a href="/" class="section7__image image image--contain" target="_blank">
                            <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section7-slider-3.png')?>" alt="">
                        </a>
                    </div>
                </div>
                <div>
                    <div class="section7__item">
                        <a href="/" class="section7__image image image--contain" target="_blank">
                            <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section7-slider-4.jpg')?>" alt="">
                        </a>
                    </div>
                </div>
                <div>
                    <div class="section7__item">
                        <a href="/" class="section7__image image image--contain" target="_blank">
                            <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section7-slider-5.jpg')?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>