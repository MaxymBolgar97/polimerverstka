<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<div class="section3 pv-50 pv-ms30">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="grid i-10">
                <div class="cell cell--24">
                    <div class="section3__title revealator-slideright revealator-delay1 revealator-once">
                        <span><?php echo $firstLineSect3->text ?></span>
                    </div>
                </div>
                <div class="cell cell--24">
                    <div class="section3__text revealator-slideleft revealator-delay2 revealator-once">
                        <span><?php echo $secondLineSect3->text ?></span>
                    </div>
                </div>
                <!-- @TODO Изображение должно быть таких размеров: 660х286 -->
                <div class="cell cell--12 cell--sm24">
                    <a href="index.html" class="section3__item revealator-zoomin revealator-delay1 revealator-once">
                        <div class="section3__image section3__image--big">
                            <div class="section3__image-inner">
                                <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section3-gallery-1.jpg')?>" alt=""> </div>
                        </div>
                        <div class="section3__name">
                            <span><?php echo $imgTitleSect3->text ?></span>
                        </div>
                    </a>
                </div>
                <div class="cell cell--12 cell--sm24">
                    <a href="index.html" class="section3__item revealator-zoomin revealator-delay1 revealator-once">
                        <div class="section3__image section3__image--big">
                            <div class="section3__image-inner">
                                <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section3-gallery-2.jpg')?>" alt=""> </div>
                        </div>
                        <div class="section3__name">
                            <span><?php echo $imgTitleSect3->text ?></span>
                        </div>
                    </a>
                </div>
                <div class="cell cell--8 cell--sm24">
                    <a href="index.html" class="section3__item revealator-zoomin revealator-delay1 revealator-once">
                        <div class="section3__image">
                            <div class="section3__image-inner">
                                <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section3-gallery-3.jpg')?>" alt=""> </div>
                        </div>
                        <div class="section3__name">
                            <span><?php echo $imgTitleSect3->text ?></span>
                        </div>
                    </a>
                </div>
                <div class="cell cell--8 cell--sm24">
                    <a href="index.html" class="section3__item revealator-zoomin revealator-delay1 revealator-once">
                        <div class="section3__image">
                            <div class="section3__image-inner">
                                <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section3-gallery-4.jpg')?>" alt=""> </div>
                        </div>
                        <div class="section3__name">
                            <span><?php echo $imgTitleSect3->text ?></span>
                        </div>
                    </a>
                </div>
                <div class="cell cell--8 cell--sm24">
                    <a href="index.html" class="section3__item revealator-zoomin revealator-delay1 revealator-once">
                        <div class="section3__image">
                            <div class="section3__image-inner">
                                <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section3-gallery-5.jpg')?>" alt=""> </div>
                        </div>
                        <div class="section3__name">
                            <span><?php echo $imgTitleSect3->text ?></span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>