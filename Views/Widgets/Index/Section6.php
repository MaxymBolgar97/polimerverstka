<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<div class="section6">
    <div class="section6__block js-inview-div" data-background="<?php echo HTML::media('pic/section6-bg-1.png')?>">
        <div class="grid grid--lg pg-30">
            <div class="cell cell--24">
                <div class="grid grid--acenter i-10 js-section-block">
                    <div class="cell cell--12 cell--sm24">
                        <div class="grid grid--col iv-15 iv-md10">
                            <div class="cell cell--16 cell--sm24">
                                <div class="section6__title revealator-slideright revealator-delay1 revealator-once">
                                    <span><?php echo $titleSect6->text?></span>
                                </div>
                            </div>
                            <div class="cell cell--16 cell--sm24">
                                <div class="section6__text revealator-slideleft revealator-delay2 revealator-once">
                                    <span><?php echo $descSect6->text?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cell cell--12 cell--sm24">
                        <div class="grid grid--jcenter">
                            <div class="cell cell--13 cell--xl14 cell--md16 cell--sm24">
                                <div class="section6__form form js-form" data-errors-by-step="true" data-form="true" data-ajax="<?php echo HTML::media('hidden/request.php')?>">
                                    <div class="grid grid--smjcenter i-10">
                                        <div class="cell cell--24">
                                            <div class="form__label">Продукты и услуги *</div>
                                            <div class="control-holder control-holder--text">
                                                <select name="type" class="js-select2" required="">
                                                    <option value="">Выбрать</option>
                                                    <option value="1">Продукты</option>
                                                    <option value="2">Услуги</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="cell cell--24">
                                            <div class="form__label">Ваше имя *</div>
                                            <div class="control-holder control-holder--text">
                                                <input type="text" name="name" data-name="name" required="" data-rule-minlength="2" data-rule-word="true">
                                            </div>
                                        </div>
                                        <div class="cell cell--24">
                                            <div class="form__label">Номер телефона *</div>
                                            <div class="control-holder control-holder--text">
                                                <input type="tel" name="tel" data-name="tel" required="" data-rule-phone="true" class="js-inputmask" data-mask="+38(099)999-99-99">
                                            </div>
                                        </div>
                                        <div class="cell cell--24">
                                            <div class="control-holder control-holder--flag">
                                                <label>
                                                    <input type="checkbox" name="agree" data-name="agree" value="1" required>
                                                    <ins>
                                                        <svg>
                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-check')?>" />
                                                        </svg>
                                                    </ins>
                                                    <span>Я согласен с
                                                        <a href="/textPage" class="form__link" target="_blank">политикой конфиденциальности</a>
                                                    </span>
                                                </label>
                                                <div>
                                                    <label for="agree" class="has-error" style="display: none;"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cell cell--18 cell--sm24">
                                            <div class="btn btn--full js-form-submit">
                                                <span><?php echo $btn1Sect6->text?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell cell--12">

            </div>
        </div>
    </div>
</div>