<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<div class="section5 pv-50 pv-ms30">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="grid grid--col grid--acenter i-10">
                <div class="cell">
                    <div class="section5__title revealator-slideright revealator-delay1 revealator-once">
                        <span><?php echo $titleSect5->text?></span>
                    </div>
                </div>
                <div class="cell">
                    <div class="section5__line revealator-slideleft revealator-delay2 revealator-once"></div>
                </div>
                <div class="cell">
                    <div class="section5__text revealator-slideright revealator-delay2 revealator-once">
                        <span><?php echo $descSect5->text?></span>
                    </div>
                </div>
                <div class="cell cell--24">
                    <div class="section5__slider js-section5-slider js-mfp-container">
                        <div>
                            <div class="section5__item">
                                <!-- @TODO Изображение должно быть самое большое, которое есть -->
                                <div class="section5__link js-mfp-image" data-url="<?php echo HTML::media('images/section5-certificate-1.jpg')?>">
                                    <div class="section5__image">
                                        <div class="section5__image-inner">
                                            <!-- @TODO Изображение должно быть таких размеров: 195х273 -->
                                            <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section5-certificate-1.jpg')?>" alt="">
                                        </div>
                                    </div>
                                    <div class="section5__name">
                                        <span><?php echo $sert1Sect5->text?></span>
                                    </div>
                                </div>
                                <div class="section5__btn">
                                    <div class="btn">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-download')?>" />
                                        </svg>
                                        <span>Загрузить</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="section5__item">
                                <div class="section5__link js-mfp-image" data-url="<?php echo HTML::media('images/section5-certificate-2.jpg')?>">
                                    <div class="section5__image">
                                        <div class="section5__image-inner">
                                            <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section5-certificate-2.jpg')?>" alt="">
                                        </div>
                                    </div>
                                    <div class="section5__name">
                                        <span><?php echo $sert2Sect5->text?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="section5__item">
                                <div class="section5__link js-mfp-image" data-url="<?php echo HTML::media('images/section5-certificate-3.jpg')?>">
                                    <div class="section5__image">
                                        <div class="section5__image-inner">
                                            <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section5-certificate-3.jpg')?>" alt="">
                                        </div>
                                    </div>
                                    <div class="section5__name">
                                        <span><?php echo $sert3Sect5->text?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="section5__item">
                                <div class="section5__link js-mfp-image" data-url="<?php echo HTML::media('images/section5-certificate-4.jpg')?>">
                                    <div class="section5__image">
                                        <div class="section5__image-inner">
                                            <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section5-certificate-4.jpg')?>" alt="">
                                        </div>
                                    </div>
                                    <div class="section5__name">
                                        <span><?php echo $sert4Sect5->text?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="section5__item">
                                <div class="section5__link js-mfp-image" data-url="<?php echo HTML::media('images/section5-certificate-1.jpg')?>">
                                    <div class="section5__image">
                                        <div class="section5__image-inner">
                                            <img class="js-inview-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/section5-certificate-1.jpg')?>" alt="">
                                        </div>
                                    </div>
                                    <div class="section5__name">
                                        <span><?php echo $sert5Sect5->text?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>