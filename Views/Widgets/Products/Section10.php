<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<div class="section10 pv-50 pv-lg30">
    <div class="grid i-25 i-lg15">
        <div class="cell cell--24">
            <div class="section10__item js-mfp-container">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid grid--jcenter iv-15 ig-25 ig-lg15">
                            <div class="cell cell--24">
                                <div class="section10__title revealator-slideright revealator-delay1 revealator-once">
                                    <span>Плиты пенополистирольные</span>
                                </div>
                            </div>
                            <div class="cell cell--12 cell--sm24">
                                <div class="section10__image revealator-zoomin revealator-delay1 revealator-once">
                                    <!-- @TODO Изображение должно быть самое большое, которое есть -->
                                    <div class="section10__image-item js-mfp-image" data-url="<?php HTML::media('images/section3-gallery-1.jpg')?>">
                                        <!-- @TODO Изображение должно быть таких размеров: 592х474 -->
                                        <img style="float: right; margin: 30px;" src="<?php echo HTML::media('images/section3-gallery-1.jpg') ?>" width="592" height="474"> </div>
                                </div>
                            </div>
                            <div class="cell cell--12 cell--ms24">
                                <div class="grid i-15">
                                    <div class="cell cell--24">
                                        <div class="section10__desc">
                                            <p>Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.</p>
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="section10__line"></div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="grid grid--acenter grid--jbetween i-10">
                                            <div class="cell">
                                                <div class="section10__price">
                                                    <span>Стоимость</span>
                                                    <div>от 180 м3/грн</div>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <a href="media-files/price-list.pdf" target="_blank" class="section10__link-block">
                                                    <div class="grid grid--acenter grid--nowrap i-10">
                                                        <div class="cell cell--nogrow cell--noshrink">
                                                            <div class="section10__icon">
                                                                <svg>
                                                                <use xlink:href="<?php HTML::media('svg/sprite.svg#icon-download')?>" />
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div class="cell cell--grow">
                                                            <div class="section10__link">
                                                                <span>Скачать прайс-лист</span>
                                                            </div>
                                                            <div class="section10__info">
                                                                <span>PDF, 1.2 Mb</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="cell cell--24">
                                                <div class="btn btn--big btn--full js-link" data-link=".section6">
                                                    <span>Оставить заявку</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section10__line mt-50 mt-lg30"></div>
            </div>
        </div>
        <div class="cell cell--24">
            <div class="section10__item js-mfp-container">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid grid--jcenter iv-15 ig-25 ig-lg15">
                            <div class="cell cell--24">
                                <div class="section10__title revealator-slideright revealator-delay1 revealator-once">
                                    <span>Плиты пенополистирольные</span>
                                </div>
                            </div>
                            <div class="cell cell--12 cell--sm24">
                                <div class="section10__image revealator-zoomin revealator-delay1 revealator-once">
                                    <!-- @TODO Изображение должно быть самое большое, которое есть -->
                                    <div class="section10__image-item js-mfp-image" data-url="<?php HTML::media('images/section3-gallery-2.jpg')?>">
                                        <!-- @TODO Изображение должно быть таких размеров: 592х474 -->
                                        <img style="float: right; margin: 30px;" src="<?php echo HTML::media('images/section3-gallery-2.jpg') ?>" width="592" height="474"> </div>
                                </div>
                            </div>
                            <div class="cell cell--12 cell--ms24">
                                <div class="grid i-15">
                                    <div class="cell cell--24">
                                        <div class="section10__desc">
                                            <p>Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.</p>
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="section10__line"></div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="grid grid--acenter grid--jbetween i-10">
                                            <div class="cell">
                                                <div class="section10__price">
                                                    <span>Стоимость</span>
                                                    <div>от 180 м3/грн</div>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <a href="media-files/price-list.pdf" target="_blank" class="section10__link-block">
                                                    <div class="grid grid--acenter grid--nowrap i-10">
                                                        <div class="cell cell--nogrow cell--noshrink">
                                                            <div class="section10__icon">
                                                                <svg>
                                                                <use xlink:href="<?php HTML::media('svg/sprite.svg#icon-download')?>" />
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div class="cell cell--grow">
                                                            <div class="section10__link">
                                                                <span>Скачать прайс-лист</span>
                                                            </div>
                                                            <div class="section10__info">
                                                                <span>PDF, 1.2 Mb</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="cell cell--24">
                                                <div class="btn btn--big btn--full js-link" data-link=".section6">
                                                    <span>Оставить заявку</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section10__line mt-50 mt-lg30"></div>
            </div>
        </div>
        <div class="cell cell--24">
            <div class="section10__item js-mfp-container">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid grid--jcenter iv-15 ig-25 ig-lg15">
                            <div class="cell cell--24">
                                <div class="section10__title revealator-slideright revealator-delay1 revealator-once">
                                    <span>Плиты пенополистирольные</span>
                                </div>
                            </div>
                            <div class="cell cell--12 cell--sm24">
                                <div class="section10__image revealator-zoomin revealator-delay1 revealator-once">
                                    <!-- @TODO Изображение должно быть самое большое, которое есть -->
                                    <div class="section10__image-item js-mfp-image" data-url="<?php HTML::media('images/section3-gallery-1.jpg')?>">
                                        <!-- @TODO Изображение должно быть таких размеров: 592х474 -->
                                        <img style="float: right; margin: 30px;" src="<?php echo HTML::media('images/section3-gallery-1.jpg') ?>" width="592" height="474"> </div>
                                </div>
                            </div>
                            <div class="cell cell--12 cell--ms24">
                                <div class="grid i-15">
                                    <div class="cell cell--24">
                                        <div class="section10__desc">
                                            <p>Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.</p>
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="section10__line"></div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="grid grid--acenter grid--jbetween i-10">
                                            <div class="cell">
                                                <div class="section10__price">
                                                    <span>Стоимость</span>
                                                    <div>от 180 м3/грн</div>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <a href="media-files/price-list.pdf" target="_blank" class="section10__link-block">
                                                    <div class="grid grid--acenter grid--nowrap i-10">
                                                        <div class="cell cell--nogrow cell--noshrink">
                                                            <div class="section10__icon">
                                                                <svg>
                                                                <use xlink:href="<?php HTML::media('svg/sprite.svg#icon-download')?>" />
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div class="cell cell--grow">
                                                            <div class="section10__link">
                                                                <span>Скачать прайс-лист</span>
                                                            </div>
                                                            <div class="section10__info">
                                                                <span>PDF, 1.2 Mb</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="cell cell--24">
                                                <div class="btn btn--big btn--full js-link" data-link=".section6">
                                                    <span>Оставить заявку</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section10__line mt-50 mt-lg30"></div>
            </div>
        </div>
        <div class="cell cell--24">
            <div class="section10__item js-mfp-container">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid grid--jcenter iv-15 ig-25 ig-lg15">
                            <div class="cell cell--24">
                                <div class="section10__title revealator-slideright revealator-delay1 revealator-once">
                                    <span>Плиты пенополистирольные</span>
                                </div>
                            </div>
                            <div class="cell cell--12 cell--sm24">
                                <div class="section10__image revealator-zoomin revealator-delay1 revealator-once">
                                    <!-- @TODO Изображение должно быть самое большое, которое есть -->
                                    <div class="section10__image-item js-mfp-image" data-url="<?php HTML::media('images/section3-gallery-2.jpg')?>">
                                        <!-- @TODO Изображение должно быть таких размеров: 592х474 -->
                                        <img style="float: right; margin: 30px;" src="<?php echo HTML::media('images/section3-gallery-2.jpg') ?>" width="592" height="474"> </div>
                                </div>
                            </div>
                            <div class="cell cell--12 cell--ms24">
                                <div class="grid i-15">
                                    <div class="cell cell--24">
                                        <div class="section10__desc">
                                            <p>Стандартные размеры раскроя листа вне зависимости от толщины - 2000х1000, 1000х1000 и 1000х500. Но мы имеем опыт по раскрою пенополистирольных блоков на листы нестандартных размеров исходя из желаний заказчиков.</p>
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="section10__line"></div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="grid grid--acenter grid--jbetween i-10">
                                            <div class="cell">
                                                <div class="section10__price">
                                                    <span>Стоимость</span>
                                                    <div>от 180 м3/грн</div>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <a href="media-files/price-list.pdf" target="_blank" class="section10__link-block">
                                                    <div class="grid grid--acenter grid--nowrap i-10">
                                                        <div class="cell cell--nogrow cell--noshrink">
                                                            <div class="section10__icon">
                                                                <svg>
                                                                <use xlink:href="<?php HTML::media('svg/sprite.svg#icon-download')?>" />
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div class="cell cell--grow">
                                                            <div class="section10__link">
                                                                <span>Скачать прайс-лист</span>
                                                            </div>
                                                            <div class="section10__info">
                                                                <span>PDF, 1.2 Mb</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="cell cell--24">
                                                <div class="btn btn--big btn--full js-link" data-link=".section6">
                                                    <span>Оставить заявку</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>