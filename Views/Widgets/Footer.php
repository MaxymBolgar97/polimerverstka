<?php

use Core\Widgets;
use Core\HTML;
use Core\Config;
?>
<footer>
    <div class="move-top">
        <div class="grid grid--jcenter p-10">
            <div class="cell">
                <div class="btn btn--transparent js-link" data-link="main">
                    <svg>
                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-up')?>" />
                    </svg>
                    <span>Вернуться наверх</span>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-block">
        <div class="grid grid--lg pg-30">
            <div class="cell cell--24">
                <div class="grid grid--jbetween grid--smcol grid--smacenter i-10">
                    <div class="cell cell--4 cell--ms8 cell--sm sm-gcenter">
                        <div class="footer-block__logo">
                            <img src="<?php echo HTML::media('pic/logo.png')?>" alt="">
                        </div>
                        <div class="footer-block__text">
                            <span><?php echo $footerText1->text ?></span>
                        </div>
                    </div>
                    <div class="cell cell--8 cell--ms0">
                        <div class="grid i-5">
                            <?php foreach ($contentMenu as $obj): ?>
                                <div class="cell cell--12">
                                    <a href="<?php echo HTML::link($obj->url); ?>" class="footer-block__link">
                                        <span><?php echo $obj->name ?></span>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="cell cell--6 cell--ms8 cell--sm12 cell--xs24">
                        <div class="grid grid--nowrap i-5">
                            <div class="cell cell--nogrow cell--noshrink">
                                <div class="footer-block__icon">
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-address')?>" />
                                    </svg>
                                </div>
                            </div>
                            <div class="cell cell--grow">
                                <div class="footer-block__label">
                                    <span>Адрес компании:</span>
                                </div>
                                <div class="footer-block__address">
                                    <p><?php echo $footerAdress->text ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cell cell--6 cell--ms8 cell--sm12 cell--xs24">
                        <div class="grid grid--nowrap i-5">
                            <div class="cell cell--nogrow cell--noshrink">
                                <div class="footer-block__icon">
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-phone')?>" />
                                    </svg>
                                </div>
                            </div>
                            <div class="cell cell--grow">
                                <div class="footer-block__label">
                                    <span>Дирекция:</span>
                                </div>
                                <div class="footer-block__address">
                                    <span>Тел: </span>
                                    <a href="tel:<?php echo $footerTel1->text ?>" class="footer-block__tel"><?php echo $footerTel1->text ?></a>
                                </div>
                                <div class="footer-block__label">
                                    <span>Отдел сбыта:</span>
                                </div>
                                <div class="footer-block__address">
                                    <span>Тел: </span>
                                    <a href="tel:<?php echo $footerTel1->text ?>" class="footer-block__tel"><?php echo $footerTel2->text ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cell cell--24">
                        <div class="footer-block__line"></div>
                    </div>
                    <div class="cell cell--ms14 cell--sm">
                        <div class="footer-block__copyright">
                            <span><?php echo $footerCopyright->text ?></span>
                            <a href="/" class="ms-block">Политика конфиденциальности</a>
                            <a href="/sitemap" class="ms-block">Карта сайта</a>
                        </div>
                    </div>
                    <div class="cell">
                        <a href="http://wezom.com.ua/" class="footer-block__wezom" target="_blank">
                            <span>Агентство Wezom</span>
                            <svg>
                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#wezom-logo')?>" />
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <svg class="clip-path">
    <clipPath id="icon-triangle-right">
        <path d="M395,0h825v680H0L395,0z"></path>
    </clipPath>
    <clipPath id="icon-triangle-left">
        <path d="M0,0h1220L848,640H0V0z"></path>
    </clipPath>
    </svg>
</footer>