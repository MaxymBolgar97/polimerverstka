<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<div class="section11 pv-50 pv-lg30">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="grid iv-15 ig-25 ig-lg15">
                <div class="cell cell--24">
                    <div class="section11__title">
                        <span>Цены на нашу продукцию</span>
                    </div>
                </div>
                <div class="cell cell--16 cell--lg14 cell--ms24">
                    <div class="table-wrapper js-table-wrapper">
                        <div class="table-wrapper__holder js-table-wrapper__holder">
                            <table class="table-wrapper__table js-table-wrapper__table">
                                <thead>
                                    <tr>
                                        <td>
                                            <span>Перечень товаров</span>
                                        </td>
                                        <td>
                                            <span>Стоимость при заказе до 30 м3, м3/грн</span>
                                        </td>
                                        <td>
                                            <span>Стоимость при заказе 30-60 м3, м3/грн</span>
                                        </td>
                                        <td>
                                            <span>Стоимость при заказе 60-120 м3, м3/грн</span>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="4">
                                            <span>Плиты пенополистирольные</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Плита пенополистирольная ПСБ-С-15</span>
                                        </td>
                                        <td>
                                            <span>190</span>
                                        </td>
                                        <td>
                                            <span>185</span>
                                        </td>
                                        <td>
                                            <span>180</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Плита пенополистирольная ПСБ-С-15</span>
                                        </td>
                                        <td>
                                            <span>190</span>
                                        </td>
                                        <td>
                                            <span>185</span>
                                        </td>
                                        <td>
                                            <span>180</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Плита пенополистирольная ПСБ-С-15</span>
                                        </td>
                                        <td>
                                            <span>190</span>
                                        </td>
                                        <td>
                                            <span>185</span>
                                        </td>
                                        <td>
                                            <span>180</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Плита пенополистирольная ПСБ-С-15</span>
                                        </td>
                                        <td>
                                            <span>190</span>
                                        </td>
                                        <td>
                                            <span>185</span>
                                        </td>
                                        <td>
                                            <span>180</span>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody>
                                    <tr>
                                        <td colspan="4">
                                            <span>Плиты пенополистирольные</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Плита пенополистирольная ПСБ-С-15</span>
                                        </td>
                                        <td>
                                            <span>190</span>
                                        </td>
                                        <td>
                                            <span>185</span>
                                        </td>
                                        <td>
                                            <span>180</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Плита пенополистирольная ПСБ-С-15</span>
                                        </td>
                                        <td>
                                            <span>190</span>
                                        </td>
                                        <td>
                                            <span>185</span>
                                        </td>
                                        <td>
                                            <span>180</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Плита пенополистирольная ПСБ-С-15</span>
                                        </td>
                                        <td>
                                            <span>190</span>
                                        </td>
                                        <td>
                                            <span>185</span>
                                        </td>
                                        <td>
                                            <span>180</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Плита пенополистирольная ПСБ-С-15</span>
                                        </td>
                                        <td>
                                            <span>190</span>
                                        </td>
                                        <td>
                                            <span>185</span>
                                        </td>
                                        <td>
                                            <span>180</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="cell cell--8 cell--lg10 cell--ms24">
                    <div class="section11__right mb-50 mb-lg30">
                        <div class="grid i-20">
                            <div class="cell cell--24">
                                <a href="media-files/price-list.pdf" target="_blank" class="section11__link-block">
                                    <div class="grid grid--acenter grid--nowrap i-10">
                                        <div class="cell cell--nogrow cell--noshrink">
                                            <div class="section11__icon">
                                                <svg>
                                                <use xlink:href="svg/sprite.svg#icon-download" />
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="cell cell--grow">
                                            <div class="section11__link">
                                                <span>Скачать прайс-лист</span>
                                            </div>
                                            <div class="section11__info">
                                                <span>PDF, 1.2 Mb</span>
                                            </div>
                                            <div class="section11__line"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="cell cell--24">
                                <a href="media-files/price-list.xls" target="_blank" class="section11__link-block">
                                    <div class="grid grid--acenter grid--nowrap i-10">
                                        <div class="cell cell--nogrow cell--noshrink">
                                            <div class="section11__icon">
                                                <svg>
                                                <use xlink:href="svg/sprite.svg#icon-download" />
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="cell cell--grow">
                                            <div class="section11__link">
                                                <span>Скачать прайс-лист</span>
                                            </div>
                                            <div class="section11__info">
                                                <span>XLS, 0.6 Mb</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="section11__desc view-text">
                        <p>Минимальная расчетная стоимость заказа на услуги – 30000 рублей.</p>
                        <p>Все цены в таблице являются ориентировочными. В зависимости от сложности и величины заказа они могут корректироваться.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>