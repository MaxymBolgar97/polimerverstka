<?php

use Core\HTML;
use Core\Config;
use Core\Widgets;
?>
<div class="section8 section8--background js-section">
    <div class="section8__right">
        <div class="section8__right-inner">
            <div class="section8__right-svg">
                <svg viewBox="0 0 1220 680">
                <!-- @TODO Изображение должно быть таких размеров: 1220х680 -->
                <image xlink:href="<?php echo HTML::media('images/section8-bg-3.jpg') ?>" x="0" y="0" width="1220" height="680" clip-path="url(#icon-triangle-right)"></image>
                </svg>
            </div>
        </div>
    </div>
    <div class="section8__left">
        <div class="section8__left-inner">
            <div class="section8__left-svg">
                <svg>
                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-triangle-left') ?>" />
                </svg>
            </div>
        </div>
    </div>
    <div class="section8__block">
        <div class="grid grid--lg pg-30">
            <div class="cell cell--24">
                <div class="grid grid--col grid--nowrap grid--jcenter iv-15 iv-md10 iv-ms5 js-section-block">
                    <div class="cell cell--14 cell--md13 cell--sm24">
                        <div class="section8__title revealator-slideright revealator-delay1 revealator-once">
                            <span><?php echo $firstLineTop->text ?></span>
                            <div><?php echo $secondLineTop->text . ' ' . $thirdLineTop->text ?></div>
                        </div>
                    </div>
                    <div class="cell cell--11 cell--lg12 cell--md112 cell--sm24">
                        <div class="section8__text revealator-slideleft revealator-delay2 revealator-once">
                            <span><?php echo $paragTop->text ?></span>
                        </div>
                    </div>
                    <div class="cell cell--sm24">
                        <div class="section8__btn revealator-slideright revealator-delay4 revealator-once">
                            <div class="btn btn--big js-link" data-link=".section6">
                                <span><?php echo $btnSect8priceList->text ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>