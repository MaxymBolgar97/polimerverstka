<?php
use Core\HTML; 
use Core\Widgets;
?>
<style>
    section {
        text-align: center;
    }
    form {
        text-align: center;
        display: inline-block;
    }
    input {
        display: block;
        border: 2px #000 solid;
        padding: 10px;
        margin: 10px;
    }
</style>
<section>
    <h2>Вход \ Регистрация</h2>
    <form action="/reg" method="POST">
        <input type="text" name="nameF" placeholder="NAME"/>
        <input type="text" name="loginF" placeholder="LOGIN"/>
        <input type="email" name="emailF" placeholder="EMAIL"/>
        <input type="password" name="passwordF" placeholder="PASSWORD"/>
        <input type="submit" name="sendF" value="SEND"/>
    </form>
    <?php echo Widgets::get('Scripts'); ?>
</section>
