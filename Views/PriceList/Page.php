<?php
use Core\HTML; 
use Core\Widgets;
?>
<section>
    <?php echo Widgets::get('PriceList_Section8'); ?>
    <?php echo Widgets::get('PriceList_Section11'); ?>
    <?php echo Widgets::get('Index_Section11'); ?>
    <?php echo Widgets::get('Index_Section6'); ?>
    <?php echo Widgets::get('Index_Section7'); ?>
    <?php echo Widgets::get('Scripts'); ?>
</section>