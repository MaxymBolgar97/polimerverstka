<?php

use Core\HTML;
use Core\Widgets;
?>
<section>
    <div class="page pv-30">
        <div class="grid grid--md pg-30">
            <div class="cell cell--24">
                <div class="page__title">
                    <span>Карта сайта</span>
                </div>
                <div class="page__content view-text">
                    <nav class="sitemap">
                        <ul>
                            <?php foreach($result as $link): ?>
                            <li>
                                <a href="<?php echo $link->url ?>">> <?php echo $link->name?></a>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
<?php echo Widgets::get('Scripts'); ?>
</section>