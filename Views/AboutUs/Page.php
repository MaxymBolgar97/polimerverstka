<?php
use Core\HTML; 
use Core\Widgets;
?>
<section>
    <?php echo Widgets::get('AboutUs_Section8'); ?>
    <?php echo Widgets::get('AboutUs_Section9'); ?>
    <?php echo Widgets::get('Index_Section4'); ?>
    <?php echo Widgets::get('Index_Section5'); ?>
    <?php echo Widgets::get('Index_Section6'); ?>
    <?php echo Widgets::get('Index_Section7'); ?>
    <?php echo Widgets::get('Scripts'); ?>
</section>