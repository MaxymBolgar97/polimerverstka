<?php
use Core\HTML; 
use Core\Widgets;
?>
<style>
    section {
        margin-top: 40px;
        text-align: center;
    }
    form {
        margin-top: 40px;
        text-align: center;
        display: inline-block;
    }
    input {
        display: inline-block;
        border: 2px #000 solid;
        padding: 10px;
        margin: 10px;
        
    }
    #reg {
        display: inline-block;
        border: 2px #000 solid;
        padding: 10px;
        margin: 10px;
    }
</style>
<section>
    <h2>Вход \ Регистрация</h2>
    <form action="/login" method="POST">
        <input type="email" name="emailF" placeholder="EMAIL"/>
        <input type="password" name="passwordF" placeholder="PASSWORD"/>
        <input type="submit" name="sendF" value="Войти"/>
        <a id="reg" href="/registF">Регистрация</a>
    </form>
    <?php echo Widgets::get('Scripts'); ?>
</section>
