<?php ob_start(); ?>
<div class="mfp-popup" style="background-image: url(pic/call-bg.png)">
	<div class="popup-form form js-form" data-form="true" data-ajax="hidden/call-resp.php">
		<div class="grid grid--col i-15">
			<div class="cell cell--24">
				<div class="popup-form__title">
					<span>Заказать обратный звонок</span>
				</div>
			</div>
			<div class="cell cell--24">
				<div class="form__label">Ваше имя *</div>
				<div class="control-holder control-holder--text">
					<input type="text" name="name" data-name="name" required="" data-rule-minlength="2" data-rule-word="true">
				</div>
			</div>
			<div class="cell cell--24">
				<div class="form__label">Номер телефона *</div>
				<div class="control-holder control-holder--text">
					<input type="tel" name="tel" data-name="tel" required="" data-rule-phone="true" class="js-inputmask" data-mask="+38(099)999-99-99">
				</div>
			</div>
			<div class="cell cell--24">
				<div class="control-holder control-holder--flag">
					<label>
						<input type="checkbox" name="agree" data-name="agree" value="1" required>
						<ins>
							<svg><use xlink:href="svg/sprite.svg#icon-check" /></svg>
						</ins>
						<span>Я согласен с <a href="text.html" class="form__link" target="_blank">политикой конфиденциальности</a></span>
					</label>
					<div>
						<label for="agree" class="has-error" style="display: none;"></label>
					</div>
				</div>
			</div>
			<div class="cell cell--24">
				<div class="btn btn--full btn--big js-form-submit">
					<span>Оставить заявку</span>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo $content;
die;
?>