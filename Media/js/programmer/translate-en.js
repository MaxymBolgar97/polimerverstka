var validationTranslate = {
	required: "This field is required!",
	required_checker: "This parameter is required!",
	required_select: "This parameter is required!",

	remote: "Please fix this field.",
	email: "Please enter a valid email address.",
	url: "Please enter a valid URL.",
	date: "Please enter a valid date.",
	dateISO: "Please enter a valid date (ISO).",
	number: "Please enter a valid number.",
	digits: "Please enter only digits.",
	creditcard: "Please enter a valid credit card number.",
	equalTo: "Please enter the same value again.",

	maxlength: "Please enter no more than {0} characters.",
	maxlength_checker: "Please select no more than {0} parameters!",
	maxlength_select: "Please select no more than {0} items!",

	minlength: "Please enter at least {0} characters.",
	minlength_checker: "Please select at least {0} options!",
	minlength_select: "Please select at least {0} items!",

	rangelength: "Please enter a value between {0} and {1} characters long.",
	rangelength_checker: "Please select from {0} to {1} options!",
	rangelength_select: "Please select from {0} to {1} items!",

	range: "Please enter a value between {0} and {1}.",
	max: "Please enter a value less than or equal to {0}.",
	min: "Please enter a value greater than or equal to {0}.",

	filetype: "Allowed file extensions: {0}!",
	filesize: "Maximum size {0}KB!",
	filesizeEach: "Maximum amount of each file {0}KB!",

	pattern: "Specify a value corresponding to the mask {0}!",
	word: "Please enter the correct word meanings!",
	noempty: "The field can not be empty!",
	login: "Please enter a valid username!",
	phoneUA: "Invalid phone number format Ukrainian",
	phone: "Please enter valid phone number!"
};

var mfpTranslate = {
	tClose: 'Close (ESC)',
	tLoading: 'Loading ...',
	tNotFound: 'Content not found',
	tError: '<a href="%url%" target="_blank">The content</a> could not be loaded.',
	tErrorImage: '<a href="%url%" target="_blank">The image #%curr%</a> could not be loaded.',
	tPrev: 'Previous (Left arrow key)',
	tNext: 'Next (Right arrow key)',
	tCounter: '%curr% of %total%'
};

var timelineTranslate = {
	"name": "English",
	"lang": "en",
	"date": {
		"month_abbr": [
			"Jan.",
			"Feb.",
			"March",
			"April",
			"May",
			"June",
			"July",
			"Aug.",
			"Sept.",
			"Oct.",
			"Nov.",
			"Dec."
		],
		"day_abbr": [
			"Sun.",
			"Mon.",
			"Tues.",
			"Wed.",
			"Thurs.",
			"Fri.",
			"Sat."
		],
		"day": [
			"Sunday",
			"Monday",
			"Tuesday",
			"Wednesday",
			"Thursday",
			"Friday",
			"Saturday"
		],
		"month": [
			"January",
			"February",
			"March",
			"April",
			"May",
			"June",
			"July",
			"August",
			"September",
			"October",
			"November",
			"December"
		]
	},
	"api": {
		"wikipedia": "en" // the two letter code at the beginning of the Wikipedia subdomain for this language
	},
	"messages": {
		"loading": "Loading",
		"error": "Error",
		"contract_timeline": "Contract Timeline",
		"return_to_title": "Return to Title",
		"wikipedia": "From Wikipedia, the free encyclopedia",
		"loading_content": "Loading Content",
		"expand_timeline": "Expand Timeline",
		"loading_timeline": "Loading Timeline... ",
		"swipe_to_navigate": "Swipe to Navigate<br><span class='tl-button'>OK</span>",
		"unknown_read_err": "An unexpected error occurred trying to read your spreadsheet data",
		"invalid_url_err": "Unable to read Timeline data. Make sure your URL is for a Google Spreadsheet or a Timeline JSON file.",
		"network_err": "Unable to read your Google Spreadsheet. Make sure you have published it to the web.",
		"empty_feed_err": "No data entries found",
		"missing_start_date_err": "Missing start_date",
		"invalid_data_format_err": "Header row has been modified.",
		"date_compare_err": "Can't compare TL.Dates on different scales",
		"invalid_scale_err": "Invalid scale",
		"invalid_date_err": "Invalid date: month, day and year must be numbers.",
		"invalid_separator_error": "Invalid time: misuse of : or . as separator.",
		"invalid_hour_err": "Invalid time (hour)",
		"invalid_minute_err": "Invalid time (minute)",
		"invalid_second_err": "Invalid time (second)",
		"invalid_fractional_err": "Invalid time (fractional seconds)",
		"invalid_second_fractional_err": "Invalid time (seconds and fractional seconds)",
		"invalid_year_err": "Invalid year",
		"flickr_notfound_err": "Photo not found or private",
		"flickr_invalidurl_err": "Invalid Flickr URL",
		"imgur_invalidurl_err": "Invalid Imgur URL",
		"twitter_invalidurl_err": "Invalid Twitter URL",
		"twitter_load_err": "Unable to load Tweet",
		"twitterembed_invalidurl_err": "Invalid Twitter Embed url",
		"wikipedia_load_err": "Unable to load Wikipedia entry",
		"youtube_invalidurl_err": "Invalid YouTube URL",
		"spotify_invalid_url": "Invalid Spotify URL",
		"template_value_err": "No value provided for variable",
		"invalid_rgb_err": "Invalid RGB argument",
		"time_scale_scale_err": "Don't know how to get date from time for scale",
		"axis_helper_no_options_err": "Axis helper must be configured with options",
		"axis_helper_scale_err": "No AxisHelper available for scale",
		"invalid_integer_option": 				"Invalid option value—must be a whole number."
	},
	"dateformats": {
		"full_long": "mmm d',' yyyy 'at' h:MM TT",
		"full_short": "mmm d",
		"full": "mmmm d',' yyyy",
		"month_short": "mmm",
		"time_no_seconds_small_date": "h:MM TT'<br/><small>'mmmm d',' yyyy'</small>'",
		"month": "mmmm yyyy",
		"time_no_seconds_short": "h:MM TT",
		"time_short": "h:MM:ss TT",
		"year": "yyyy",
		"full_long_small_date": "h:MM TT'<br/><small>mmm d',' yyyy'</small>'"
	},
	"era_labels": { // specify prefix or suffix to apply to formatted date. Blanks mean no change.
		"positive_year": {
			"prefix": "",
			"suffix": ""
		},
		"negative_year": { // if either of these is specified, the year will be converted to positive before they are applied
			"prefix": "",
			"suffix": "BCE"
		}
	},
	"period_labels": {  // use of t/tt/T/TT is a legacy of original Timeline date format
		"t": ["a", "p"],
		"tt": ["am", "pm"],
		"T": ["A", "P"],
		"TT": ["AM", "PM"]
	},
};

var woldTranslate = {
	en: {
		'title': 'Studio Wezom',
		'close': 'Close',
		'small': ' or younger',
		'end-1': 'The site may not work properly. We recommend you to use <b>%b</b>.</p>',
		'end-2': 'The site may not work properly. We recommend you to use <b>%b</b>.</p>',
		'end-3': 'The site may not work properly.</p>',
		'inform': '<p>You are using an old browser - <b>%w</b>!',
		'device': '<p>You are using - <b>%w</b>!',
		'old-os': '<p>You are using an outdated operating system - <b>%w</b>!',
		'ffx-esr': '<p>You are using <b> <a href="https://www.mozilla.org/en-US/firefox/organizations/" title="Firefox - ESR" target="_blank">Firefox - ESR</a></b>!',
		'unknown': '<p>You are using an unknown browser!'
	}
}