/*!
 * modernizr v3.3.1
 * Build http://modernizr.com/download?-android-chrome-contains-csstransitions-dataset-edge-hidden-history-ie-input-ios-mac-moz-opacity-opera-placeholder-retina-safari-search-template-touchevents-prefixes-setclasses-dontmin
 *
 * Copyright (c)
 *  Faruk Ates
 *  Paul Irish
 *  Alex Sexton
 *  Ryan Seddon
 *  Patrick Kettner
 *  Stu Cox
 *  Richard Herrera

 * MIT License
 */
!function(window,document,undefined){function is(obj,type){return typeof obj===type}function testRunner(){var featureNames,feature,aliasIdx,result,nameIdx,featureName,featureNameSplit;for(var featureIdx in tests)if(tests.hasOwnProperty(featureIdx)){if(featureNames=[],feature=tests[featureIdx],feature.name&&(featureNames.push(feature.name.toLowerCase()),feature.options&&feature.options.aliases&&feature.options.aliases.length))for(aliasIdx=0;aliasIdx<feature.options.aliases.length;aliasIdx++)featureNames.push(feature.options.aliases[aliasIdx].toLowerCase());for(result=is(feature.fn,"function")?feature.fn():feature.fn,nameIdx=0;nameIdx<featureNames.length;nameIdx++)featureName=featureNames[nameIdx],featureNameSplit=featureName.split("."),1===featureNameSplit.length?Modernizr[featureNameSplit[0]]=result:(!Modernizr[featureNameSplit[0]]||Modernizr[featureNameSplit[0]]instanceof Boolean||(Modernizr[featureNameSplit[0]]=new Boolean(Modernizr[featureNameSplit[0]])),Modernizr[featureNameSplit[0]][featureNameSplit[1]]=result),classes.push((result?"":"no-")+featureNameSplit.join("-"))}}function setClasses(classes){var className=docElement.className,classPrefix=Modernizr._config.classPrefix||"";if(isSVG&&(className=className.baseVal),Modernizr._config.enableJSClass){var reJS=new RegExp("(^|\\s)"+classPrefix+"no-js(\\s|$)");className=className.replace(reJS,"$1"+classPrefix+"js$2")}Modernizr._config.enableClasses&&(className+=" "+classPrefix+classes.join(" "+classPrefix),isSVG?docElement.className.baseVal=className:docElement.className=className)}function createElement(){return"function"!=typeof document.createElement?document.createElement(arguments[0]):isSVG?document.createElementNS.call(document,"http://www.w3.org/2000/svg",arguments[0]):document.createElement.apply(document,arguments)}function getBody(){var body=document.body;return body||(body=createElement(isSVG?"svg":"body"),body.fake=!0),body}function injectElementWithStyles(rule,callback,nodes,testnames){var style,ret,node,docOverflow,mod="modernizr",div=createElement("div"),body=getBody();if(parseInt(nodes,10))for(;nodes--;)node=createElement("div"),node.id=testnames?testnames[nodes]:mod+(nodes+1),div.appendChild(node);return style=createElement("style"),style.type="text/css",style.id="s"+mod,(body.fake?body:div).appendChild(style),body.appendChild(div),style.styleSheet?style.styleSheet.cssText=rule:style.appendChild(document.createTextNode(rule)),div.id=mod,body.fake&&(body.style.background="",body.style.overflow="hidden",docOverflow=docElement.style.overflow,docElement.style.overflow="hidden",docElement.appendChild(body)),ret=callback(div,rule),body.fake?(body.parentNode.removeChild(body),docElement.style.overflow=docOverflow,docElement.offsetHeight):div.parentNode.removeChild(div),!!ret}function contains(str,substr){return!!~(""+str).indexOf(substr)}function domToCSS(name){return name.replace(/([A-Z])/g,function(str,m1){return"-"+m1.toLowerCase()}).replace(/^ms-/,"-ms-")}function nativeTestProps(props,value){var i=props.length;if("CSS"in window&&"supports"in window.CSS){for(;i--;)if(window.CSS.supports(domToCSS(props[i]),value))return!0;return!1}if("CSSSupportsRule"in window){for(var conditionText=[];i--;)conditionText.push("("+domToCSS(props[i])+":"+value+")");return conditionText=conditionText.join(" or "),injectElementWithStyles("@supports ("+conditionText+") { #modernizr { position: absolute; } }",function(node){return"absolute"==getComputedStyle(node,null).position})}return undefined}function cssToDOM(name){return name.replace(/([a-z])-([a-z])/g,function(str,m1,m2){return m1+m2.toUpperCase()}).replace(/^-/,"")}function testProps(props,prefixed,value,skipValueTest){function cleanElems(){afterInit&&(delete mStyle.style,delete mStyle.modElem)}if(skipValueTest=!is(skipValueTest,"undefined")&&skipValueTest,!is(value,"undefined")){var result=nativeTestProps(props,value);if(!is(result,"undefined"))return result}for(var afterInit,i,propsLength,prop,before,elems=["modernizr","tspan"];!mStyle.style;)afterInit=!0,mStyle.modElem=createElement(elems.shift()),mStyle.style=mStyle.modElem.style;for(propsLength=props.length,i=0;i<propsLength;i++)if(prop=props[i],before=mStyle.style[prop],contains(prop,"-")&&(prop=cssToDOM(prop)),mStyle.style[prop]!==undefined){if(skipValueTest||is(value,"undefined"))return cleanElems(),"pfx"!=prefixed||prop;try{mStyle.style[prop]=value}catch(e){}if(mStyle.style[prop]!=before)return cleanElems(),"pfx"!=prefixed||prop}return cleanElems(),!1}function fnBind(fn,that){return function(){return fn.apply(that,arguments)}}function testDOMProps(props,obj,elem){var item;for(var i in props)if(props[i]in obj)return elem===!1?props[i]:(item=obj[props[i]],is(item,"function")?fnBind(item,elem||obj):item);return!1}function testPropsAll(prop,prefixed,elem,value,skipValueTest){var ucProp=prop.charAt(0).toUpperCase()+prop.slice(1),props=(prop+" "+cssomPrefixes.join(ucProp+" ")+ucProp).split(" ");return is(prefixed,"string")||is(prefixed,"undefined")?testProps(props,prefixed,value,skipValueTest):(props=(prop+" "+domPrefixes.join(ucProp+" ")+ucProp).split(" "),testDOMProps(props,prefixed,elem))}function testAllProps(prop,value,skipValueTest){return testPropsAll(prop,undefined,undefined,value,skipValueTest)}var tests=[],ModernizrProto={_version:"3.3.1",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(test,cb){var self=this;setTimeout(function(){cb(self[test])},0)},addTest:function(name,fn,options){tests.push({name:name,fn:fn,options:options})},addAsyncTest:function(fn){tests.push({name:null,fn:fn})}},Modernizr=function(){};Modernizr.prototype=ModernizrProto,Modernizr=new Modernizr;var classes=[],docElement=document.documentElement,isSVG="svg"===docElement.nodeName.toLowerCase(),prefixes=ModernizrProto._config.usePrefixes?" -webkit- -moz- -o- -ms- ".split(" "):[];ModernizrProto._prefixes=prefixes;var testStyles=ModernizrProto.testStyles=injectElementWithStyles;/*!
{
  "name": "Touch Events",
  "property": "touchevents",
  "caniuse" : "touch",
  "tags": ["media", "attribute"],
  "notes": [{
    "name": "Touch Events spec",
    "href": "https://www.w3.org/TR/2013/WD-touch-events-20130124/"
  }],
  "warnings": [
    "Indicates if the browser supports the Touch Events spec, and does not necessarily reflect a touchscreen device"
  ],
  "knownBugs": [
    "False-positive on some configurations of Nokia N900",
    "False-positive on some BlackBerry 6.0 builds – https://github.com/Modernizr/Modernizr/issues/372#issuecomment-3112695"
  ]
}
!*/
Modernizr.addTest("touchevents",function(){var bool;if("ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch)bool=!0;else{var query=["@media (",prefixes.join("touch-enabled),("),"heartz",")","{#modernizr{top:9px;position:absolute}}"].join("");testStyles(query,function(node){bool=9===node.offsetTop})}return bool}),/*!
{
  "name": "ios",
  "property": "ios"
}
!*/
Modernizr.addTest("ios",null!==navigator.platform.match(/(iPhone|iPod|iPad)/i)),/*!
{
  "name": "android",
  "property": "android"
}
!*/
Modernizr.addTest("android",navigator.userAgent.toLowerCase().indexOf("android")>=0),/*!
{
  "name": "ie",
  "property": "ie"
}
!*/
Modernizr.addTest("ie",document.documentMode),/*!
{
  "name": "moz",
  "property": "moz"
}
!*/
Modernizr.addTest("moz","undefined"!=typeof InstallTrigger),/*!
{
  "name": "opera",
  "property": "opera"
}
!*/
Modernizr.addTest("opera",!!window.opera||null!==navigator.userAgent.match(/Opera|OPR\//)),/*!
{
  "name": "safari",
  "property": "safari"
}
!*/
Modernizr.addTest("safari",Object.prototype.toString.call(window.HTMLElement).indexOf("Constructor")>0),/*!
{
  "name": "retina",
  "property": "retina"
}
!*/
Modernizr.addTest("retina",function(){var dpr=window.devicePixelRatio||window.screen.deviceXDPI/window.screen.logicalXDPI||1,flag=dpr>1;return!!flag}),/*!
{
  "name": "mac",
  "property": "mac"
}
!*/
Modernizr.addTest("mac",navigator.platform.toLowerCase().indexOf("mac")>=0),/*!
{
  "name": "edge",
  "property": "edge"
}
!*/
Modernizr.addTest("edge",navigator.userAgent.toLowerCase().indexOf(" edge/")>0),/*!
{
  "name": "chrome",
  "property": "chrome"
}
!*/
Modernizr.addTest("chrome",function(){var ua=navigator.userAgent.toLowerCase(),chrome=!!window.chrome&&null===ua.match(/Opera|OPR\//i),edje=ua.indexOf(" edge/")>0;return chrome&&!edje}),/*!
{
  "name": "History API",
  "property": "history",
  "caniuse": "history",
  "tags": ["history"],
  "authors": ["Hay Kranen", "Alexander Farkas"],
  "notes": [{
    "name": "W3C Spec",
    "href": "https://www.w3.org/TR/html51/browsers.html#the-history-interface"
  }, {
    "name": "MDN documentation",
    "href": "https://developer.mozilla.org/en-US/docs/Web/API/window.history"
  }],
  "polyfills": ["historyjs", "html5historyapi"]
}
!*/
Modernizr.addTest("history",function(){var ua=navigator.userAgent;return(ua.indexOf("Android 2.")===-1&&ua.indexOf("Android 4.0")===-1||ua.indexOf("Mobile Safari")===-1||ua.indexOf("Chrome")!==-1||ua.indexOf("Windows Phone")!==-1)&&(window.history&&"pushState"in window.history)});var inputElem=createElement("input"),inputattrs="autocomplete autofocus list placeholder max min multiple pattern required step".split(" "),attrs={};Modernizr.input=function(props){for(var i=0,len=props.length;i<len;i++)attrs[props[i]]=!!(props[i]in inputElem);return attrs.list&&(attrs.list=!(!createElement("datalist")||!window.HTMLDataListElement)),attrs}(inputattrs);var hasEvent=function(){function inner(eventName,element){var isSupported;return!!eventName&&(element&&"string"!=typeof element||(element=createElement(element||"div")),eventName="on"+eventName,isSupported=eventName in element,!isSupported&&needsFallback&&(element.setAttribute||(element=createElement("div")),element.setAttribute(eventName,""),isSupported="function"==typeof element[eventName],element[eventName]!==undefined&&(element[eventName]=undefined),element.removeAttribute(eventName)),isSupported)}var needsFallback=!("onblur"in document.documentElement);return inner}();ModernizrProto.hasEvent=hasEvent,/*!
{
  "name": "input[search] search event",
  "property": "search",
  "tags": ["input","search"],
  "authors": ["Calvin Webster"],
  "notes": [{
    "name": "Wufoo demo",
    "href": "https://www.wufoo.com/html5/types/5-search.html?"
  }, {
    "name": "CSS Tricks",
    "href": "https://css-tricks.com/webkit-html5-search-inputs/"
  }]
}
!*/
Modernizr.addTest("inputsearchevent",hasEvent("search")),/*!
{
  "name": "CSS Opacity",
  "caniuse": "css-opacity",
  "property": "opacity",
  "tags": ["css"]
}
!*/
Modernizr.addTest("opacity",function(){var style=createElement("a").style;return style.cssText=prefixes.join("opacity:.55;"),/^0.55$/.test(style.opacity)});var omPrefixes="Moz O ms Webkit",cssomPrefixes=ModernizrProto._config.usePrefixes?omPrefixes.split(" "):[];ModernizrProto._cssomPrefixes=cssomPrefixes;var modElem={elem:createElement("modernizr")};Modernizr._q.push(function(){delete modElem.elem});var mStyle={style:modElem.elem.style};Modernizr._q.unshift(function(){delete mStyle.style});var domPrefixes=ModernizrProto._config.usePrefixes?omPrefixes.toLowerCase().split(" "):[];ModernizrProto._domPrefixes=domPrefixes,ModernizrProto.testAllProps=testPropsAll,ModernizrProto.testAllProps=testAllProps,/*!
{
  "name": "CSS Transitions",
  "property": "csstransitions",
  "caniuse": "css-transitions",
  "tags": ["css"]
}
!*/
Modernizr.addTest("csstransitions",testAllProps("transition","all",!0)),/*!
{
  "name": "dataset API",
  "caniuse": "dataset",
  "property": "dataset",
  "tags": ["dom"],
  "builderAliases": ["dom_dataset"],
  "authors": ["@phiggins42"]
}
!*/
Modernizr.addTest("dataset",function(){var n=createElement("div");return n.setAttribute("data-a-b","c"),!(!n.dataset||"c"!==n.dataset.aB)}),/*!
{
  "name": "[hidden] Attribute",
  "property": "hidden",
  "tags": ["dom"],
  "notes": [{
    "name": "WHATWG: The hidden attribute",
    "href": "https://developers.whatwg.org/editing.html#the-hidden-attribute"
  }, {
    "name": "original implementation of detect code",
    "href": "https://github.com/aFarkas/html5shiv/blob/bf4fcc4/src/html5shiv.js#L38"
  }],
  "polyfills": ["html5shiv"],
  "authors": ["Ron Waldon (@jokeyrhyme)"]
}
!*/
Modernizr.addTest("hidden","hidden"in createElement("a")),/*!
{
  "name": "Template Tag",
  "property": "template",
  "tags": ["elem"],
  "notes": [{
    "name": "HTML5Rocks Article",
    "href": "http://www.html5rocks.com/en/tutorials/webcomponents/template/"
  },{
    "name": "W3 Spec",
    "href": "https://dvcs.w3.org/hg/webcomponents/raw-file/tip/spec/templates/index.html"
  }]
}
!*/
Modernizr.addTest("template","content"in createElement("template")),/*!
{
  "name": "ES5 String.prototype.contains",
  "property": "contains",
  "authors": ["Robert Kowalski"],
  "tags": ["es6"]
}
!*/
Modernizr.addTest("contains",is(String.prototype.contains,"function")),/*!
{
  "name": "placeholder attribute",
  "property": "placeholder",
  "tags": ["forms", "attribute"],
  "builderAliases": ["forms_placeholder"]
}
!*/
Modernizr.addTest("placeholder","placeholder"in createElement("input")&&"placeholder"in createElement("textarea")),testRunner(),setClasses(classes),delete ModernizrProto.addTest,delete ModernizrProto.addAsyncTest;for(var i=0;i<Modernizr._q.length;i++)Modernizr._q[i]();window.Modernizr=Modernizr}(window,document);