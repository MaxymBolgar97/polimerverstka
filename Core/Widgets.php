<?php

namespace Core;

use Modules\Catalog\Models\Groups;
use Modules\Catalog\Models\Items;
use Modules\News\Models\News;
use Core\QB\DB;
use Modules\Cart\Models\Cart;
use Modules\Catalog\Models\Filter;
use Core\User;

/**
 *  Class that helps with widgets on the site
 */
class Widgets {

    static $_instance; // Constant that consists self class
    public $_data = []; // Array of called widgets
    public $_tree = []; // Only for catalog menus on footer and header. Minus one query

    // Instance method

    static function factory() {
        if (self::$_instance == NULL) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     *  Get widget
     * @param  string $name [Name of template file]
     * @param  array $array [Array with data -> go to template]
     * @return string        [Widget HTML]
     */
    public static function get($name, $array = [], $save = true, $cache = false) {
        $arr = explode('_', $name);
        $viewpath = implode('/', $arr);

        if (APPLICATION == 'backend' && !Config::get('error')) {
            $w = WidgetsBackend::factory();
        } else {
            $w = Widgets::factory();
        }

        $_cache = Cache::instance();
        if ($cache) {
            if (!$_cache->get($name)) {
                $data = NULL;
                if ($save && isset($w->_data[$name])) {
                    $data = $w->_data[$name];
                } else {
                    if ($save && isset($w->_data[$name])) {
                        $data = $w->_data[$name];
                    } else if (method_exists($w, $name)) {
                        $result = $w->$name($array);
                        if ($result !== null && $result !== false) {
                            $array = array_merge($array, $result);
                            $data = View::widget($array, $viewpath);
                        } else {
                            $data = null;
                        }
                    } else {
                        $data = $w->common($viewpath, $array);
                    }
                }
                $_cache->set($name, HTML::compress($data, true));
                return $w->_data[$name] = $data;
            } else {
                return $_cache->get($name);
            }
        }
        if ($_cache->get($name)) {
            $_cache->delete($name);
        }
        if ($save && isset($w->_data[$name])) {
            return $w->_data[$name];
        }
        if (method_exists($w, $name)) {
            $result = $w->$name($array);
            if ($result !== null && $result !== false) {
                if (is_array($result)) {
                    $array = array_merge($array, $result);
                }
                return $w->_data[$name] = View::widget($array, $viewpath);
            } else {
                return $w->_data[$name] = null;
            }
        }
        return $w->_data[$name] = $w->common($viewpath, $array);
    }

    /**
     *  Common widget method. Uses when we have no widgets called $name
     * @param  string $viewpath [Name of template file]
     * @param  array $array [Array with data -> go to template]
     * @return string            [Widget HTML or NULL if template doesn't exist]
     */
    public function common($viewpath, $array) {
        if (file_exists(HOST . '/Views/Widgets/' . $viewpath . '.php')) {
            return View::widget($array, $viewpath);
        }
        return null;
    }

    public function Index_Section1() {
        $firstLineTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'firstLineTop')
                ->find();

        $secondLineTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'secondLineTop')
                ->find();

        $thirdLineTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'thirdLineTop')
                ->find();

        $paragTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'paragTop')
                ->find();

        $sliderTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'sliderTop')
                ->find();

        $priceList = DB::select()
                ->from('instructionsUSE')
                ->where('alias', '=', 'priceList')
                ->find();

        $array['firstLineTop'] = $firstLineTop;
        $array['secondLineTop'] = $secondLineTop;
        $array['thirdLineTop'] = $thirdLineTop;
        $array['paragTop'] = $paragTop;
        $array['sliderTop'] = $sliderTop;
        $array['priceList'] = $priceList;
        return $array;
    }

    public function Index_Section2() {
        $firstLineSect2 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'firstLineSect2')
                ->find();

        $secondLineSect2 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'secondLineSect2')
                ->find();

        $paragSect2 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'paragSect2')
                ->order_by('id')
                ->find_all();

        $btn1Sect2 = DB::select()
                ->from('instructionsUSE')
                ->where('alias', '=', 'takeOrder')
                ->find();

        $btn2Sect2 = DB::select()
                ->from('instructionsUSE')
                ->where('alias', '=', 'aboutComany')
                ->find();

        $array['firstLineSect2'] = $firstLineSect2;
        $array['secondLineSect2'] = $secondLineSect2;
        $array['paragSect2'] = $paragSect2;
        $array['btn1Sect2'] = $btn1Sect2;
        $array['btn2Sect2'] = $btn2Sect2;
        return $array;
    }

    public function Index_Section3() {
        $firstLineSect3 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'firstLineSect3')
                ->find();

        $secondLineSect3 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'secondLineSect3')
                ->find();

        $imgTitleSect3 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'imgTitleSect3')
                ->find();

        $array['firstLineSect3'] = $firstLineSect3;
        $array['secondLineSect3'] = $secondLineSect3;
        $array['imgTitleSect3'] = $imgTitleSect3;

        return $array;
    }

    public function Index_Section4() {
        $titleSect4 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'titleSect4')
                ->find();

        $smallTitle1Sect4 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'smallTitle1Sect4')
                ->find();

        $parag1Sect4 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'parag1Sect4')
                ->find();

        $smallTitle2Sect4 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'smallTitle2Sect4')
                ->find();

        $parag2Sect4 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'parag2Sect4')
                ->find();

        $smallTitle3Sect4 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'smallTitle3Sect4')
                ->find();

        $parag3Sect4 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'parag3Sect4')
                ->find();

        $array['titleSect4'] = $titleSect4;
        $array['smallTitle1Sect4'] = $smallTitle1Sect4;
        $array['parag1Sect4'] = $parag1Sect4;
        $array['smallTitle2Sect4'] = $smallTitle2Sect4;
        $array['parag2Sect4'] = $parag2Sect4;
        $array['smallTitle3Sect4'] = $smallTitle3Sect4;
        $array['parag3Sect4'] = $parag3Sect4;

        return $array;
    }

    public function Index_Section5() {

        $titleSect5 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'titleSect5')
                ->find();

        $descSect5 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'descSect5')
                ->find();

        $sert1Sect5 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'sert1Sect5')
                ->find();

        $sert2Sect5 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'sert2Sect5')
                ->find();

        $sert3Sect5 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'sert3Sect5')
                ->find();

        $sert4Sect5 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'sert4Sect5')
                ->find();

        $sert5Sect5 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'sert5Sect5')
                ->find();

        $array['titleSect5'] = $titleSect5;
        $array['descSect5'] = $descSect5;
        $array['sert1Sect5'] = $sert1Sect5;
        $array['sert2Sect5'] = $sert2Sect5;
        $array['sert3Sect5'] = $sert3Sect5;
        $array['sert4Sect5'] = $sert4Sect5;
        $array['sert5Sect5'] = $sert5Sect5;

        return $array;
    }

    public function Index_Section6() {

        $titleSect6 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'titleSect6')
                ->find();

        $descSect6 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'descSect6')
                ->find();

        $btn1Sect6 = DB::select()
                ->from('instructionsUSE')
                ->where('alias', '=', 'takeOrder')
                ->find();

        $array['titleSect6'] = $titleSect6;
        $array['descSect6'] = $descSect6;
        $array['btn1Sect6'] = $btn1Sect6;

        return $array;
    }

    public function Index_Section7() {

        return true;
    }

    public function Delivery_Section8() {

        $titleLine1Sect8 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'titleLine1Sect8')
                ->find();

        $titleLine2Sect8 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'titleLine2Sect8')
                ->find();

        $paragSect8 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'paragSect8')
                ->find();

        $btnSect8 = DB::select()
                ->from('instructionsUSE')
                ->where('alias', '=', 'takeOrder')
                ->find();

        $array['titleLine1Sect8'] = $titleLine1Sect8;
        $array['titleLine2Sect8'] = $titleLine2Sect8;
        $array['paragSect8'] = $paragSect8;
        $array['btnSect8'] = $btnSect8;

        return $array;
    }

    public function AboutUs_Section8() {

        $firstLineTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'firstLineTop')
                ->find();

        $secondLineTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'secondLineTop')
                ->find();

        $thirdLineTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'thirdLineTop')
                ->find();

        $paragTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'paragTop')
                ->find();

        $btnSect8about = DB::select()
                ->from('instructionsUSE')
                ->where('alias', '=', 'takeOrder')
                ->find();

        $array['firstLineTop'] = $firstLineTop;
        $array['secondLineTop'] = $secondLineTop;
        $array['thirdLineTop'] = $thirdLineTop;
        $array['paragTop'] = $paragTop;
        $array['btnSect8about'] = $btnSect8about;

        return $array;
    }

    public function AboutUs_Section9() {

        $titleSect9 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'titleSect9')
                ->find();

        $descSect9 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'descSect9')
                ->find();

        $paragSect9 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'paragSect9')
                ->find_all();

        $array['titleSect9'] = $titleSect9;
        $array['descSect9'] = $descSect9;
        $array['paragSect9'] = $paragSect9;

        return $array;
    }

    public function Delivery_Section12() {

        $titleSect12 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'titleSect12')
                ->find();

        $paragSect12 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'paragSect12')
                ->find_all();

        $strongSect12 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'strongSect12')
                ->find();

        $descSect12 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'descSect12')
                ->find();

        $liSect12 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'liSect12')
                ->find_all();

        $array['titleSect12'] = $titleSect12;
        $array['paragSect12'] = $paragSect12;
        $array['strongSect12'] = $strongSect12;
        $array['descSect12'] = $descSect12;
        $array['liSect12'] = $liSect12;

        return $array;
    }

    public function Contacts_Section13() {

        $titleSect13 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'titleSect13')
                ->find();

        $descSect13 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'descSect13')
                ->find();

        $write = DB::select()
                ->from('instructionsUSE')
                ->where('alias', '=', 'write')
                ->find();

        $array['titleSect13'] = $titleSect13;
        $array['descSect13'] = $descSect13;
        $array['write'] = $write;

        return $array;
    }

    public function Contacts_Section14() {

        $adress1Sect14 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'adress1Sect14')
                ->find();

        $mainTelSect14 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'mainTelSect14')
                ->find();

        $mainEmailSect14 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'mainEmailSect14')
                ->find();

        $mainSiteSect14 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'mainSiteSect14')
                ->find();

        $mob1Sect14 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'mob1Sect14')
                ->find();

        $email11Sect14 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'email11Sect14')
                ->find();

        $mob2Sect14 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'mob2Sect14')
                ->find();

        $rab2Sect14 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'rab2Sect14')
                ->find();

        $email2Sect14 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'email2Sect14')
                ->find();

        $mob3Sect14 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'mob3Sect14')
                ->find();

        $rab3Sect14 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'rab3Sect14')
                ->find();

        $email3Sect14 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'email3Sect14')
                ->find();

        $array['adress1Sect14'] = $adress1Sect14;
        $array['mainTelSect14'] = $mainTelSect14;
        $array['mainEmailSect14'] = $mainEmailSect14;
        $array['mainSiteSect14'] = $mainSiteSect14;
        $array['mob1Sect14'] = $mob1Sect14;
        $array['email11Sect14'] = $email11Sect14;
        $array['mob2Sect14'] = $mob2Sect14;
        $array['rab2Sect14'] = $rab2Sect14;
        $array['email2Sect14'] = $email2Sect14;
        $array['mob3Sect14'] = $mob3Sect14;
        $array['rab3Sect14'] = $rab3Sect14;
        $array['email3Sect14'] = $email3Sect14;

        return $array;
    }
    
    public function PriceList_Section8() {

        $firstLineTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'priceTitle')
                ->find();

        $secondLineTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'secondLineTop')
                ->find();

        $thirdLineTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'thirdLineTop')
                ->find();

        $paragTop = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'paragTop')
                ->find();

        $btnSect8priceList = DB::select()
                ->from('instructionsUSE')
                ->where('alias', '=', 'takeOrder')
                ->find();

        $array['firstLineTop'] = $firstLineTop;
        $array['secondLineTop'] = $secondLineTop;
        $array['thirdLineTop'] = $thirdLineTop;
        $array['paragTop'] = $paragTop;
        $array['btnSect8priceList'] = $btnSect8priceList;

        return $array;
    }
    
     public function Products_Section8() {
         
         $productsTitle = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'productsTitle')
                ->find();

        $descSect9 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'descSect9')
                ->find();
        
        $btnSect8products = DB::select()
                ->from('instructionsUSE')
                ->where('alias', '=', 'takeOrder')
                ->find();

        $array['productsTitle'] = $productsTitle;
        $array['descSect9'] = $descSect9;
        $array['btnSect8products'] = $btnSect8products;
        
        return $array;
     }

    public function Footer() {
        $contentMenu = Common::factory('sitemenuUSE')->getRows(1, 'sort');

        $footerText1 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'footerText1')
                ->find();

        $footerAdress = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'footerAdress')
                ->find();

        $footerTel1 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'footerTel1')
                ->find();

        $footerTel2 = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'footerTel2')
                ->find();

        $footerCopyright = DB::select()
                ->from('titlesUSE')
                ->where('alias', '=', 'footerCopyright')
                ->find();

        $array['footerText1'] = $footerText1;
        $array['footerAdress'] = $footerAdress;
        $array['footerTel1'] = $footerTel1;
        $array['footerTel2'] = $footerTel2;
        $array['footerCopyright'] = $footerCopyright;
        $array['contentMenu'] = $contentMenu;

        return $array;
    }

    public function Header() {
        $contentMenu = Common::factory('sitemenuUSE')->getRows(1, 'sort');

        $phones = DB::select()
                ->from('contactsInfoUSE')
                ->where('status', '=', 1)
                ->where('alias', '=', 'phone')
                ->order_by('sort')
                ->find_all();

        $adresses = DB::select()
                ->from('contactsInfoUSE')
                ->where('status', '=', 1)
                ->where('alias', '=', 'adress')
                ->order_by('sort')
                ->find_all();

        $callBack = DB::select()
                ->from('instructionsUSE')
                ->where('alias', '=', 'callback')
                ->find();
        
        $user = DB::select()
                ->from('users1')
                ->where('id', '=', $_SESSION['user'])
                ->find();
        

        $array['callBack'] = $callBack;
        $array['adresses'] = $adresses;
        $array['phones'] = $phones;
        $array['contentMenu'] = $contentMenu;
        $array['user'] = $user;
        return $array;
    }

    public function Head() {
        return true;
    }

}
